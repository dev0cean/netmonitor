#!/usr/bin/env python
import os
import sys
sys.path.append(os.path.join("src", "python"))
from install import InstallCommand
from setuptools import setup, find_packages


if sys.platform == 'darwin' and [int(x) for x in os.uname()[2].split('.')] >= [11, 0, 0]:
    # clang is serious now
    extra_compile_args = ['-Werror', '-Wunused-command-line-argument-hard-error-in-future', '-Qunused-arguments']
else:
    extra_compile_args = ['-Werror']


setup(
        name="netmonitor",
        version="1.0.0",
        description="",
        url='',
        license='',
        author="dwhitla",
        author_email="dave.whitla@devocean.net",
        package_dir={'': "src/python"},
        packages=find_packages("src/python"),
        prefix="/opt/local",
        install_dir="sbin",
        scripts=[
            "netmonitor",
            "networking",
            "pflogd",
        ],
        config_files=[
            ("src/resources/netmonitor", "etc/netmonitor"),
            ("src/resources/firewall", "etc/firewall"),
            ("src/resources/LaunchDaemons", "etc/LaunchDaemons"),
            ("src/resources/network.conf", "etc/network.conf"),
        ],
        cmdclass={
            "install": InstallCommand,
        },
        install_requires=[
            "six", "pydevd",
            "pyobjc",
            # "py-pf",
            # "ocsutil==1.0.4"
        ],
        dependency_links=[
            # "https://bitbucket.org/dwhitla/python-ocsutil/get/1.0.4.zip#egg=ocsutil-1.0.4",
        ],
        classifiers=[
            "Programming Language :: Python :: 3",
            "Development Status :: 3 - Alpha",
            "Topic :: System :: Systems Administration :: Security"
        ],
)
