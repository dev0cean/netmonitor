from socket import AF_INET
import unittest
from pf.util import ctonm, nmtoc


class UtilTestCase(unittest.TestCase):
    def test_ctonm(self):
        self.assertEqual("255.255.255.0", ctonm(24, AF_INET))
        self.assertEqual("255.255.255.255", ctonm(32, AF_INET))

    def test_nmtoc(self):
        self.assertEqual(24, nmtoc("255.255.255.0", AF_INET))
        self.assertEqual(32, nmtoc("255.255.255.255", AF_INET))


if __name__ == '__main__':
    unittest.main()
