from pf.iface import Interface
import unittest


class InterfaceTestCase(unittest.TestCase):
    def test_init(self):
        iface = Interface()
        iface.name = "lo0"
        self.assertEqual("lo0", iface.name)


if __name__ == '__main__':
    unittest.main()
