# -*- coding: utf-8 -*-
import os
from distutils import log
from distutils.dep_util import newer
from setuptools.command import install as orig
from stat import ST_MODE


# noinspection PyAttributeOutsideInit
class InstallCommand(orig.install):

    user_options = orig.install.user_options + [
        ('config-files', None, "a collection of tuples mapping config file source locations (relative to setup.py) "
                               "to destination locations (relative to the config_prefix)"),
        ('config-prefix', None, "base installation directory for configuration files"),
    ]

    def initialize_options(self):
        orig.install.initialize_options(self)
        self.config_files = None
        self.config_prefix = "/etc"
        self.outfiles = []

    def finalize_options(self):
        orig.install.finalize_options(self)
        # If a new root directory was supplied, make the config dirs relative to it.
        if self.root is not None:
            self.change_roots('config_prefix')

    def do_egg_install(self):
        self.install_config()
        orig.install.do_egg_install(self)

    def run(self):
        orig.install.run(self)
        self.install_config()

    def install_config(self):
        if self.config_files is None:
            return
        config_prefix = self.config_prefix
        if self.prefix and self.prefix != "/usr":
            config_prefix = self.prefix + "/etc"
        config_prefix = (self.root or "") + os.path.normcase(os.path.normpath(config_prefix))

        for src, dest in self.config_files:
            dest = os.path.normpath(os.path.expanduser(dest))
            if not os.path.isabs(dest):
                dest = os.path.join(config_prefix, dest)

            if not self.force and not newer(src, dest):
                log.debug("not copying %s (up-to-date)", dest)
                continue

            self.mkpath(os.path.dirname(dest))
            if os.path.isdir(src):
                self.copy_tree(src, dest)
            else:
                self.copy_file(src, dest)
            self.outfiles.append(dest)
            if self.dry_run:
                log.info("changing mode of %s", dest)
            else:
                mode = ((os.stat(dest)[ST_MODE]) | 0o660) & 0o7777
                log.info("changing mode of %s to %o", dest, mode)
                os.chmod(dest, mode)

    def get_outputs(self):
        outputs = orig.install.get_outputs(self)
        outputs.append(self.outfiles)
        return outputs
