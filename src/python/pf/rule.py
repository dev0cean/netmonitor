from _ctypes import POINTER, Union
from ctypes import Structure, c_uint32, c_uint8, c_uint16, c_int32, c_uint, c_void_p, c_char, c_uint64, c_int
from socket import AF_INET, AF_INET6, IPPROTO_TCP
import pwd
import grp

from pf.address import AddressSpec, pf_port_range, PortRange, pf_addr_wrap, pf_pool
from pf.base import StructWrapper, Constraint
from pf.enum import PF_OPS
from pf.iface import IFNAMSIZ
from pf.pfvar import PF_OP_NONE, PFTM_MAX, PF_TAG_NAME_SIZE, PF_TABLE_NAME_SIZE, PF_QNAME_SIZE, \
    PF_RULE_LABEL_SIZE, PF_SKIP_COUNT, PF_OWNER_NAME_SIZE, PF_RULESET_MAX, MAXPATHLEN
from pf.queue import TAILQ_HEAD
from pf.tree import RB_HEAD


__all__ = ['RuleUID',
           'RuleGID',
           'PortSpec',
           'AddressSpec',
           'RuleAddress',
           'AddressPool',
           'Rule',
           'Ruleset']

PF_ANCHOR_NAME_SIZE = 64


class pf_anchor(Structure):
    """
    RB_HEAD(pf_anchor_global, pf_anchor);
    RB_HEAD(pf_anchor_node, pf_anchor);
    struct pf_anchor {
        RB_ENTRY(pf_anchor)     entry_global;
        RB_ENTRY(pf_anchor)     entry_node;
        struct pf_anchor    *parent;
        struct pf_anchor_node     children;
        char             name[PF_ANCHOR_NAME_SIZE];
        char             path[MAXPATHLEN];
        struct pf_ruleset     ruleset;
        int             refcnt;    /* anchor rules */
        int             match;
        char             owner[PF_OWNER_NAME_SIZE];
    };
    """
pf_anchor_global = RB_HEAD("pf_anchor_global", pf_anchor)
pf_anchor_node = RB_HEAD("pf_anchor_node", pf_anchor)


"""
RB_PROTOTYPE(pf_anchor_global, pf_anchor, entry_global, pf_anchor_compare);
RB_PROTOTYPE(pf_anchor_node, pf_anchor, entry_node, pf_anchor_compare);
"""

PF_RESERVED_ANCHOR = "_pf"


class pf_rule_uid(Structure):
    """
    struct pf_rule_uid {
        uid_t         uid[2];
        u_int8_t     op;
        u_int8_t     _pad[3];
    };
    """
    _fields_ = [
        ("uid", c_uint32 * 2),  # uid_t
        ("op", c_uint8),
        ("_pad", c_uint8 * 3),
    ]


class RuleUID(Constraint):
    """Class representing a user ID."""

    @property
    def _struct_type(self):
        return pf_rule_uid

    def __init__(self, uid=None, op=PF_OP_NONE):
        super(RuleUID, self).__init__(uid, op)

    @property
    def operands(self):
        return self.uid, None

    @operands.setter
    def operands(self, tup):
        setattr(self, "uid", tup[0])

    @property
    def operator(self):
        return self.op

    @operator.setter
    def operator(self, value):
        if isinstance(value, basestring):
            value = PF_OPS[value]
        if not isinstance(value, int):
            raise ValueError()
        setattr(self, "op", value)

    def _num_to_str(self, n):
        """Convert a numeric user ID to a string."""
        try:
            return pwd.getpwuid(n).pw_name
        except KeyError:
            return n

    def _str_to_num(self, s):
        """Convert a string to a numeric user ID."""
        return pwd.getpwnam(s).pw_uid


class pf_rule_gid(Structure):
    """
    struct pf_rule_gid {
        uid_t         gid[2];
        u_int8_t     op;
        u_int8_t     _pad[3];
    };
    """
    _fields_ = [
        ("gid", c_uint32 * 2),  # uid_t
        ("op", c_uint8),
        ("_pad", c_uint8 * 3),
    ]


class RuleGID(Constraint):
    """Class representing a group ID."""

    @property
    def _struct_type(self):
        return pf_rule_gid

    def __init__(self, gid=None, op=PF_OP_NONE):
        super(RuleGID, self).__init__(gid, op)

    @property
    def operands(self):
        return self.gid, None

    @operands.setter
    def operands(self, tup):
        setattr(self, "gid", tup[0])

    @property
    def operator(self):
        return self.op

    @operator.setter
    def operator(self, value):
        if isinstance(value, basestring):
            value = PF_OPS[value]
        if not isinstance(value, int):
            raise ValueError()
        setattr(self, "op", value)

    def _num_to_str(self, n):
        """Convert a numeric group ID to a string."""
        try:
            return grp.getgrgid(n).gr_name
        except KeyError:
            return n

    def _str_to_num(self, s):
        """Convert a string to a numeric group ID."""
        return grp.getgrnam(s).gr_gid


class pf_rule_xport(Structure):
    """
    union pf_rule_xport {
        struct pf_port_range    range;
        u_int16_t        call_id;
        u_int32_t        spi;
    };
    """
    _fields_ = [
        ("range", pf_port_range),
        ("call_id", c_uint16),
        ("spi", c_uint32),
    ]


class RuleXPort(StructWrapper):
    def __init__(self, obj=None, **kwargs):
        super(RuleXPort, self).__init__(obj, **kwargs)
        self._range = PortRange(self.range_)

    @property
    def _struct_type(self):
        return pf_rule_xport

    @property
    def range(self):
        return self._range

    @range.setter
    def range(self, value):
        if not isinstance(value, PortRange):
            value = PortRange(value)
        self._range = value

    def _to_string(self):
        return str(self.range.port)

    def __eq__(self, other):
        return (self.range == other.range and
                self.call_id == other.call_id and
                self.spi == other.spi)

    def __ne__(self, other):
        return not self.__eq__(other)


# rule flags
PFRULE_DROP = 0x0000
PFRULE_RETURNRST = 0x0001
PFRULE_FRAGMENT = 0x0002
PFRULE_RETURNICMP = 0x0004
PFRULE_RETURN = 0x0008
PFRULE_NOSYNC = 0x0010
PFRULE_SRCTRACK = 0x0020        # track source states
PFRULE_RULESRCTRACK = 0x0040    # per rule

# scrub flags
PFRULE_NODF = 0x0100
PFRULE_FRAGCROP = 0x0200        # non-buffering frag cache
PFRULE_FRAGDROP = 0x0400        # drop funny fragments
PFRULE_RANDOMID = 0x0800
PFRULE_REASSEMBLE_TCP = 0x1000

# rule flags for TOS/DSCP/service class differentiation
PFRULE_TOS = 0x2000
PFRULE_DSCP = 0x4000
PFRULE_SC = 0x8000

# rule flags again
PFRULE_IFBOUND = 0x00010000     # if-bound
PFRULE_PFM = 0x00020000         # created by pfm device


PFSTATE_NOSYNC = 0x01
PFSTATE_FROMSYNC = 0x02
PFSTATE_STALE = 0x04


PF_STATE_NORMAL = 0x1
PF_STATE_MODULATE = 0x2
PF_STATE_SYNPROXY = 0x3


PF_FLUSH = 0x01
PF_FLUSH_GLOBAL = 0x02


# service class categories
SCIDX_MASK = 0x0f
SC_BE = 0x10
SC_BK_SYS = 0x11
SC_BK = 0x12
SC_RD = 0x13
SC_OAM = 0x14
SC_AV = 0x15
SC_RV = 0x16
SC_VI = 0x17
SC_VO = 0x18
SC_CTL = 0x19

# diffserve code points
DSCP_MASK = 0xfc
DSCP_CUMASK = 0x03
DSCP_EF = 0xb8
DSCP_AF11 = 0x28
DSCP_AF12 = 0x30
DSCP_AF13 = 0x38
DSCP_AF21 = 0x48
DSCP_AF22 = 0x50
DSCP_AF23 = 0x58
DSCP_AF31 = 0x68
DSCP_AF32 = 0x70
DSCP_AF33 = 0x78
DSCP_AF41 = 0x88
DSCP_AF42 = 0x90
DSCP_AF43 = 0x98
AF_CLASSMASK = 0xe0
AF_DROPPRECMASK = 0x18


class pf_rule_addr(Structure):
    """
    struct pf_rule_addr {
        struct pf_addr_wrap addr;
        union pf_rule_xport xport;
        u_int8_t neg;
    };
    """
    _fields_ = [
        ("addr", pf_addr_wrap),
        ("xport", pf_rule_xport),
        ("neg", c_uint8),
    ]


class RuleAddress(StructWrapper):
    """Class representing an address/port pair."""

    @property
    def _struct_type(self):
        return pf_rule_addr

    def __init__(self, *args, **kwargs):
        af = kwargs.pop('af', AF_INET)
        super(RuleAddress, self).__init__(*args, **kwargs)
        self._addr = AddressSpec(self.addr_, af)
        self._xport = RuleXPort(self.xport_)

    @property
    def addr(self):
        return self._addr

    @addr.setter
    def addr(self, value):
        if not isinstance(value, AddressSpec):
            value = AddressSpec(value)
        self._addr = value

    @property
    def xport(self):
        return self._xport

    @xport.setter
    def xport(self, value):
        if not isinstance(value, RuleXPort):
            value = RuleXPort(value)
        self._xport = value

    @property
    def neg(self):
        return bool(self.neg_)

    # noinspection PyAttributeOutsideInit
    @neg.setter
    def neg(self, value):
        self.neg_ = 1 if value else 0

    def _to_string(self):
        s = ("! {.addr}" if self.neg else "{.addr}").format(self)
        if self.xport:
            s += (":" if self.xport.range.op == PF_OP_NONE else " port ") + "{.xport}".format(self)
        return s

    def __eq__(self, other):
        return (self.addr == other.addr and
                self.xport == other.xport and
                self.neg == other.neg)

    def __ne__(self, other):
        return not self.__eq__(other)


class pf_rule(Structure):
    """
    struct pf_rule {
        struct pf_rule_addr     src;
        struct pf_rule_addr     dst;
        union pf_rule_ptr     skip[PF_SKIP_COUNT];
        char             label[PF_RULE_LABEL_SIZE];
        char             ifname[IFNAMSIZ];
        char             qname[PF_QNAME_SIZE];
        char             pqname[PF_QNAME_SIZE];
        char             tagname[PF_TAG_NAME_SIZE];
        char             match_tagname[PF_TAG_NAME_SIZE];

        char             overload_tblname[PF_TABLE_NAME_SIZE];

        TAILQ_ENTRY(pf_rule)     entries;
        struct pf_pool         rpool;

        u_int64_t         evaluations;
        u_int64_t         packets[2];
        u_int64_t         bytes[2];

        u_int64_t         ticket;
        char             owner[PF_OWNER_NAME_SIZE];
        u_int32_t         priority;

    #ifdef KERNEL
        struct pfi_kif        *kif        __attribute__((aligned(8)));
    #else /* !KERNEL */
        void            *kif        __attribute__((aligned(8)));
    #endif /* !KERNEL */
        struct pf_anchor    *anchor        __attribute__((aligned(8)));
    #ifdef KERNEL
        struct pfr_ktable    *overload_tbl    __attribute__((aligned(8)));
    #else /* !KERNEL */
        void            *overload_tbl    __attribute__((aligned(8)));
    #endif /* !KERNEL */

        pf_osfp_t         os_fingerprint    __attribute__((aligned(8)));

        unsigned int         rtableid;
        u_int32_t         timeout[PFTM_MAX];
        u_int32_t         states;
        u_int32_t         max_states;
        u_int32_t         src_nodes;
        u_int32_t         max_src_nodes;
        u_int32_t         max_src_states;
        u_int32_t         max_src_conn;
        struct {
            u_int32_t        limit;
            u_int32_t        seconds;
        }             max_src_conn_rate;
        u_int32_t         qid;
        u_int32_t         pqid;
        u_int32_t         rt_listid;
        u_int32_t         nr;
        u_int32_t         prob;
        uid_t             cuid;
        pid_t             cpid;

        u_int16_t         return_icmp;
        u_int16_t         return_icmp6;
        u_int16_t         max_mss;
        u_int16_t         tag;
        u_int16_t         match_tag;

        struct pf_rule_uid     uid;
        struct pf_rule_gid     gid;

        u_int32_t         rule_flag;
        u_int8_t         action;
        u_int8_t         direction;
        u_int8_t         log;
        u_int8_t         logif;
        u_int8_t         quick;
        u_int8_t         ifnot;
        u_int8_t         match_tag_not;
        u_int8_t         natpass;

        u_int8_t         keep_state;
        sa_family_t         af;
        u_int8_t         proto;
        u_int8_t         type;
        u_int8_t         code;
        u_int8_t         flags;
        u_int8_t         flagset;
        u_int8_t         min_ttl;
        u_int8_t         allow_opts;
        u_int8_t         rt;
        u_int8_t         return_ttl;

        u_int8_t         tos;
        u_int8_t         anchor_relative;
        u_int8_t         anchor_wildcard;

    #define PF_FLUSH        0x01
    #define PF_FLUSH_GLOBAL        0x02
        u_int8_t         flush;

        u_int8_t        proto_variant;
        u_int8_t        extfilter; /* Filter mode [PF_EXTFILTER_xxx] */
        u_int8_t        extmap;    /* Mapping mode [PF_EXTMAP_xxx] */
        u_int32_t               dnpipe;
        u_int32_t               dntype;
    };
    """


class pf_rule_ptr(Union):
    """
    union pf_rule_ptr {
        struct pf_rule* ptr     __attribute__((aligned(8)));
        u_int32_t       nr      __attribute__((aligned(8)));
    } __attribute__((aligned(8)));
    """
    _fields_ = [
        ("ptr", POINTER(pf_rule)),  # struct pf_rule* ptr
        ("nr", c_uint32),
    ]


pf_rule._fields_ = [
    ("src", pf_rule_addr),
    ("dst", pf_rule_addr),
    ("skip", pf_rule_ptr * PF_SKIP_COUNT),
    ("label", c_char * PF_RULE_LABEL_SIZE),
    ("ifname", c_char * IFNAMSIZ),
    ("qname", c_char * PF_QNAME_SIZE),
    ("pqname", c_char * PF_QNAME_SIZE),
    ("tagname", c_char * PF_TAG_NAME_SIZE),
    ("match_tagname", c_char * PF_TAG_NAME_SIZE),
    ("overload_tblname", c_char * PF_TABLE_NAME_SIZE),
    ("entries", c_void_p * 2),  # TAILQ_ENTRY(pf_rule)
    ("rpool", pf_pool),
    ("evaluations", c_uint64),
    ("packets", c_uint64 * 2),
    ("bytes", c_uint64 * 2),
    ("ticket", c_uint64),
    ("owner", c_char * PF_OWNER_NAME_SIZE),
    ("priority", c_uint32),
    ("kif", c_void_p),
    ("anchor", POINTER(pf_anchor)),
    ("overload_tbl", c_void_p),
    ("os_fingerprint", c_uint32),   # pf_osfp_t
    ("rtableid", c_uint),
    ("timeout", c_uint32 * PFTM_MAX),
    ("states", c_uint32),
    ("max_states", c_uint32),
    ("src_nodes", c_uint32),
    ("max_src_nodes", c_uint32),
    ("max_src_states", c_uint32),
    ("max_src_conn", c_uint32),
    ("max_src_conn_rate", type("_conn_rate", (Structure,), {
        "_fields_": [
            ("limit", c_uint32),
            ("seconds", c_uint32),
        ]})),
    ("qid", c_uint32),
    ("pqid", c_uint32),
    ("rt_listid", c_uint32),
    ("nr", c_uint32),
    ("prob", c_uint32),
    ("cuid", c_uint32),      # uid_t
    ("cpid", c_int32),       # pid_t
    ("return_icmp", c_uint16),
    ("return_icmp6", c_uint16),
    ("max_mss", c_uint16),
    ("tag", c_uint16),
    ("match_tag", c_uint16),
    ("uid", pf_rule_uid),
    ("gid", pf_rule_gid),
    ("rule_flag", c_uint32),
    ("action", c_uint8),
    ("direction", c_uint8),
    ("log", c_uint8),
    ("logif", c_uint8),
    ("quick", c_uint8),
    ("ifnot", c_uint8),
    ("match_tag_not", c_uint8),
    ("natpass", c_uint8),
    ("keep_state", c_uint8),
    ("af", c_uint8),    # sa_family_t
    ("proto", c_uint8),
    ("type", c_uint8),
    ("code", c_uint8),
    ("flags", c_uint8),
    ("flagset", c_uint8),
    ("min_ttl", c_uint8),
    ("allow_opts", c_uint8),
    ("rt", c_uint8),
    ("return_ttl", c_uint8),
    ("tos", c_uint8),
    ("anchor_relative", c_uint8),
    ("anchor_wildcard", c_uint8),
    ("flush", c_uint8),
    ("proto_variant", c_uint8),
    ("extfilter", c_uint8),     # Filter mode [PF_EXTFILTER_xxx]
    ("extmap", c_uint8),        # Mapping mode [PF_EXTMAP_xxx]
    ("dnpipe", c_uint32),
    ("dntype", c_uint32),
]


# noinspection PyUnresolvedReferences
class Rule(StructWrapper):
    """Class representing a Packet Filter rule."""

    @property
    def _struct_type(self):
        return pf_rule

    def __init__(self, rule=None, **kw):
        """Check arguments and initialize instance attributes."""
        if rule is None:
            rule = pf_rule(rtableid=-1, onrdomain=-1)
        super(Rule, self).__init__(rule, **kw)
        self._src = RuleAddress(self.src_, af=self.af)
        self._dst = RuleAddress(self.dst_, af=self.af)
        self._uid = RuleUID(self.uid_)
        self._gid = RuleGID(self.gid_)

    @property
    def src(self):
        return self._src

    @src.setter
    def src(self, value):
        if not isinstance(value, RuleAddress):
            value = RuleAddress(value)
        self._src = value

    @property
    def dst(self):
        return self._dst

    @dst.setter
    def dst(self, value):
        if not isinstance(value, RuleAddress):
            value = RuleAddress(value)
        self._dst = value

    @property
    def packets(self):
        return tuple(self.packets_)

    @packets.setter
    def packets(self, value):
        self.packets_[0] = value[0]
        self.packets_[1] = value[1]

    @property
    def bytes(self):
        return tuple(self.bytes_)

    @bytes.setter
    def bytes(self, value):
        self.bytes_[0] = value[0]
        self.bytes_[1] = value[1]

    @property
    def timeout(self):
        return list(self.timeout_)

    @timeout.setter
    def timeout(self, value):
        for i, t in enumerate(self.timeout):
            self.timeout_[i] = t

    @property
    def max_src_conn_rate(self):
        return self.max_src_conn_rate_.limit, self.max_src_conn_rate_.seconds

    @property
    def uid(self):
        return self._uid

    @property
    def gid(self):
        return self._gid

    @property
    def quick(self):
        return bool(self.quick_)

    @quick.setter
    def quick(self, value):
        self.quick_ = 1 if value else 0

    @property
    def ifnot(self):
        return bool(self.ifnot_)

    @ifnot.setter
    def ifnot(self, value):
        self.ifnot_ = 1 if value else 0

    @property
    def match_tag_not(self):
        return bool(self.match_tag_not_)

    @match_tag_not.setter
    def match_tag_not(self, value):
        self.match_tag_not_ = 1 if value else 0

    @property
    def flags(self):
        return "".join([f for n, f in enumerate("FSRPAUEW") if self.flags_ & (1 << n)])

    @flags.setter
    def flags(self, value):
        self.flags_ = sum([1 << "FSRPAUEW".find(f) for f in value])

    @property
    def flagset(self):
        return "".join([f for n, f in enumerate("FSRPAUEW") if self.flagset_ & (1 << n)])

    @flagset.setter
    def flagset(self, value):
        self.flagset_ = sum([1 << "FSRPAUEW".find(f) for f in value])

    @property
    def allow_opts(self):
        return bool(self.allow_opts_)

    def _to_string(self):
        """Return the string representation of the rule."""
        pf_actions = ("pass", "block", "scrub", "no scrub", "nat", "no nat",
                      "binat", "no binat", "rdr", "no rdr", "", "", "match")
        pf_anchors = ("anchor", "anchor", "anchor", "anchor", "nat-anchor",
                      "nat-anchor", "binat-anchor", "binat-anchor",
                      "rdr-anchor", "rdr-anchor")

        if isinstance(self, Ruleset):
            s = pf_anchors[self.action]
            if not self.name.startswith("_"):
                s += " \"{.name}\"".format(self)
        else:
            s = pf_actions[self.action]

        if self.action == PF_DROP:
            if self.rule_flag & PFRULE_RETURN:
                s += " return"
            elif self.rule_flag & PFRULE_RETURNRST:
                s += " return-rst"
                if self.return_ttl:
                    s += "(ttl {.return_ttl})".format(self)
            elif self.rule_flag & PFRULE_RETURNICMP:
                ic = geticmpcodebynumber(self.return_icmp >> 8, self.return_icmp & 0xff, AF_INET)
                ic6 = geticmpcodebynumber(self.return_icmp6 >> 8, self.return_icmp6 & 0xff, AF_INET6)
                s += " return-icmp"
                if self.af == AF_INET:
                    s += "({})".format(ic or self.return_icmp & 0xff)
                elif self.af == AF_INET6:
                    s += "6({})".format(ic6 or self.return_icmp6 & 0xff)
                else:
                    s += "({}, {})".format((ic or self.return_icmp & 0xff), (ic6 or self.return_icmp6 & 0xff))
            else:
                s += " drop"

        if self.direction == PF_IN:
            s += " in"
        elif self.direction == PF_OUT:
            s += " out"

        if self.log:
            s += " log"
            if (self.log & ~PF_LOG) or self.logif:
                l = []
                if self.log & PF_LOG_ALL:
                    l.append("all")
                # UNSUPPORTED ON MAC OS X
                # if self.log & PF_LOG_MATCHES:
                # l.append("matches")
                if self.log & PF_LOG_SOCKET_LOOKUP:
                    l.append("user")
                if self.logif:
                    l.append("to pflog{.logif}".format(self))
                s += " ({})".format(", ".join(l))

        if self.quick:
            s += " quick"

        if self.ifname and self.ifname != "all":
            # "on all" not printed because it would make
            # the rule not parseable by pfctl
            if self.ifnot:
                s += " on ! {.ifname}".format(self)
            else:
                s += " on {.ifname}".format(self)

        if self.onrdomain >= 0:
            if self.ifnot:
                s += " on ! rdomain {.onrdomain}".format(self)
            else:
                s += " on rdomain {.onrdomain}".format(self)

        if self.af:
            s += " inet" if (self.af == AF_INET) else " inet6"

        if self.proto:
            s += " proto {}".format(getprotobynumber(self.proto) or self.proto)

        if (
            self.src.addr._is_any() and
            self.dst.addr._is_any() and
            not self.src.neg and
            not self.dst.neg and
            not self.src.port.operator and
            not self.dst.port.operator and
            self.os_fingerprint == PF_OSFP_ANY
        ):
            s += " all"
        else:
            s += " from {.src}".format(self)
            # if self.os_fingerprint != PF_OSFP_ANY:
            s += " to {.dst}".format(self)

        if self.rcv_ifname:
            s += " received on {.rcv_ifname}".format(self)
        if self.uid.operator:
            s += " user {.uid}".format(self)
        if self.gid.operator:
            s += " group {.gid}".format(self)

        if self.flags or self.flagset:
            s += " flags {0.flags}/{0.flagset}".format(self)
        elif (
            self.action in (PF_PASS, PF_MATCH) and
            self.proto in (0, IPPROTO_TCP) and
            not (self.rule_flag & PFRULE_FRAGMENT) and
            not isinstance(self, Ruleset) and
            self.keep_state
        ):
            s += " flags any"

        if self.type:
            it = geticmptypebynumber(self.type - 1, self.af)
            if self.af != AF_INET6:
                s += " icmp-type"
            else:
                s += " icmp6-type"
            s += " {}".format(it or self.type - 1)
            if self.code:
                ic = geticmpcodebynumber(self.type - 1, self.code - 1, self.af)
                s += " code {}".format(ic or self.code - 1)

        if self.tos:
            s += " tos {.tos:#04x}".format(self)

        has_opts = False
        if (
            (self.max_states or self.max_src_nodes or self.max_src_states) or
            self.rule_flag & (PFRULE_NOSYNC | PFRULE_SRCTRACK | PFRULE_IFBOUND) or
            filter(None, self.timeout)
        ):
            has_opts = True

        if not self.keep_state and self.action == PF_PASS and not isinstance(self, Ruleset):
            s += " no state"
        elif (self.keep_state == PF_STATE_NORMAL) and has_opts:
            s += " keep state"
        elif self.keep_state == PF_STATE_MODULATE:
            s += " modulate state"
        elif self.keep_state == PF_STATE_SYNPROXY:
            s += " synproxy state"

        if self.prob:
            s += " probability {:.0f}%".format(self.prob * 100.0 / (UINT_MAX + 1))

        if has_opts:
            opts = []
            if self.max_states:
                opts.append("max {.max_states}".format(self))
            if self.rule_flag & PFRULE_NOSYNC:
                opts.append("no-sync")
            if self.rule_flag & PFRULE_SRCTRACK:
                if self.rule_flag & PFRULE_RULESRCTRACK:
                    opts.append("source-track rule")
                else:
                    opts.append("source-track global")
            if self.max_src_states:
                opts.append("max-src-states {.max_src_states}".format(self))
            if self.max_src_conn:
                opts.append("max-src-conn {.max_src_conn}".format(self))
            if self.max_src_conn_rate[0]:
                opts.append("max-src-conn-rate " +
                            "{}/{}".format(*self.max_src_conn_rate))
            if self.max_src_nodes:
                opts.append("max-src-nodes {.max_src_nodes}".format(self))
            if self.overload_tblname:
                opt = "overload <{.overload_tblname}>".format(self)
                if self.flush:
                    opt += " flush"
                if self.flush & PF_FLUSH_GLOBAL:
                    opt += " global"
                opts.append(opt)
            if self.rule_flag & PFRULE_IFBOUND:
                opts.append("if-bound")
            for i, t in enumerate(self.timeout):
                if t:
                    tm = [k for (k, v) in PF_TIMEOUTS.iteritems() if v == i][0]
                    opts.append("{} {}".format(tm, t))

            s += " ({})".format(", ".join(opts))

        if self.rule_flag & PFRULE_FRAGMENT:
            s += " fragment"

        if self.scrub_flags >= PFRULE_NODF or self.min_ttl or self.max_mss:
            opts = []
            if self.scrub_flags & PFRULE_NODF:
                opts.append("no-df")
            if self.scrub_flags & PFRULE_RANDOMID:
                opts.append("random-id")
            if self.min_ttl:
                opts.append("min-ttl {.min_ttl}".format(self))
            if self.scrub_flags & PFRULE_REASSEMBLE_TCP:
                opts.append("reassemble tcp")
            if self.max_mss:
                opts.append("max_mss {.max_mss}".format(self))
            s += " scrub ({})".format(" ".join(opts))

        if self.allow_opts:
            s += " allow-opts"
        if self.label:
            s += " label \"{.label}\"".format(self)

        if self.qname and self.pqname:
            s += " queue({0.qname}, {0.pqname})".format(self)
        elif self.qname:
            s += " queue {.qname}".format(self)

        if self.tagname:
            s += " tag {.tagname}".format(self)
        if self.match_tagname:
            if self.match_tag_not:
                s += " !"
            s += " tagged {.match_tagname}".format(self)

        if self.rtableid != -1:
            s += " rtable {.rtableid}".format(self)

        if self.rt == PF_ROUTETO:
            s += " route-to {.route}".format(self)
        elif self.rt == PF_REPLYTO:
            s += " reply-to {.route}".format(self)
        elif self.rt == PF_DUPTO:
            s += " dup-to {.route}".format(self)

        return s


pf_rulequeue = TAILQ_HEAD("pf_rulequeue", pf_rule)


class pf_ruleset(Structure):
    """
    TAILQ_HEAD(pf_rulequeue, pf_rule);

    struct pf_ruleset {
        struct {
            struct pf_rulequeue     queues[2];
            struct {
                struct pf_rulequeue    *ptr;
                struct pf_rule        **ptr_array;
                u_int32_t         rcount;
                u_int32_t         ticket;
                int             open;
            }             active, inactive;
        }             rules[PF_RULESET_MAX];
        struct pf_anchor    *anchor;
        u_int32_t         tticket;
        int             tables;
        int             topen;
    };
    """
    class _rules(Structure):
        class _active_inactive(Structure):
            _fields_ = [
                ("ptr", POINTER(pf_rulequeue)),          # struct pf_rulequeue*
                ("ptr_array", POINTER(POINTER(pf_rule))),    # struct pf_rule**
                ("rcount", c_uint32),
                ("ticket", c_uint32),
                ("open", c_int),
            ]

        _fields_ = [
            ("queues", pf_rulequeue * 2),   # TAILQ_HEAD(pf_rulequeue, pf_rule);
            ("active", _active_inactive),
            ("inactive", _active_inactive),
        ]

    _fields_ = [
        ("rules", _rules * PF_RULESET_MAX),
        ("anchor", POINTER(pf_anchor)),   # struct pf_anchor*
        ("tticket", c_uint32),
        ("tables", c_int),
        ("topen", c_int),
    ]


class Ruleset(StructWrapper):
    """Class representing a Packet Filter ruleset or anchor."""

    @property
    def _struct_type(self):
        return pf_ruleset

    def __init__(self, name="", *rules, **kw):
        """Check arguments and initialize instance attributes."""
        super(Ruleset, self).__init__(**kw)
        self.name = name
        self._rules = []
        for rule in rules:
            self.append(Rule(rule))

    def append(self, *items):
        """Append one or more rules and/or tables and/or altqs."""
        self._rules.extend(filter(lambda i: isinstance(i, Rule), items))

    def insert(self, index, rule):
        """Insert a 'rule' before 'index'."""
        self._rules.insert(index, rule)

    def remove(self, index=-1):
        """Remove the rule at 'index'."""
        self._rules.pop(index)

    @property
    def rules(self):
        """Return the rules in this ruleset."""
        return self._rules

    def _to_string(self):
        """Return the string representation of the ruleset."""
        return "\n".join([Rule._to_string(rule) for rule in self._rules])


# this is defined here due to a cyclic dependency pf_anchor <-> pf_ruleset
pf_anchor._fields_ = [
    ("entry_global", POINTER(pf_anchor) * 3),   # RB_ENTRY(pf_anchor)
    ("entry_node", POINTER(pf_anchor) * 3),   # RB_ENTRY(pf_anchor)
    ("parent", POINTER(pf_anchor)),         # struct pf_anchor*
    ("children", pf_anchor_node),           # RB_HEAD(pf_anchor_node, pf_anchor)
    ("name", c_char * PF_ANCHOR_NAME_SIZE),
    ("path", c_char * MAXPATHLEN),
    ("ruleset", pf_ruleset),
    ("refcnt", c_int),
    ("match", c_int),
    ("owner", c_char * PF_OWNER_NAME_SIZE),
]
