from ctypes import Structure, POINTER, Union, c_uint32, c_uint64, c_char, c_int, c_void_p, c_uint8, c_uint
from pf.address import pf_addr_wrap, pf_addr, pf_pooladdr
from pf.altq import pf_altq
from pf.iface import IFNAMSIZ
from pf.ioccom import _IOWR, _IO, _IOR
from pf.osfp import pf_osfp_ioctl
from pf.pfvar import BufferStructure, MAXPATHLEN, PF_RULESET_MAX, pf_threshold
from pf.rule import pf_rule_xport, pf_rule_addr, pf_rule, pf_rule_ptr, PF_ANCHOR_NAME_SIZE
from pf.state import pf_state_xport, pfsync_state
from pf.status import pf_status
from pf.table import pfr_table


# ioctl parameter structures

class pfioc_pooladdr(BufferStructure):
    """
    struct pfioc_pooladdr {
        u_int32_t         action;
        u_int32_t         ticket;
        u_int32_t         nr;
        u_int32_t         r_num;
        u_int8_t         r_action;
        u_int8_t         r_last;
        u_int8_t         af;
        char             anchor[MAXPATHLEN];
        struct pf_pooladdr     addr;
    };
    """
    _fields_ = [
        ("action", c_uint32),
        ("ticket", c_uint32),
        ("nr", c_uint32),
        ("r_num", c_uint32),
        ("r_action", c_uint8),
        ("r_last", c_uint8),
        ("af", c_uint8),
        ("anchor", c_char * MAXPATHLEN),
        ("addr", pf_pooladdr)
    ]


class pfioc_rule(BufferStructure):
    """
    struct pfioc_rule {
        u_int32_t     action;
        u_int32_t     ticket;
        u_int32_t     pool_ticket;
        u_int32_t     nr;
        char         anchor[MAXPATHLEN];
        char         anchor_call[MAXPATHLEN];
        struct pf_rule     rule;
    };
    """
    _fields_ = [
        ("action", c_uint32),
        ("ticket", c_uint32),
        ("pool_ticket", c_uint32),
        ("nr", c_uint32),
        ("anchor", c_char * MAXPATHLEN),
        ("anchor_call", c_char * MAXPATHLEN),
        ("rule", pf_rule)
    ]


class pfioc_natlook(BufferStructure):
    """
    struct pfioc_natlook {
        struct pf_addr     saddr;
        struct pf_addr     daddr;
        struct pf_addr     rsaddr;
        struct pf_addr     rdaddr;
        union pf_state_xport    sxport;
        union pf_state_xport    dxport;
        union pf_state_xport    rsxport;
        union pf_state_xport    rdxport;
        sa_family_t     af;
        u_int8_t     proto;
        u_int8_t     proto_variant;
        u_int8_t     direction;
    };
    """
    _fields_ = [
        ("saddr", pf_addr),
        ("daddr", pf_addr),
        ("rsaddr", pf_addr),
        ("rdaddr", pf_addr),
        ("sxport", pf_state_xport),
        ("dxport", pf_state_xport),
        ("rsxport", pf_state_xport),
        ("rdxport", pf_state_xport),
        ("af", c_uint8),   # sa_family_t
        ("proto", c_uint8),
        ("proto_variant", c_uint8),
        ("direction", c_uint8),
    ]


class pfioc_state(BufferStructure):
    """
    struct pfioc_state {
        struct pfsync_state    state;
    };
    """
    _fields_ = [("state", pfsync_state)]


class pfioc_src_node_kill(BufferStructure):
    """
    struct pfioc_src_node_kill {
        /* XXX returns the number of src nodes killed in psnk_af */
        sa_family_t psnk_af;
        struct pf_rule_addr psnk_src;
        struct pf_rule_addr psnk_dst;
    };
    """
    _fields_ = [
        ("psnk_af", c_uint8),  # sa_family_t
        ("psnk_src", pf_rule_addr),
        ("psnk_dst", pf_rule_addr),
    ]


class pfioc_state_addr_kill(BufferStructure):
    """
    struct pfioc_state_addr_kill {
        struct pf_addr_wrap        addr;
        u_int8_t            reserved_[3];
        u_int8_t            neg;
        union pf_rule_xport        xport;
    };
    """
    _fields_ = [
        ("addr", pf_addr_wrap),
        ("reserved_", c_uint8),
        ("neg", c_uint8),
        ("xport", pf_rule_xport),
    ]


class pfioc_state_kill(BufferStructure):
    """
    struct pfioc_state_kill {
        /* XXX returns the number of states killed in psk_af */
        sa_family_t        psk_af;
        u_int8_t        psk_proto;
        u_int8_t        psk_proto_variant;
        u_int8_t        _pad;
        struct pfioc_state_addr_kill    psk_src;
        struct pfioc_state_addr_kill    psk_dst;
        char            psk_ifname[IFNAMSIZ];
    };
    """
    _fields_ = [
        ("psk_af", c_uint8),  # sa_family_t
        ("psk_proto", c_uint8),
        ("psk_proto_variant", c_uint8),
        ("_pad", c_uint8),
        ("psk_src", pfioc_state_addr_kill),
        ("psk_dst", pfioc_state_addr_kill),
        ("psk_ifname", c_char * IFNAMSIZ),
    ]


class pfioc_states(BufferStructure):
    """
    struct pfioc_states {
        int    ps_len;
        union {
            caddr_t             psu_buf;
            struct pfsync_state    *psu_states;
        } ps_u __attribute__((aligned(8)));
    #define ps_buf        ps_u.psu_buf
    #define ps_states    ps_u.psu_states
    };
    """
    class _ps_u(Union):
        _fields_ = [
            ("psu_buf", POINTER(c_char)),      # caddr_t (char*)
            ("psu_states", POINTER(pfsync_state))    # struct pfsync_state*
        ]
    _anonymous_ = ("ps_u",)
    _fields_ = [
        ("ps_len", c_int),
        ("ps_u", _ps_u)
    ]

PFTOK_PROCNAME_LEN = 64


class pfioc_token(BufferStructure):
    """
    #pragma pack(1)
    struct pfioc_token {
        u_int64_t            token_value;
        u_int64_t            timestamp;
        pid_t                pid;
        char                proc_name[PFTOK_PROCNAME_LEN];
    };
    #pragma pack()
    """
    _fields_ = [
        ("token_value", c_uint64),
        ("timestamp", c_uint64),
        ("pid", c_uint32),  # pid_t
        ("proc_name", c_char * PFTOK_PROCNAME_LEN),
    ]


class pfioc_kernel_token(BufferStructure):
    """
    struct pfioc_kernel_token {
        SLIST_ENTRY(pfioc_kernel_token)    next;
        struct pfioc_token        token;
    };
    """
    _fields_ = [
        ("next", c_void_p),     # SLIST_ENTRY(pfioc_kernel_token)
        ("token", pfioc_token),
    ]


class pfioc_remove_token(BufferStructure):
    """
    struct pfioc_remove_token {
        u_int64_t                token_value;
        u_int64_t                refcount;
    };
    """
    _fields_ = [
        ("token_value", c_uint64),
        ("refcount", c_uint64),
    ]


class pfioc_tokens(BufferStructure):
    """
    struct pfioc_tokens {
        int    size;
        union {
            caddr_t                pgtu_buf;
            struct pfioc_token        *pgtu_tokens;
        } pgt_u __attribute__((aligned(8)));
    #define pgt_buf        pgt_u.pgtu_buf
    #define pgt_tokens    pgt_u.pgtu_tokens
    };
    """
    class _pgt_u(Union):
        _fields_ = [
            ("pgtu_buf", POINTER(c_char)),     # caddr_t (char*)
            ("pgtu_tokens", POINTER(pfioc_token)),  # struct pfioc_token*
        ]
    _anonymous_ = ("pgt_u",)
    _fields_ = [
        ("size", c_int),
        ("pgt_u", _pgt_u),
    ]


PFSNODE_HIWAT = 10000   # default source node table size


class pf_src_node(BufferStructure):
    """
    struct pf_src_node {
        RB_ENTRY(pf_src_node) entry;
        struct pf_addr	 addr;
        struct pf_addr	 raddr;
        union pf_rule_ptr rule;
        void		*kif;
        u_int64_t	 bytes[2];
        u_int64_t	 packets[2];
        u_int32_t	 states;
        u_int32_t	 conn;
        struct pf_threshold	conn_rate;
        u_int64_t	 creation;
        u_int64_t	 expire;
        sa_family_t	 af;
        u_int8_t	 ruletype;
    };
    """

pf_src_node._fields_ = [
    ("entry", POINTER(pf_src_node) * 3),    # RB_ENTRY(pf_src_node)
    ("addr", pf_addr),
    ("raddr", pf_addr),
    ("rule", pf_rule_ptr),
    ("kif", c_void_p),
    ("bytes", c_uint64 * 2),
    ("packets", c_uint64 * 2),
    ("states", c_uint32),
    ("conn", c_uint32),
    ("conn_rate", pf_threshold),
    ("creation", c_uint64),
    ("expire", c_uint64),
    ("af", c_uint8),    # sa_family_t
    ("ruletype", c_uint8)
]


class pfioc_src_nodes(BufferStructure):
    """
    struct pfioc_src_nodes {
        int    psn_len;
        union {
            caddr_t            psu_buf;
            struct pf_src_node    *psu_src_nodes;
        } psn_u __attribute__((aligned(8)));
    #define psn_buf        psn_u.psu_buf
    #define psn_src_nodes    psn_u.psu_src_nodes
    };
    """
    class _psn_u(Union):
        _fields_ = [
            ("psu_buf", POINTER(c_char)),          # caddr_t (char*)
            ("psu_src_nodes", POINTER(pf_src_node)),    # struct pf_src_node*
        ]
    _anonymous_ = ("psn_u",)
    _fields_ = [
        ("psn_len", c_int),
        ("psn_u", _psn_u),
    ]


class pfioc_if(BufferStructure):
    """
    struct pfioc_if {
        char         ifname[IFNAMSIZ];
    };
    """
    _fields_ = [
        ("ifname", c_char * IFNAMSIZ),
    ]


class pfioc_tm(BufferStructure):
    """
    struct pfioc_tm {
        int         timeout;
        int         seconds;
    };
    """
    _fields_ = [
        ("timeout", c_int),
        ("seconds", c_int),
    ]


class pfioc_limit(BufferStructure):
    """
    struct pfioc_limit {
        int         index;
        unsigned     limit;
    };
    """
    _fields_ = [
        ("index", c_int),
        ("limit", c_uint),
    ]


class pfioc_altq(BufferStructure):
    """
    struct pfioc_altq {
        u_int32_t     action;
        u_int32_t     ticket;
        u_int32_t     nr;
        struct pf_altq     altq            __attribute__((aligned(8)));
    };
    """
    _fields_ = [
        ("action", c_uint32),
        ("ticket", c_uint32),
        ("nr", c_uint32),
        ("altq", pf_altq),
    ]


class pfioc_qstats(BufferStructure):
    """
    struct pfioc_qstats {
        u_int32_t     ticket;
        u_int32_t     nr;
        void        *buf            __attribute__((aligned(8)));
        int         nbytes            __attribute__((aligned(8)));
        u_int8_t     scheduler;
    };
    """
    _fields_ = [
        ("ticket", c_uint32),
        ("nr", c_uint32),
        ("buf", c_void_p),
        ("nbytes", c_int),
        ("scheduler", c_uint8),
    ]


class pfioc_ruleset(BufferStructure):
    """
    struct pfioc_ruleset {
        u_int32_t     nr;
        char         path[MAXPATHLEN];
        char         name[PF_ANCHOR_NAME_SIZE];
    };
    """
    _fields_ = [
        ("nr", c_uint32),
        ("path", c_char * MAXPATHLEN),
        ("name", c_char * PF_ANCHOR_NAME_SIZE),
    ]

PF_RULESET_ALTQ = PF_RULESET_MAX
PF_RULESET_TABLE = (PF_RULESET_MAX + 1)


class pfioc_trans_e(Structure):
    """
    struct pfioc_trans_e {
        int        rs_num;
        char        anchor[MAXPATHLEN];
        u_int32_t    ticket;
    };
    """
    _fields_ = [
        ("rs_num", c_int),
        ("anchor", c_char * MAXPATHLEN),
        ("ticket", c_uint32),
    ]


class pfioc_trans(BufferStructure):
    """
    struct pfioc_trans {
        int         size;    /* number of elements */
        int         esize; /* size of each element in bytes */
        struct pfioc_trans_e* array __attribute__((aligned(8)));
    };
    """
    _fields_ = [
        ("size", c_int),
        ("esize", c_int),
        ("array", POINTER(pfioc_trans_e)),    # struct pfioc_trans_e*
    ]

PFR_FLAG_ATOMIC = 0x00000001
PFR_FLAG_DUMMY = 0x00000002
PFR_FLAG_FEEDBACK = 0x00000004
PFR_FLAG_CLSTATS = 0x00000008
PFR_FLAG_ADDRSTOO = 0x00000010
PFR_FLAG_REPLACE = 0x00000020
PFR_FLAG_ALLRSETS = 0x00000040
PFR_FLAG_ALLMASK = 0x0000007F


class pfioc_table(BufferStructure):
    """
    struct pfioc_table {
        struct pfr_table     pfrio_table;
        void            *pfrio_buffer    __attribute__((aligned(8)));
        int             pfrio_esize    __attribute__((aligned(8)));
        int             pfrio_size;
        int             pfrio_size2;
        int             pfrio_nadd;
        int             pfrio_ndel;
        int             pfrio_nchange;
        int             pfrio_flags;
        u_int32_t         pfrio_ticket;
    };
    """
    _fields_ = [
        ("pfrio_table", pfr_table),
        ("pfrio_buffer", c_void_p),
        ("pfrio_esize", c_int),
        ("pfrio_size", c_int),
        ("pfrio_size2", c_int),
        ("pfrio_nadd", c_int),
        ("pfrio_ndel", c_int),
        ("pfrio_nchange", c_int),
        ("pfrio_flags", c_int),
        ("pfrio_ticket", c_uint32),
        ]


class pfioc_iface(BufferStructure):
    """
    struct pfioc_iface {
        char     pfiio_name[IFNAMSIZ];
        void    *pfiio_buffer            __attribute__((aligned(8)));
        int     pfiio_esize            __attribute__((aligned(8)));
        int     pfiio_size;
        int     pfiio_nzero;
        int     pfiio_flags;
    };
    """
    _fields_ = [
        ("pfiio_name", c_char * IFNAMSIZ),
        ("pfiio_buffer", c_void_p),
        ("pfiio_esize", c_int),
        ("pfiio_size", c_int),
        ("pfiio_nzero", c_int),
        ("pfiio_flags", c_int),
    ]


class pf_ifspeed(Structure):
    """
    struct pf_ifspeed {
        char            ifname[IFNAMSIZ];
        u_int64_t        baudrate;
    };
    """
    _fields_ = [
        ("ifname", c_char * IFNAMSIZ),
        ("baudrate", c_uint64),
    ]


DIOCSTART = _IO('D', 1)
DIOCSTOP = _IO('D', 2)
DIOCADDRULE = _IOWR('D', 4, pfioc_rule)
DIOCGETSTARTERS = _IOWR('D', 5, pfioc_tokens)
DIOCGETRULES = _IOWR('D', 6, pfioc_rule)
DIOCGETRULE = _IOWR('D', 7, pfioc_rule)
DIOCSTARTREF = _IOR('D', 8, c_uint64)
DIOCSTOPREF = _IOWR('D', 9, pfioc_remove_token)
# XXX cut 10 - 17
DIOCCLRSTATES = _IOWR('D', 18, pfioc_state_kill)
DIOCGETSTATE = _IOWR('D', 19, pfioc_state)
DIOCSETSTATUSIF = _IOWR('D', 20, pfioc_if)
DIOCGETSTATUS = _IOWR('D', 21, pf_status)
DIOCCLRSTATUS = _IO('D', 22)
DIOCNATLOOK = _IOWR('D', 23, pfioc_natlook)
DIOCSETDEBUG = _IOWR('D', 24, c_uint32)
DIOCGETSTATES = _IOWR('D', 25, pfioc_states)
DIOCCHANGERULE = _IOWR('D', 26, pfioc_rule)
DIOCINSERTRULE = _IOWR('D',  27, pfioc_rule)
DIOCDELETERULE = _IOWR('D',  28, pfioc_rule)
DIOCSETTIMEOUT = _IOWR('D', 29, pfioc_tm)
DIOCGETTIMEOUT = _IOWR('D', 30, pfioc_tm)
DIOCADDSTATE = _IOWR('D', 37, pfioc_state)
DIOCCLRRULECTRS = _IO('D', 38)
DIOCGETLIMIT = _IOWR('D', 39, pfioc_limit)
DIOCSETLIMIT = _IOWR('D', 40, pfioc_limit)
DIOCKILLSTATES = _IOWR('D', 41, pfioc_state_kill)
DIOCSTARTALTQ = _IO('D', 42)
DIOCSTOPALTQ = _IO('D', 43)
DIOCADDALTQ = _IOWR('D', 45, pfioc_altq)
DIOCGETALTQS = _IOWR('D', 47, pfioc_altq)
DIOCGETALTQ = _IOWR('D', 48, pfioc_altq)
DIOCCHANGEALTQ = _IOWR('D', 49, pfioc_altq)
DIOCGETQSTATS = _IOWR('D', 50, pfioc_qstats)
DIOCBEGINADDRS = _IOWR('D', 51, pfioc_pooladdr)
DIOCADDADDR = _IOWR('D', 52, pfioc_pooladdr)
DIOCGETADDRS = _IOWR('D', 53, pfioc_pooladdr)
DIOCGETADDR = _IOWR('D', 54, pfioc_pooladdr)
DIOCCHANGEADDR = _IOWR('D', 55, pfioc_pooladdr)
# XXX cut 55 - 57
DIOCGETRULESETS = _IOWR('D', 58, pfioc_ruleset)
DIOCGETRULESET = _IOWR('D', 59, pfioc_ruleset)
DIOCRCLRTABLES = _IOWR('D', 60, pfioc_table)
DIOCRADDTABLES = _IOWR('D', 61, pfioc_table)
DIOCRDELTABLES = _IOWR('D', 62, pfioc_table)
DIOCRGETTABLES = _IOWR('D', 63, pfioc_table)
DIOCRGETTSTATS = _IOWR('D', 64, pfioc_table)
DIOCRCLRTSTATS = _IOWR('D', 65, pfioc_table)
DIOCRCLRADDRS = _IOWR('D', 66, pfioc_table)
DIOCRADDADDRS = _IOWR('D', 67, pfioc_table)
DIOCRDELADDRS = _IOWR('D', 68, pfioc_table)
DIOCRSETADDRS = _IOWR('D', 69, pfioc_table)
DIOCRGETADDRS = _IOWR('D', 70, pfioc_table)
DIOCRGETASTATS = _IOWR('D', 71, pfioc_table)
DIOCRCLRASTATS = _IOWR('D', 72, pfioc_table)
DIOCRTSTADDRS = _IOWR('D', 73, pfioc_table)
DIOCRSETTFLAGS = _IOWR('D', 74, pfioc_table)
DIOCRINADEFINE = _IOWR('D', 77, pfioc_table)
DIOCOSFPFLUSH = _IO('D', 78)
DIOCOSFPADD = _IOWR('D', 79, pf_osfp_ioctl)
DIOCOSFPGET = _IOWR('D', 80, pf_osfp_ioctl)
DIOCXBEGIN = _IOWR('D', 81, pfioc_trans)
DIOCXCOMMIT = _IOWR('D', 82, pfioc_trans)
DIOCXROLLBACK = _IOWR('D', 83, pfioc_trans)
DIOCGETSRCNODES = _IOWR('D', 84, pfioc_src_nodes)
DIOCCLRSRCNODES = _IO('D', 85)
DIOCSETHOSTID = _IOWR('D', 86, c_uint32)
DIOCIGETIFACES = _IOWR('D', 87, pfioc_iface)
DIOCSETIFFLAG = _IOWR('D', 89, pfioc_iface)
DIOCCLRIFFLAG = _IOWR('D', 90, pfioc_iface)
DIOCKILLSRCNODES = _IOWR('D', 91, pfioc_src_node_kill)
DIOCGIFSPEED = _IOWR('D', 92, pf_ifspeed)
