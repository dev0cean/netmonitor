from ctypes import Union, Structure, addressof, c_uint8, c_char, c_short, c_int, c_char_p, c_uint32, c_uint64, \
    c_ubyte, c_ushort, c_uint, c_void_p
from fcntl import ioctl
from socket import socket, AF_INET, SOCK_DGRAM
import time
from pf.base import StructWrapper
from pf.pfvar import PF_PASS, PF_DROP, PFR_DIR_IN, PFR_DIR_OUT, PFI_IFLAG_SKIP, BufferStructure
from pf.util import timeval


IFNAMSIZ = 16   # from bsd/net/if.h

IFRIFOF_BLOCK_OPPORTUNISTIC = 0x00000001

IFRLOGF_DLIL = 0x00000001
IFRLOGF_FAMILY = 0x00010000
IFRLOGF_DRIVER = 0x01000000
IFRLOGF_FIRMWARE = 0x10000000

IFRLOGCAT_CONNECTIVITY = 1
IFRLOGCAT_QUALITY = 2
IFRLOGCAT_PERFORMANCE = 3

IFRTYPE_FAMILY_ANY = 0
IFRTYPE_FAMILY_LOOPBACK = 1
IFRTYPE_FAMILY_ETHERNET = 2
IFRTYPE_FAMILY_SLIP = 3
IFRTYPE_FAMILY_TUN = 4
IFRTYPE_FAMILY_VLAN = 5
IFRTYPE_FAMILY_PPP = 6
IFRTYPE_FAMILY_PVC = 7
IFRTYPE_FAMILY_DISC = 8
IFRTYPE_FAMILY_MDECAP = 9
IFRTYPE_FAMILY_GIF = 10
IFRTYPE_FAMILY_FAITH = 11
IFRTYPE_FAMILY_STF = 12
IFRTYPE_FAMILY_FIREWIRE = 13
IFRTYPE_FAMILY_BOND = 14
IFRTYPE_FAMILY_CELLULAR = 15

IFRTYPE_SUBFAMILY_ANY = 0
IFRTYPE_SUBFAMILY_USB = 1
IFRTYPE_SUBFAMILY_BLUETOOTH = 2
IFRTYPE_SUBFAMILY_WIFI = 3
IFRTYPE_SUBFAMILY_THUNDERBOLT = 4


class ifdevmtu(BufferStructure):
    """
    /*
     * ifdevmtu: interface device mtu
     *    Used with SIOCGIFDEVMTU to get the current mtu in use by the device,
     *    as well as the minimum and maximum mtu allowed by the device.
     */
    struct ifdevmtu {
        int	ifdm_current;
        int	ifdm_min;
        int	ifdm_max;
    };
    """
    _fields_ = [
        ("ifdm_current", c_int),
        ("ifdm_min", c_int),
        ("ifdm_max", c_int),
    ]


class ifkpi(BufferStructure):
    """
    /*
     ifkpi: interface kpi ioctl
     Used with SIOCSIFKPI and SIOCGIFKPI.

     ifk_module_id - From in the kernel, a value from kev_vendor_code_find. From
        user space, a value from SIOCGKEVVENDOR ioctl on a kernel event socket.
     ifk_type - The type. Types are specific to each module id.
     ifk_data - The data. ifk_ptr may be a 64bit pointer for 64 bit processes.

     Copying data between user space and kernel space is done using copyin
     and copyout. A process may be running in 64bit mode. In such a case,
     the pointer will be a 64bit pointer, not a 32bit pointer. The following
     sample is a safe way to copy the data in to the kernel from either a
     32bit or 64bit process:

     user_addr_t tmp_ptr;
     if (IS_64BIT_PROCESS(current_proc())) {
        tmp_ptr = CAST_USER_ADDR_T(ifkpi.ifk_data.ifk_ptr64);
     }
     else {
        tmp_ptr = CAST_USER_ADDR_T(ifkpi.ifk_data.ifk_ptr);
     }
     error = copyin(tmp_ptr, allocated_dst_buffer, size of allocated_dst_buffer);
     */

    struct ifkpi {
        unsigned int	ifk_module_id;
        unsigned int	ifk_type;
        union {
            void		*ifk_ptr;
            int		ifk_value;
        } ifk_data;
    };
    """
    class _ifk_data(Union):
        _fields_ = [
            ("ifk_ptr", c_void_p),
            ("ifk_value", c_int),
        ]
    _fields_ = [
        ("ifk_module_id", c_uint),
        ("ifk_type", c_uint),
        ("ifk_data", _ifk_data),
    ]


class ifreq(BufferStructure):   # from bsd/net/if.h
    """
    /*
     * Interface request structure used for socket
     * ioctl's.  All interface ioctl's must have parameter
     * definitions which begin with ifr_name.  The
     * remainder may be interface specific.
     */
    struct	ifreq {
        char	ifr_name[IFNAMSIZ];		/* if name, e.g. "en0" */
        union {
            struct	sockaddr ifru_addr;
            struct	sockaddr ifru_dstaddr;
            struct	sockaddr ifru_broadaddr;
            short	ifru_flags;
            int	ifru_metric;
            int	ifru_mtu;
            int	ifru_phys;
            int	ifru_media;
            int	ifru_intval;
            caddr_t	ifru_data;
            struct	ifdevmtu ifru_devmtu;
            struct	ifkpi	ifru_kpi;
            u_int32_t ifru_wake_flags;
            u_int32_t ifru_route_refcnt;
            int	ifru_cap[2];
        } ifr_ifru;

        #define	ifr_addr	ifr_ifru.ifru_addr	/* address */
        #define	ifr_dstaddr	ifr_ifru.ifru_dstaddr	/* other end of p-to-p link */
        #define	ifr_broadaddr	ifr_ifru.ifru_broadaddr	/* broadcast address */
#ifdef __APPLE__
        #define	ifr_flags	ifr_ifru.ifru_flags	/* flags */
#else
        #define	ifr_flags	ifr_ifru.ifru_flags[0]	/* flags */
        #define	ifr_prevflags	ifr_ifru.ifru_flags[1]	/* flags */
#endif /* __APPLE__ */
        #define	ifr_metric	ifr_ifru.ifru_metric	/* metric */
        #define	ifr_mtu		ifr_ifru.ifru_mtu	/* mtu */
        #define	ifr_phys	ifr_ifru.ifru_phys	/* physical wire */
        #define	ifr_media	ifr_ifru.ifru_media	/* physical media */
        #define	ifr_data	ifr_ifru.ifru_data	/* for use by interface */
        #define	ifr_devmtu	ifr_ifru.ifru_devmtu
        #define	ifr_intval	ifr_ifru.ifru_intval	/* integer value */
        #define	ifr_kpi		ifr_ifru.ifru_kpi
        #define	ifr_wake_flags	ifr_ifru.ifru_wake_flags /* wake capabilities */
        #define	ifr_route_refcnt ifr_ifru.ifru_route_refcnt /* route references count */
        #define	ifr_reqcap	ifr_ifru.ifru_cap[0]	/* requested capabilities */
        #define	ifr_curcap	ifr_ifru.ifru_cap[1]	/* current capabilities */
    };

    """
    _anonymous_ = ("ifr_ifru",)

    class _ifr_ifru(Union):
        class _sockaddr(Structure):     # From /usr/include/sys/socket.h
            """
            struct sockaddr {
                __uint8_t	sa_len;		/* total length */
                sa_family_t	sa_family;	/* [XSI] address family */
                char		sa_data[14];	/* [XSI] addr value (actually larger) */
            };
            """
            _fields_ = [("sa_len",    c_uint8),
                        ("sa_family", c_uint8),       # sa_family_t
                        ("sa_data",   c_char * 14)]

        """
        union {
            struct	sockaddr ifru_addr;
            struct	sockaddr ifru_dstaddr;
            struct	sockaddr ifru_broadaddr;
            short	ifru_flags;
            int	ifru_metric;
            int	ifru_mtu;
            int	ifru_phys;
            int	ifru_media;
            int	ifru_intval;
            caddr_t	ifru_data;
            struct	ifdevmtu ifru_devmtu;
            struct	ifkpi	ifru_kpi;
            u_int32_t ifru_wake_flags;
            u_int32_t ifru_route_refcnt;
            int	ifru_cap[2];
        } ifr_ifru;
        """
        _fields_ = [
            ("ifru_addr", _sockaddr),
            ("ifru_dstaddr", _sockaddr),
            ("ifru_broadaddr", _sockaddr),
            ("ifru_flags", c_short),
            ("ifru_metric", c_int),
            ("ifru_mtu", c_int),
            ("ifru_phys", c_int),
            ("ifru_media", c_int),
            ("ifru_intval", c_int),
            ("ifru_data", c_char_p),    # caddr_t
            ("ifru_devmtu", ifdevmtu),
            ("ifru_kpi", ifkpi),
            ("ifru_wake_flags", c_uint32),
            ("ifru_route_refcnt", c_uint32),
            ("ifru_cap", c_int * 2),
        ]

    _fields_ = [
        ("ifr_name", c_char * IFNAMSIZ),
        ("ifr_ifru", _ifr_ifru),
    ]


class if_data(Structure):               # From /usr/include/net/if.h
    _MCLPOOLS = 7

    class _mclpool(Structure):          # From /usr/include/net/if.h
        _fields_ = [("mcl_grown",    c_uint),
                    ("mcl_alive",    c_ushort),
                    ("mcl_hwm",      c_ushort),
                    ("mcl_cwm",      c_ushort),
                    ("mcl_lwm",      c_ushort)]

    _fields_ = [("ifi_type",         c_ubyte),
                ("ifi_addrlen",      c_ubyte),
                ("ifi_hdrlen",       c_ubyte),
                ("ifi_link_state",   c_ubyte),
                ("ifi_mtu",          c_uint32),
                ("ifi_metric",       c_uint32),
                ("ifi_pad",          c_uint32),
                ("ifi_baudrate",     c_uint64),
                ("ifi_ipackets",     c_uint64),
                ("ifi_ierrors",      c_uint64),
                ("ifi_opackets",     c_uint64),
                ("ifi_oerrors",      c_uint64),
                ("ifi_collisions",   c_uint64),
                ("ifi_ibytes",       c_uint64),
                ("ifi_obytes",       c_uint64),
                ("ifi_imcasts",      c_uint64),
                ("ifi_omcasts",      c_uint64),
                ("ifi_iqdrops",      c_uint64),
                ("ifi_noproto",      c_uint64),
                ("ifi_capabilities", c_uint32),
                ("ifi_lastchange",   timeval),
                ("ifi_mclpool",      _mclpool * _MCLPOOLS)]


class pfi_kif(Structure):
    """
    struct pfi_kif {
        char                 pfik_name[IFNAMSIZ];
        u_int64_t             pfik_packets[2][2][2];
        u_int64_t             pfik_bytes[2][2][2];
        u_int64_t             pfik_tzero;
        int                 pfik_flags;
        int                 pfik_states;
        int                 pfik_rules;
    };
    """
    _fields_ = [
        ("pfik_name", c_char * IFNAMSIZ),
        ("pfik_packets", c_uint64 * 2 * 2 * 2),
        ("pfik_bytes", c_uint64 * 2 * 2 * 2),
        ("pfik_tzero", c_uint64),
        ("pfik_flags", c_int),
        ("pfik_states", c_int),
        ("pfik_rules", c_int),
    ]


class Interface(StructWrapper):
    """Class representing a network interface."""

    @property
    def _prefix(self):
        return "pfik_"

    @property
    def _struct_type(self):
        return pfi_kif

    def _to_string(self):
        """Return a string containing the description of the interface."""
        if self.flags & PFI_IFLAG_SKIP:
            s = "{.name} (skip)\n".format(self)
        else:
            s = "{.name}\n".format(self)
        s += "\tCleared:     {}\n".format(time.ctime(self.tzero))
        s += "\tReferences:  [ States:  {.states:<18d}".format(self)
        s += " Rules: {.rules:<18d} ]\n".format(self)

        for proto, proto_label in enumerate([4, 6]):
            for dirn, dirn_label in [(PFR_DIR_IN, "In"), (PFR_DIR_OUT, "Out")]:
                for action, action_label in [(PF_PASS, "Pass"), (PF_DROP, "Block")]:
                    label = "%s%i/%s:" % (dirn_label, proto_label, action_label)
                    s += "\t{:<13s}[ Packets: {:<18d} Bytes: {:<18d} ]\n".format(
                        label, self.packets[proto][dirn][action], self.bytes[proto][dirn][action])

        return s


def get_if_mtu(ifname):
    """Quick hack to get MTU and speed for a specified interface."""
    from pf.filter import _IOWR
    SIOCGIFMTU = _IOWR('i', 126, ifreq)
    s = socket(AF_INET, SOCK_DGRAM)
    ifrdat = if_data()
    ifr = ifreq(ifr_name=ifname, ifru_data=addressof(ifrdat))

    try:
        ioctl(s, SIOCGIFMTU, ifr.asBuffer())
    except IOError:
        pass

    s.close()
    mtu = (ifr.ifru_metric if (ifr.ifru_metric > 0) else 1500)
    speed = ifrdat.ifi_baudrate

    return mtu, speed
