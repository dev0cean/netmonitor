from ctypes import c_uint64, c_uint32, c_char, c_uint8
import socket
import time
from pf.base import StructWrapper
from pf.enum import LOG_LEVELS
from pf.iface import IFNAMSIZ
from pf.pfvar import PFRES_NAMES, LCNT_NAMES, FCNT_NAMES, SCNT_NAMES, PFR_DIR_IN, PFR_DIR_OUT, PF_PASS, PF_DROP, \
    PFRES_MAX, LCNT_MAX, FCNT_MAX, SCNT_MAX, PFR_DIR_MAX, PF_MD5_DIGEST_LENGTH, BufferStructure


class pf_status(BufferStructure):
    """
    struct pf_status {
        u_int64_t    counters[PFRES_MAX];
        u_int64_t    lcounters[LCNT_MAX];    /* limit counters */
        u_int64_t    fcounters[FCNT_MAX];
        u_int64_t    scounters[SCNT_MAX];
        u_int64_t    pcounters[2][2][3];
        u_int64_t    bcounters[2][2];
        u_int64_t    stateid;
        u_int32_t    running;
        u_int32_t    states;
        u_int32_t    src_nodes;
        u_int64_t    since            __attribute__((aligned(8)));
        u_int32_t    debug;
        u_int32_t    hostid;
        char        ifname[IFNAMSIZ];
        u_int8_t    pf_chksum[PF_MD5_DIGEST_LENGTH];
    };
    """
    _fields_ = [
        ("counters", c_uint64 * PFRES_MAX),
        ("lcounters", c_uint64 * LCNT_MAX),
        ("fcounters", c_uint64 * FCNT_MAX),
        ("scounters", c_uint64 * SCNT_MAX),
        ("pcounters", c_uint64 * 2 * PFR_DIR_MAX * 3),
        ("bcounters", c_uint64 * 2 * PFR_DIR_MAX),
        ("stateid", c_uint64),
        ("running", c_uint32),
        ("states", c_uint32),
        ("src_nodes", c_uint32),
        ("since", c_uint32),
        ("debug", c_uint32),
        ("hostid", c_uint32),
        ("ifname", c_char * IFNAMSIZ),
        ("pf_chksum", c_uint8 * PF_MD5_DIGEST_LENGTH),
    ]


class Status(StructWrapper):
    """Class representing the internal Packet Filter statistics and counters."""

    @property
    def _prefix(self):
        return ''

    @property
    def _struct_type(self):
        return pf_status

    # noinspection PyAttributeOutsideInit
    def _from_struct(self, struct):
        """Initialize class attributes from a pf_status structure."""
        self.running = bool(struct.running)
        self.hostid = socket.ntohl(struct.hostid) & 0xffffffff
        self.checksum = "0x" + "".join(map("{:02x}".format, struct.pf_chksum))
        self.counters = dict(zip(PFRES_NAMES, struct.counters))
        self.lcounters = dict(zip(LCNT_NAMES, struct.lcounters))
        self.fcounters = dict(zip(FCNT_NAMES, struct.fcounters))
        self.scounters = dict(zip(SCNT_NAMES, struct.scounters))
        self.bytes = {'in': (struct.bcounters[0][PFR_DIR_IN], struct.bcounters[1][PFR_DIR_IN]),
                      'out':  (struct.bcounters[0][PFR_DIR_OUT], struct.bcounters[1][PFR_DIR_OUT])}
        self.packets = {'in':  ((struct.pcounters[0][0][PF_PASS], struct.pcounters[1][0][PF_PASS]),
                                (struct.pcounters[0][0][PF_DROP], struct.pcounters[1][0][PF_DROP])),
                        'out': ((struct.pcounters[0][1][PF_PASS], struct.pcounters[1][1][PF_PASS]),
                                (struct.pcounters[0][1][PF_DROP], struct.pcounters[1][1][PF_DROP]))}

    def _to_string(self):
        """Return a string containing the statistics."""
        s = "Status: " + ('Enabled' if self.running else 'Disabled')
        if self.since:
            runtime = int(time.time()) - self.since
            days, seconds = divmod(runtime, 60)
            days, minutes = divmod(days, 60)
            days, hours = divmod(days, 24)
            s += " for {} days {:02}:{:02}:{:02}".format(days, hours, minutes, seconds)

        dbg = next((k for k, v in LOG_LEVELS.iteritems() if v == self.debug), "unknown")
        s = "{:<44}{:>15}\n\n".format(s, "Debug: " + dbg)
        s += "Hostid:   0x{.hostid:08x}\n".format(self)
        s += "Checksum: {.pf_chksum}\n\n".format(self)

        if self.ifname:
            fmt = "  {0:<25} {1[0]:>14d} {1[1]:>16d}\n"
            s += "Interface Stats for {.ifname:<16} ".format(self)
            s += "{:>5} {:>16}\n".format("IPv4", "IPv6")
            s += fmt.format("Bytes In", self.bytes["in"])
            s += fmt.format("Bytes Out", self.bytes["out"])
            s += "  Packets In\n"
            s += fmt.format("  Passed", self.packets["in"][PF_PASS])
            s += fmt.format("  Blocked", self.packets["in"][PF_DROP])
            s += "  Packets Out\n"
            s += fmt.format("  Passed", self.packets["out"][PF_PASS])
            s += fmt.format("  Blocked", self.packets["out"][PF_DROP])
            s += "\n"

        s += "{:<27} {:>14} {:>16}\n".format("State Table", "Total", "Rate")
        s += "  {:<25} {.states:>14d}".format("current entries", self)
        for k, v in self.fcnt.iteritems():
            s += "\n  {:<25} {:>14d} ".format(k, v)
            if self.since and runtime:
                s += "{:>14.1f}/s".format(float(v)/runtime)

        s += "\nSource Tracking Table\n"
        s += "  {:<25} {.src_nodes:>14d}".format("current entries", self)
        for k, v in self.scnt.iteritems():
            s += "\n  {:<25} {:>14d} ".format(k, v)
            if self.since and runtime:
                s += "{:>14.1f}/s".format(float(v)/runtime)

        s += "\nCounters"
        for k, v in self.cnt.iteritems():
            s += "\n  {:<25} {:>14d} ".format(k, v)
            if self.since and runtime:
                s += "{:>14.1f}/s".format(float(v)/runtime)

        s += "\nLimit Counters"
        for k, v in self.lcnt.iteritems():
            s += "\n  {:<25} {:>14d} ".format(k, v)
            if self.since and runtime:
                s += "{:>14.1f}/s".format(float(v)/runtime)

        return s
