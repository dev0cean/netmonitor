from ctypes import Structure, Union, c_void_p, c_char, c_uint32, c_uint64, c_int32
from pf.iface import IFNAMSIZ
from pf.pfvar import PF_QNAME_SIZE
from pf.queue import TAILQ_ENTRY


class cbq_opts(Structure):
    """
    struct cbq_opts {
        u_int32_t    minburst;
        u_int32_t    maxburst;
        u_int32_t    pktsize;
        u_int32_t    maxpktsize;
        u_int32_t    ns_per_byte;
        u_int32_t    maxidle;
        int32_t        minidle;
        u_int32_t    offtime;
        u_int32_t    flags;
    };
    """
    _fields_ = [
        ("minburst", c_uint32),
        ("maxburst", c_uint32),
        ("pktsize", c_uint32),
        ("maxpktsize", c_uint32),
        ("ns_per_byte", c_uint32),
        ("maxidle", c_uint32),
        ("minidle", c_int32),
        ("offtime", c_uint32),
        ("flags", c_uint32),
    ]


class priq_opts(Structure):
    """
    struct priq_opts {
        u_int32_t    flags;
    };
    """
    _fields_ = [
        ("flags", c_uint32),
    ]


class qfq_opts(Structure):
    """
    struct qfq_opts {
        u_int32_t    flags;
        u_int32_t    lmax;
    };
    """
    _fields_ = [
        ("flags", c_uint32),
        ("lmax", c_uint32),
    ]


class hfsc_opts(Structure):
    """
    struct hfsc_opts {
        /* real-time service curve */
        u_int64_t    rtsc_m1;    /* slope of the 1st segment in bps */
        u_int64_t    rtsc_d;        /* the x-projection of m1 in msec */
        u_int64_t    rtsc_m2;    /* slope of the 2nd segment in bps */
        u_int32_t    rtsc_fl;    /* service curve flags */
    #if !defined(__LP64__)
        u_int32_t    _pad;
    #endif /* !__LP64__ */
        /* link-sharing service curve */
        u_int64_t    lssc_m1;
        u_int64_t    lssc_d;
        u_int64_t    lssc_m2;
        u_int32_t    lssc_fl;
    #if !defined(__LP64__)
        u_int32_t    __pad;
    #endif /* !__LP64__ */
        /* upper-limit service curve */
        u_int64_t    ulsc_m1;
        u_int64_t    ulsc_d;
        u_int64_t    ulsc_m2;
        u_int32_t    ulsc_fl;
        u_int32_t    flags;        /* scheduler flags */
    };
    """
    _fields_ = [
        ("rtsc_m1", c_uint64),
        ("rtsc_d", c_uint64),
        ("rtsc_m2", c_uint64),
        ("rtsc_fl", c_uint32),
        ("lssc_m1", c_uint64),
        ("lssc_d", c_uint64),
        ("lssc_m2", c_uint64),
        ("lssc_fl", c_uint32),
        ("ulsc_m1", c_uint64),
        ("ulsc_d", c_uint64),
        ("ulsc_m2", c_uint64),
        ("ulsc_fl", c_uint32),
        ("flags", c_uint32),
    ]


class fairq_opts(Structure):
    """
    struct fairq_opts {
        u_int32_t    nbuckets;    /* hash buckets */
        u_int32_t    flags;
        u_int64_t    hogs_m1;    /* hog detection bandwidth */

        /* link-sharing service curve */
        u_int64_t    lssc_m1;
        u_int64_t    lssc_d;
        u_int64_t    lssc_m2;
    };
    """
    _fields_ = [
        ("nbuckets", c_uint32),
        ("flags", c_uint32),
        ("hogs_m1", c_uint64),
        ("lssc_m1", c_uint64),
        ("lssc_d", c_uint64),
        ("lssc_m2", c_uint64),
    ]


class pf_altq(Structure):
    """
    struct pf_altq {
        char             ifname[IFNAMSIZ];

        /* discipline-specific state */
        void            *altq_disc __attribute__((aligned(8)));
        TAILQ_ENTRY(pf_altq)     entries __attribute__((aligned(8)));
    #if !defined(__LP64__)
        u_int32_t        _pad[2];
    #endif /* !__LP64__ */

        u_int32_t         aflags;    /* ALTQ rule flags */
        u_int32_t         bwtype;    /* bandwidth type */

        /* scheduler spec */
        u_int32_t         scheduler;    /* scheduler type */
        u_int32_t         tbrsize;    /* tokenbucket regulator size */
        u_int64_t         ifbandwidth;    /* interface bandwidth */

        /* queue spec */
        char             qname[PF_QNAME_SIZE];    /* queue name */
        char             parent[PF_QNAME_SIZE];    /* parent name */
        u_int32_t         parent_qid;    /* parent queue id */
        u_int32_t         qrflags;    /* queue rule flags */
        union {
            u_int32_t     priority;    /* priority */
            u_int32_t     weight;    /* weight */
        };
        u_int32_t         qlimit;    /* queue size limit */
        u_int32_t         flags;        /* misc flags */
    #if !defined(__LP64__)
        u_int32_t        __pad;
    #endif /* !__LP64__ */
        u_int64_t         bandwidth;    /* queue bandwidth */
        union {
            struct cbq_opts         cbq_opts;
            struct priq_opts     priq_opts;
            struct hfsc_opts     hfsc_opts;
            struct fairq_opts     fairq_opts;
            struct qfq_opts         qfq_opts;
        } pq_u;

        u_int32_t         qid;        /* return value */
    };
    """
    _anonymous_ = ("_priority_weight",)

pf_altq._fields_ = [
    ("ifname", c_char * IFNAMSIZ),
    ("altq_disc", c_void_p),
    ("entries", c_void_p * 2),  # TAILQ_ENTRY(pf_altq)
    ("aflags", c_uint32),
    ("bwtype", c_uint32),
    ("scheduler", c_uint32),
    ("tbrsize", c_uint32),
    ("ifbandwidth", c_uint64),
    ("qname", c_char * PF_QNAME_SIZE),
    ("parent", c_char * PF_QNAME_SIZE),
    ("parent_qid", c_uint32),
    ("qrflags", c_uint32),
    ("_priority_weight", type("_priority_weight", (Union,), {
        "_fields_": [
            ("priority", c_uint32),
            ("weight", c_uint32)
        ]})),
    ("qlimit", c_uint32),
    ("flags", c_uint32),
    ("bandwidth", c_uint64),
    ("pq_u", type("_pq_u", (Union,), {
        "_fields_": [
            ("cbq_opts", cbq_opts),
            ("priq_opts", priq_opts),
            ("hfsc_opts", hfsc_opts),
            ("fairq_opts", fairq_opts),
            ("qfq_opts", qfq_opts),
            ]})),
    ("qid", c_uint32),
]


