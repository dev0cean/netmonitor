from ctypes import Structure, c_int64, c_long
import re
from socket import inet_pton, inet_ntop, AF_INET, AF_INET6


class timeval(Structure):   # From /usr/include/sys/time.h
    _fields_ = [
        ("tv_sec", c_int64),    # time_t
        ("tv_usec", c_long)     # suseconds_t
    ]


def ctonm(cidr, af):
    """Convert netmask from CIDR to dotted decimal notation."""
    # cton then ntop
    try:
        broadcast, bits = {AF_INET: (0xFFFFFFFF, 32), AF_INET6: (0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF, 128)}.get(af)
        if cidr > bits:
            raise ValueError("Invalid CIDR mask")
        mask = (broadcast >> cidr) ^ broadcast
        packed = bytearray()
        for i in range(bits/8):
            packed.insert(0, mask % 256)
            mask >>= 8
        return inet_ntop(af, str(packed))
    except KeyError:
        raise ValueError("Invalid address family")


def nmtoc(netmask, af):
    """Convert netmask from dotted decimal to CIDR notation."""
    #pton then ntoc
    cidr = 0
    for b in map(ord, inet_pton(af, netmask)):
        while b:
            cidr += b & 1
            b >>= 1

    return cidr


def getprotobynumber(number, file="/etc/protocols"):
    """Map a protocol number to a name.

    Return the protocol name or None if no match is found.
    """
    r = re.compile("(?P<proto>\S+)\s+(?P<num>\d+)")
    with open(file, 'r') as f:
        for line in f:
            m = r.match(line)
            if m and int(m.group("num")) == number:
                return m.group("proto")


def si_units(value, base_unit='b'):
    for prefix, multiplier in (
            ("P", 1125899906842624.0),
            ("T", 1099511627776.0),
            ("G", 1073741824.0),
            ("M", 1048576.0),
            ("k", 1024.0),
            ("", 1.0)):
        if value >= multiplier:
            return "%#.2f%s%s" % (value / multiplier, prefix, base_unit)
    return "%i%s" % (value, base_unit)


def metric_units(value, base_unit='b'):
    for prefix, multiplier in (
            ("P", 1000000000000000.0),
            ("T", 1000000000000.0),
            ("G", 1000000000.0),
            ("M", 1000000.0),
            ("k", 1000.0),
            ("", 1.0)):
        if value >= multiplier:
            return "%#.2f%s%s" % (value / multiplier, prefix, base_unit)
    return "%i%s" % (value, base_unit)
