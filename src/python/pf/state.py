from socket import IPPROTO_TCP, IPPROTO_UDP, IPPROTO_ICMP, IPPROTO_ICMPV6, ntohl
from ctypes import Union, Structure, c_uint16, c_uint32, c_uint8, c_char, c_uint64, string_at, c_void_p, addressof, \
    sizeof
from struct import pack, unpack
from pf.address import pf_addr, pf_addr_wrap, AddressSpec
from pf.base import StructWrapper
from pf.iface import IFNAMSIZ
from pf.pfvar import PF_OUT, PF_TCPS_PROXY_DST, PF_TCPS_PROXY_SRC, PF_WSCALE_MASK, PFUDPS_NSTATES, PFOTHERS_NSTATES, \
    PFUDPS_NAMES, PFSTATE_NOSYNC
from pf.queue import TAILQ_HEAD, TAILQ_ENTRY
from pf.tcp_fsm import TCPS_TIME_WAIT, TCPS_NAMES
from pf.util import getprotobynumber


__all__ = ['StatePeer',
           'PFStateKey',
           'State']

PFSYNC_SCRUB_FLAG_VALID = 0x01


class pf_state_xport(Union):
    """
    union pf_state_xport {
        u_int16_t    port;
        u_int16_t    call_id;
        u_int32_t    spi;
    };
    """
    _fields_ = [
        ("port", c_uint16),
        ("call_id", c_uint16),
        ("spi", c_uint32),
    ]


class pfsync_state_host(Structure):
    """
    struct pfsync_state_host {
        struct pf_addr        addr;
        union pf_state_xport    xport;
        u_int16_t        pad[2];
    } __packed;
    """
    _fields_ = [
        ("addr", pf_addr),
        ("xport", pf_state_xport),
        ("pad", c_uint16 * 2),
    ]


PFSYNC_FLAG_COMPRESS = 0x01
PFSYNC_FLAG_STALE = 0x02
PFSYNC_FLAG_SRCNODE = 0x04
PFSYNC_FLAG_NATSRCNODE = 0x08


class pf_state_host(Structure):
    """
    struct pf_state_host {
        struct pf_addr        addr;
        union pf_state_xport    xport;
    };
    """
    _fields_ = [
        ("addr", pf_addr),
        ("xport", pf_state_xport),
    ]


class pfsync_state_scrub(Structure):
    """
    struct pfsync_state_scrub {
        u_int16_t    pfss_flags;
        u_int8_t    pfss_ttl;    /* stashed TTL        */
        u_int8_t    scrub_flag;
        u_int32_t    pfss_ts_mod;    /* timestamp modulation    */
    } __packed;
    """
    _fields_ = [
        ("pfss_flags", c_uint16),
        ("pfss_ttl", c_uint8),
        ("scrub_flag", c_uint8),
        ("pfss_ts_mod", c_uint32),
    ]


class StateScrub(StructWrapper):
    @property
    def _struct_type(self):
        return pfsync_state_peer

    def __init__(self, peer):
        super(StateScrub, self).__init__(peer)

    def _to_string(self):
        return object.__str__(self)


class pfsync_state_peer(Structure):
    """
    struct pfsync_state_peer {
        struct pfsync_state_scrub scrub;    /* state is scrubbed    */
        u_int32_t    seqlo;        /* Max sequence number sent    */
        u_int32_t    seqhi;        /* Max the other end ACKd + win    */
        u_int32_t    seqdiff;    /* Sequence number modulator    */
        u_int16_t    max_win;    /* largest window (pre scaling)    */
        u_int16_t    mss;        /* Maximum segment size option    */
        u_int8_t    state;        /* active state level        */
        u_int8_t    wscale;        /* window scaling factor    */
        u_int8_t    pad[6];
    } __packed;
    """
    _fields_ = [
        ("scrub", pfsync_state_scrub),
        ("seqlo", c_uint32),
        ("seqhi", c_uint32),
        ("seqdiff", c_uint32),
        ("max_win", c_uint16),
        ("mss", c_uint16),
        ("state", c_uint8),
        ("wscale", c_uint8),
        ("pad", c_uint8 * 6),
    ]


class StatePeer(StructWrapper):
    """Represents a connection endpoint."""

    @property
    def _struct_type(self):
        return pfsync_state_peer

    def __init__(self, peer):
        super(StatePeer, self).__init__(peer)
        self._scrub = StateScrub(self.scrub_)

    @property
    def seqlo(self):
        return ntohl(self.seqlo_)

    @property
    def seqhi(self):
        return ntohl(self.seqhi_)

    @property
    def seqdiff(self):
        return ntohl(self.seqdiff_)

    @property
    def scrub(self):
        return self._scrub

    def _to_string(self):
        return object.__str__(self)


class hook_desc(Structure):
    """
    typedef void (*hook_fn_t)(void *);

    struct hook_desc {
        TAILQ_ENTRY(hook_desc) hd_list;
        hook_fn_t hd_fn;
        void *hd_arg;
    };

    #define	HOOK_REMOVE	0x01
    #define	HOOK_FREE	0x02
    #define	HOOK_ABORT	0x04
    """


hook_desc._fields_ = [
    ("hd_list", c_void_p * 2),  # TAILQ_ENTRY(hook_desc)
    ("hd_fn", c_void_p),
    ("hd_arg", c_void_p),
]

hook_desc_head = TAILQ_HEAD("hook_desc_head", hook_desc)


class pfsync_state(Structure):
    """
    struct pfsync_state {
        u_int32_t     id[2];
        char         ifname[IFNAMSIZ];
        struct pfsync_state_host lan;
        struct pfsync_state_host gwy;
        struct pfsync_state_host ext;
        struct pfsync_state_peer src;
        struct pfsync_state_peer dst;
        struct pf_addr     rt_addr;
        struct hook_desc_head unlink_hooks;
        u_int32_t     rule;
        u_int32_t     anchor;
        u_int32_t     nat_rule;
        u_int64_t     creation;
        u_int64_t     expire;
        u_int32_t     packets[2][2];
        u_int32_t     bytes[2][2];
        u_int32_t     creatorid;
        u_int16_t     tag;
        sa_family_t     af;
        u_int8_t     proto;
        u_int8_t     direction;
        u_int8_t     log;
        u_int8_t     allow_opts;
        u_int8_t     timeout;
        u_int8_t     sync_flags;
        u_int8_t     updates;
        u_int8_t     proto_variant;
        u_int8_t     __pad;
        u_int32_t     flowhash;
    } __packed;
    """
    _fields_ = [
        ("id", c_uint32 * 2),
        ("ifname", c_char * IFNAMSIZ),
        ("lan", pfsync_state_host),
        ("gwy", pfsync_state_host),
        ("ext", pfsync_state_host),
        ("src", pfsync_state_peer),
        ("dst", pfsync_state_peer),
        ("rt_addr", pf_addr),
        ("unlink_hooks", hook_desc_head),  # TAILQ_HEAD(hook_desc_head, hook_desc);
        ("rule", c_uint32),
        ("anchor", c_uint32),
        ("nat_rule", c_uint32),
        ("creation", c_uint64),
        ("expire", c_uint64),
        ("packets", c_uint32 * 2 * 2),
        ("bytes", c_uint32 * 2 * 2),
        ("creatorid", c_uint32),
        ("tag", c_uint16),
        ("af", c_uint8),  # sa_family_t
        ("proto", c_uint8),
        ("direction", c_uint8),
        ("log", c_uint8),
        ("allow_opts", c_uint8),
        ("timeout", c_uint8),
        ("sync_flags", c_uint8),
        ("updates", c_uint8),
        ("proto_variant", c_uint8),
        ("__pad", c_uint8),
        ("flowhash", c_uint32),
    ]


class State(StructWrapper):
    """Represents an entry in Packet Filter's state table."""

    @property
    def _struct_type(self):
        return pfsync_state

    def __init__(self, state):
        """Check argument and initialize class attributes."""
        super(State, self).__init__(state)
        a = pf_addr_wrap()
        a.v.a.addr = self.rt_addr_
        self._rt_addr = AddressSpec(a, self.af)
        if self.direction == PF_OUT:
            self._src = StatePeer(self.src_)
            self._dst = StatePeer(self.dst_)
        else:
            self._src = StatePeer(self.dst_)
            self._dst = StatePeer(self.src_)

    @property
    def rt_addr(self):
        return self._rt_addr

    @property
    def id(self):
        return unpack("Q", pack(">Q", self.id_))[0]

    @property
    def rule(self):
        return ntohl(self.rule_)

    @property
    def anchor(self):
        return ntohl(self.anchor_)

    @property
    def nat_rule(self):
        return ntohl(self.nat_rule_)

    @property
    def creation(self):
        return ntohl(self.creation_)

    @property
    def expire(self):
        return ntohl(self.expire_)

    @property
    def creatorid(self):
        return ntohl(self.creatorid_) & 0xffffffff

    @property
    def packets(self):
        p = unpack('>IIII', string_at(addressof(self.packets_), sizeof(self.packets_)))
        return (p[0] << 32 | p[1]), (p[2] << 32 | p[3])

    @property
    def bytes(self):
        b = unpack('>IIII', string_at(addressof(self.bytes_), sizeof(self.bytes_)))
        return (b[0] << 32 | b[1]), (b[2] << 32 | b[3])

    @property
    def src(self):
        return self._src

    @property
    def dst(self):
        return self._dst

    def _to_string(self):
        src, dst = self.src, self.dst

        s = "{.ifname} ".format(self)
        s += "{} ".format(getprotobynumber(self.proto) or self.proto)

        if self.direction == PF_OUT:
            s += " -> "
        else:
            s += " <- "

        s += "       "
        if self.proto == IPPROTO_TCP:
            if src.state <= TCPS_TIME_WAIT and dst.state <= TCPS_TIME_WAIT:
                s += "{}:{}".format(TCPS_NAMES[src.state], TCPS_NAMES[dst.state])
            elif src.state == PF_TCPS_PROXY_SRC or dst.state == PF_TCPS_PROXY_SRC:
                s += "PROXY:SRC"
            elif src.state == PF_TCPS_PROXY_DST or dst.state == PF_TCPS_PROXY_DST:
                s += "PROXY:DST"
            else:
                s += "<BAD STATE LEVELS {.state}:{.state}>".format(src, dst)

            s += "\n"
            for p in src, dst:
                s += "   [{.seqlo} + {}]".format(p, (p.seqhi - p.seqlo))
                if p.seqdiff:
                    s += "(+{.seqdiff})".format(p)
                if src.wscale and dst.wscale:
                    s += " wscale {}".format(p.wscale & PF_WSCALE_MASK)

        elif (
            (self.proto == IPPROTO_UDP and src.state < PFUDPS_NSTATES and dst.state < PFUDPS_NSTATES) or
            (self.proto not in (IPPROTO_ICMP, IPPROTO_ICMPV6) and
                src.state < PFOTHERS_NSTATES and dst.state < PFOTHERS_NSTATES)
        ):
            s += "{}:{}".format(PFUDPS_NAMES[src.state], PFUDPS_NAMES[dst.state])
        else:
            s += "{.state}:{.state}".format(src, dst)

        hrs, secs = divmod(self.creation, 60)
        hrs, mins = divmod(hrs, 60)
        s += "\n   age {:02d}:{:02d}:{:02d}".format(hrs, mins, secs)

        hrs, secs = divmod(self.expire, 60)
        hrs, mins = divmod(hrs, 60)
        s += ", expires in {:02d}:{:02d}:{:02d}".format(hrs, mins, secs)
        s += ", {0.packets[0]}:{0.packets[1]} pkts".format(self)
        s += ", {0.bytes[0]}:{0.bytes[1]} bytes".format(self)

        if self.anchor != 0xffffffff:
            s += ", anchor {0.anchor}".format(self)
        if self.rule != 0xffffffff:
            s += ", rule {0.rule}".format(self)
        if self.sync_flags & PFSYNC_FLAG_SRCNODE:
            s += ", source-track"
        if self.sync_flags & PFSYNC_FLAG_NATSRCNODE:
            s += ", sticky-address\n"
        s += "\n   id: {0.id:016x} creatorid: {0.creatorid:08x}".format(self)
        if self.sync_flags & PFSTATE_NOSYNC:
            s += " (no-sync)"

        return s
