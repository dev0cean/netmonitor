from ctypes import Structure, Union, c_char, c_uint32, c_uint8, c_uint64, c_int, c_char_p, memmove, string_at, addressof
from io import UnsupportedOperation
from socket import AF_INET, AF_INET6, inet_ntop, inet_pton
import re
import time
from pf.base import StructWrapper
from pf.pfvar import PFR_DIR_IN, PFR_DIR_OUT, PFR_OP_ADDR_MAX, PFR_OP_BLOCK, PFR_OP_PASS, \
    PF_TABLE_NAME_SIZE, MAXPATHLEN, PFR_OP_TABLE_MAX, PFR_DIR_MAX
from pf.util import ctonm, nmtoc

__all__ = ['Address',
           'Table',
           'TableStats']


class pfr_addr(Structure):
    """
    struct pfr_addr {
        union {
            struct in_addr     _pfra_ip4addr;
            struct in6_addr     _pfra_ip6addr;
        } pfra_u;
        u_int8_t pfra_af;
        u_int8_t pfra_net;
        u_int8_t pfra_not;
        u_int8_t pfra_fback;
    };
    #define	pfra_ip4addr	pfra_u._pfra_ip4addr
    #define	pfra_ip6addr	pfra_u._pfra_ip6addr
    """
    class _pfra_u(Union):
        _fields_ = [
            ("pfra_ip4addr",  c_uint32),      # struct in_addr
            ("pfra_ip6addr",  c_uint32 * 4),  # struct in6_addr
        ]
    _anonymous_ = ("pfra_u",)
    _fields_ = [
        ("pfra_u", _pfra_u),
        ("pfra_af", c_uint8),
        ("pfra_net", c_uint8),
        ("pfra_not", c_uint8),
        ("pfra_fback", c_uint8),
    ]


class Address(StructWrapper):
    """Represents an address in a PF table."""

    @property
    def _prefix(self):
        return "pfra_"

    @property
    def _struct_type(self):
        return pfr_addr

    @property
    def addr(self):
        addr_len = {AF_INET: 4, AF_INET6: 16}[self.af]
        return inet_ntop(self.af, string_at(addressof(self._struct.pfra_ip6addr), addr_len))
        # return inet_ntop(self.af, self.ip6addr if self.af is AF_INET6 else self.ip4addr)

    @addr.setter
    def addr(self, value):
        addr = inet_pton(self.af, value)
        memmove(self._struct.pfra_ip6addr, c_char_p(addr), len(addr))
        # setattr(self, "ip6addr" if self.af is AF_INET6 else "ip4addr", struct.unpack('>L', addr))

    @property
    def mask(self):
        return ctonm(self.net, self.af)

    @mask.setter
    def mask(self, value):
        if isinstance(value, basestring) and re.compile(r"([0-9.]+)|([0-9a-f:]+)").match(value):
            setattr(self, "net", nmtoc(value, self.af))
        else:
            setattr(self, "net", int(value))

    @property
    def not_(self):
        return bool(getattr(self, "not"))

    @not_.setter
    def not_(self, value):
        super(Address, self).__setattr__("not", 1 if value else 0)

    # noinspection PyAttributeOutsideInit
    def _from_string(self, string):
        """Initalize a new instance from a string."""
        addr_re = ("(?P<not>!)?\s*"
                   "(?P<addr>"
                   "(?P<ipv4>[0-9.]+)|"
                   "(?P<ipv6>[0-9a-f:]+))"
                   "(?:/(?P<mask>\d+))?\s*")

        match = re.compile(addr_re).match(string)
        if not match:
            raise ValueError("Could not parse address: '{}'".format(string))

        self.not_ = bool(match.group("not"))
        if match.group("ipv4"):
            self.af = AF_INET
            self.addr = match.group("ipv4")
        elif match.group("ipv6"):
            self.af = AF_INET6
            self.addr = match.group("ipv6")
        if match.group("mask"):
            self.mask = int(match.group("mask"))
        else:
            self.mask = {AF_INET: 32, AF_INET6: 128}[self.af]

    def _to_string(self):
        """Return the string representation of the address."""
        s = ("! {!s}" if self.not_ else "{!s}").format(self.addr)
        bits = self.net
        if not ((self.af == AF_INET and bits == 32) or (bits == 128)):
            s += "/{}".format(bits)
        return s

    def __eq__(self, other):
        return self.af == other.af and self.addr == other.addr and self.mask == other.mask and self.not_ == other.not_

PFR_TFLAG_PERSIST = 0x00000001
PFR_TFLAG_CONST = 0x00000002
PFR_TFLAG_ACTIVE = 0x00000004
PFR_TFLAG_INACTIVE = 0x00000008
PFR_TFLAG_REFERENCED = 0x00000010
PFR_TFLAG_REFDANCHOR = 0x00000020
PFR_TFLAG_USRMASK = 0x00000003
PFR_TFLAG_SETMASK = 0x0000003C
PFR_TFLAG_ALLMASK = 0x0000003F


class pfr_table(Structure):
    """
    struct pfr_table {
        char             pfrt_anchor[MAXPATHLEN];
        char             pfrt_name[PF_TABLE_NAME_SIZE];
        u_int32_t         pfrt_flags;
        u_int8_t         pfrt_fback;
    };
    """
    _fields_ = [
        ("pfrt_anchor", c_char * MAXPATHLEN),
        ("pfrt_name", c_char * PF_TABLE_NAME_SIZE),
        ("pfrt_flags", c_uint32),
        ("pfrt_fback", c_uint8),
    ]


class Table(StructWrapper):
    """Represents a PF table."""

    @property
    def _prefix(self):
        return "pfrt_"

    @property
    def _struct_type(self):
        return pfr_table

    def __init__(self, table=None, *addrs, **kw):
        if table is None:
            table = pfr_table()
        elif isinstance(table, basestring):
            table = pfr_table(pfrt_name=table)

        self._addrs = []
        for addr in addrs:
            if not isinstance(addr, Address):
                addr = Address(addr)
            self._addrs.append(addr)

        super(Table, self).__init__(table, **kw)

    @property
    def addrs(self):
        """Return a tuple containing the address in the table."""
        return tuple(self._addrs)

    def _to_string(self):
        """Return the string representation of the table."""
        s = ('c' if (self.flags & PFR_TFLAG_CONST) else '-')
        s += ('p' if (self.flags & PFR_TFLAG_PERSIST) else '-')
        s += ('a' if (self.flags & PFR_TFLAG_ACTIVE) else '-')
        s += ('i' if (self.flags & PFR_TFLAG_INACTIVE) else '-')
        s += ('r' if (self.flags & PFR_TFLAG_REFERENCED) else '-')
        s += ('h' if (self.flags & PFR_TFLAG_REFDANCHOR) else '-')
        # NOT SUPPORTED ON MAC OS X
        # s += ('C' if (self.flags & PFR_TFLAG_COUNTERS) else '-')
        s += "\t{.name}".format(self)

        if self.anchor:
            s += "\t{.anchor}".format(self)

        return s


PFR_REFCNT_RULE = 0
PFR_REFCNT_ANCHOR = 1
PFR_REFCNT_MAX = 2


class pfr_tstats(Structure):
    """
    struct pfr_tstats {
        struct pfr_table pfrts_t;
        u_int64_t	 pfrts_packets[PFR_DIR_MAX][PFR_OP_TABLE_MAX];
        u_int64_t	 pfrts_bytes[PFR_DIR_MAX][PFR_OP_TABLE_MAX];
        u_int64_t	 pfrts_match;
        u_int64_t	 pfrts_nomatch;
        u_int64_t	 pfrts_tzero;
        int		 pfrts_cnt;
        int		 pfrts_refcnt[PFR_REFCNT_MAX];
    };
    #define	pfrts_name	pfrts_t.pfrt_name
    #define pfrts_flags	pfrts_t.pfrt_flags
    """
    _fields_ = [
        ("pfrts_t", pfr_table),
        ("pfrts_packets", c_uint64 * PFR_DIR_MAX * PFR_OP_TABLE_MAX),
        ("pfrts_bytes", c_uint64 * PFR_DIR_MAX * PFR_OP_TABLE_MAX),
        ("pfrts_match", c_uint64),
        ("pfrts_nomatch", c_uint64),
        ("pfrts_tzero", c_uint64),
        ("pfrts_cnt", c_int),
        ("pfrts_refcnt", c_int * PFR_REFCNT_MAX),
    ]


class TableStats(StructWrapper):
    """Class containing statistics for a PF table."""

    def __init__(self, obj=None, **kwargs):
        super(TableStats, self).__init__(obj, **kwargs)
        self._table = Table(self.t)

    @property
    def _prefix(self):
        return "pfrts_"

    @property
    def _struct_type(self):
        return pfr_tstats

    @property
    def table(self):
        return self._table

    @property
    def packets(self):
        return {"in": tuple(self.packets_[PFR_DIR_IN]), "out": tuple(self.packets_[PFR_DIR_OUT])}

    @packets.setter
    def packets(self, value):
        raise UnsupportedOperation()

    @property
    def bytes(self):
        return {"in": tuple(self.bytes_[PFR_DIR_IN]), "out": tuple(self.bytes_[PFR_DIR_OUT])}

    @bytes.setter
    def bytes(self, value):
        raise UnsupportedOperation()

    @property
    def evaluations(self):
        return {"match": self.match, "nomatch": self.nomatch}

    @evaluations.setter
    def evaluations(self, value):
        raise UnsupportedOperation()

    @property
    def references(self):
        return {"rules": self.refcnt[PFR_REFCNT_RULE],
                "anchors": self.refcnt[PFR_REFCNT_ANCHOR]}

    @references.setter
    def references(self, value):
        raise UnsupportedOperation()

    def _to_string(self):
        """Return the string representation of the table statistics."""

        s = "{.table}\n".format(self)
        s += "\tAddresses:   {.cnt:d}\n".format(self)
        s += "\tCleared:     {}\n".format(time.ctime(self.tzero))
        s += "\tReferences:  [ Anchors: {anchors:<18d} Rules: {rules:<18d} ]\n".format(self.references)
        s += "\tEvaluations: [ NoMatch: {nomatch:<18d} Match: {match:<18d} ]\n".format(self.evaluations)

        for dirn, dirn_label in {PFR_DIR_IN: "In", PFR_DIR_OUT: "Out"}:
            for op, op_label in {PFR_OP_BLOCK: "Block", PFR_OP_PASS: "Pass", PFR_OP_ADDR_MAX: "XPass"}:
                label = "%s/%s:" % (dirn_label, op_label)
                s += "\t{:<18s}[ Packets: {:<18d} Bytes: {:<18d} ]\n".format(
                    label, self.packets_[dirn][op], self.bytes_[dirn][op])

        return s


class pfr_astats(Structure):
    """
    struct pfr_astats {
        struct pfr_addr pfras_a;
        u_int64_t   pfras_packets[PFR_DIR_MAX][PFR_OP_ADDR_MAX];
        u_int64_t   pfras_bytes[PFR_DIR_MAX][PFR_OP_ADDR_MAX];
        u_int64_t   pfras_tzero;
    };
    """
    _fields_ = [
        ("pfras_a", pfr_addr),
        ("pfras_packets", c_uint64 * PFR_DIR_MAX * PFR_OP_ADDR_MAX),
        ("pfras_bytes", c_uint64 * PFR_DIR_MAX * PFR_OP_ADDR_MAX),
        ("pfras_tzero", c_uint64),
    ]


