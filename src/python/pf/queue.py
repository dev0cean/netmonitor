from ctypes import POINTER, Structure


# Tail queue declarations.
TAILQ_ENTRY_classes = {}
TAILQ_classes = {}


def TAILQ_HEAD(name, type_):
    return TAILQ_classes.setdefault(
        name,
        type(name, (Structure,), {
            "_fields_": [
                ("tqh_first", POINTER(type_)),          # first element
                ("tqh_last", POINTER(POINTER(type_))),  # address of last "next" element
            ]
        }))


"""
def TAILQ_HEAD_INITIALIZER(head):
    return { NULL, &(head).tqh_first }
"""


def TAILQ_ENTRY(type_):
    return TAILQ_ENTRY_classes.setdefault(
        type_,
        type("TAILQ_ENTRY_{.__name__}".format(type_), (Structure,), {
            "_fields_": [
                ("tqe_next", POINTER(type_)),           # next element
                ("tqe_prev", POINTER(POINTER(type_))),  # address of previous "next" element
            ]
        }))()


# Tail queue functions.

def TAILQ_CONCAT(head1, head2, field):
    if not TAILQ_EMPTY(head2):
        head1.tqh_last.content = head2.tqh_first
        head2.tqh_first.field.tqe_prev = head1.tqh_last
        head1.tqh_last = head2.tqh_last
        TAILQ_INIT(head2)


def TAILQ_EMPTY(head):
    # return head.tqh_first.content is None
    return head.tqh_first == type(head.tqh_first)()


def TAILQ_FIRST(head):
    return head.tqh_first.content

"""
def TAILQ_FOREACH(var, head, field):
    for ((var) = TAILQ_FIRST((head)); (var); (var) = TAILQ_NEXT((var), field))

#define    TAILQ_FOREACH_SAFE(var, head, field, tvar)            \
    for ((var) = TAILQ_FIRST((head));                \
        (var) && ((tvar) = TAILQ_NEXT((var), field), 1);        \
        (var) = (tvar))

#define    TAILQ_FOREACH_REVERSE(var, head, headname, field)        \
    for ((var) = TAILQ_LAST((head), headname);            \
        (var);                            \
        (var) = TAILQ_PREV((var), headname, field))

#define    TAILQ_FOREACH_REVERSE_SAFE(var, head, headname, field, tvar)    \
    for ((var) = TAILQ_LAST((head), headname);            \
        (var) && ((tvar) = TAILQ_PREV((var), headname, field), 1);    \
        (var) = (tvar))
"""


def TAILQ_INIT(head):
    head.tqh_first = None
    head.tqh_last.contents = head.tqh_first

"""
#define    TAILQ_INSERT_AFTER(head, listelm, elm, field) do {        \
    if ((TAILQ_NEXT((elm), field) = TAILQ_NEXT((listelm), field)) != NULL)\
        TAILQ_NEXT((elm), field)->field.tqe_prev =         \
            &TAILQ_NEXT((elm), field);                \
    else {                                \
        (head)->tqh_last = &TAILQ_NEXT((elm), field);        \
        QMD_TRACE_HEAD(head);                    \
    }                                \
    TAILQ_NEXT((listelm), field) = (elm);                \
    (elm)->field.tqe_prev = &TAILQ_NEXT((listelm), field);        \
    QMD_TRACE_ELEM(&(elm)->field);                    \
    QMD_TRACE_ELEM(&listelm->field);                \
} while (0)

#define    TAILQ_INSERT_BEFORE(listelm, elm, field) do {            \
    (elm)->field.tqe_prev = (listelm)->field.tqe_prev;        \
    TAILQ_NEXT((elm), field) = (listelm);                \
    *(listelm)->field.tqe_prev = (elm);                \
    (listelm)->field.tqe_prev = &TAILQ_NEXT((elm), field);        \
    QMD_TRACE_ELEM(&(elm)->field);                    \
    QMD_TRACE_ELEM(&listelm->field);                \
} while (0)

#define    TAILQ_INSERT_HEAD(head, elm, field) do {            \
    if ((TAILQ_NEXT((elm), field) = TAILQ_FIRST((head))) != NULL)    \
        TAILQ_FIRST((head))->field.tqe_prev =            \
            &TAILQ_NEXT((elm), field);                \
    else                                \
        (head)->tqh_last = &TAILQ_NEXT((elm), field);        \
    TAILQ_FIRST((head)) = (elm);                    \
    (elm)->field.tqe_prev = &TAILQ_FIRST((head));            \
    QMD_TRACE_HEAD(head);                        \
    QMD_TRACE_ELEM(&(elm)->field);                    \
} while (0)

#define    TAILQ_INSERT_TAIL(head, elm, field) do {            \
    TAILQ_NEXT((elm), field) = NULL;                \
    (elm)->field.tqe_prev = (head)->tqh_last;            \
    *(head)->tqh_last = (elm);                    \
    (head)->tqh_last = &TAILQ_NEXT((elm), field);            \
    QMD_TRACE_HEAD(head);                        \
    QMD_TRACE_ELEM(&(elm)->field);                    \
} while (0)

#define    TAILQ_LAST(head, headname)                    \
__MISMATCH_TAGS_PUSH                            \
    (*(((struct headname *)((head)->tqh_last))->tqh_last))        \
__MISMATCH_TAGS_POP

#define    TAILQ_NEXT(elm, field) ((elm)->field.tqe_next)

#define    TAILQ_PREV(elm, headname, field)                \
__MISMATCH_TAGS_PUSH                            \
    (*(((struct headname *)((elm)->field.tqe_prev))->tqh_last))    \
__MISMATCH_TAGS_POP

#define    TAILQ_REMOVE(head, elm, field) do {                \
    if ((TAILQ_NEXT((elm), field)) != NULL)                \
        TAILQ_NEXT((elm), field)->field.tqe_prev =         \
            (elm)->field.tqe_prev;                \
    else {                                \
        (head)->tqh_last = (elm)->field.tqe_prev;        \
        QMD_TRACE_HEAD(head);                    \
    }                                \
    *(elm)->field.tqe_prev = TAILQ_NEXT((elm), field);        \
    TRASHIT((elm)->field.tqe_next);                    \
    TRASHIT((elm)->field.tqe_prev);                    \
    QMD_TRACE_ELEM(&(elm)->field);                    \
} while (0)

/*
 * Why did they switch to spaces for this one macro?
 */
#define TAILQ_SWAP(head1, head2, type, field)                           \
__MISMATCH_TAGS_PUSH                                                    \
do {                                                                    \
    struct type *swap_first = (head1)->tqh_first;                   \
    struct type **swap_last = (head1)->tqh_last;                    \
    (head1)->tqh_first = (head2)->tqh_first;                        \
    (head1)->tqh_last = (head2)->tqh_last;                          \
    (head2)->tqh_first = swap_first;                                \
    (head2)->tqh_last = swap_last;                                  \
    if ((swap_first = (head1)->tqh_first) != NULL)                  \
        swap_first->field.tqe_prev = &(head1)->tqh_first;       \
    else                                                            \
        (head1)->tqh_last = &(head1)->tqh_first;                \
    if ((swap_first = (head2)->tqh_first) != NULL)                  \
        swap_first->field.tqe_prev = &(head2)->tqh_first;       \
    else                                                            \
        (head2)->tqh_last = &(head2)->tqh_first;                \
} while (0)                                                             \
__MISMATCH_TAGS_POP

"""
