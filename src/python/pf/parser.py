# NAT ports range (from /usr/src/sbin/pfctl/pfctl_parser.h)
PF_NAT_PROXY_PORT_LOW = 50001
PF_NAT_PROXY_PORT_HIGH = 65535

# Pool IDs (from /usr/src/sbin/pfctl/pfctl_parser.c)
PF_POOL_ROUTE = 0
PF_POOL_NAT = 1
PF_POOL_RDR = 2
