from socket import AF_UNSPEC, AF_INET, AF_INET6, inet_ntop, inet_pton, getservbyport, getservbyname
from abc import abstractmethod
from ctypes import Union, Structure, POINTER, addressof, c_uint32, c_uint8, c_uint16, c_char, c_void_p, c_int, \
    string_at, c_char_p, memmove
import re
from pf.base import StructWrapper, Constraint
from pf.enum import PF_OPS
from pf.iface import IFNAMSIZ
from pf.parser import PF_POOL_NAT, PF_POOL_RDR, PF_NAT_PROXY_PORT_LOW, PF_NAT_PROXY_PORT_HIGH
from pf.pfvar import PF_TABLE_NAME_SIZE, RTLABEL_LEN, PF_OP_NONE
from pf.queue import TAILQ_HEAD, TAILQ_ENTRY
from pf.util import ctonm, nmtoc


PFI_AFLAG_NETWORK = 0x01
PFI_AFLAG_BROADCAST = 0x02
PFI_AFLAG_PEER = 0x04
PFI_AFLAG_MODEMASK = 0x07
PFI_AFLAG_NOALIAS = 0x08


# Interface modifiers
PF_IF_MODS = {
    "network": PFI_AFLAG_NETWORK,
    "broadcast": PFI_AFLAG_BROADCAST,
    "peer": PFI_AFLAG_PEER,
    "0": PFI_AFLAG_NOALIAS
}

# pools enum
PF_POOL_NONE = 0
PF_POOL_BITMASK = 1
PF_POOL_RANDOM = 2
PF_POOL_SRCHASH = 3
PF_POOL_ROUNDROBIN = 4

# addresses enum
PF_ADDR_ADDRMASK = 0
PF_ADDR_NOROUTE = 1
PF_ADDR_DYNIFTL = 2
PF_ADDR_TABLE = 3
PF_ADDR_RTLABEL = 4
PF_ADDR_URPFFAILED = 5
PF_ADDR_RANGE = 6

PF_POOL_TYPEMASK = 0x0f
PF_POOL_STICKYADDR = 0x20


class pf_port_range(Structure):
    """
    struct pf_port_range {
        u_int16_t            port[2];
        u_int8_t            op;
    };
    """
    _fields_ = [
        ("port", c_uint16 * 2),
        ("op", c_uint8),
    ]


class PortRange(Constraint):
    @property
    def _struct_type(self):
        return pf_port_range

    def __init__(self, port=None, op=PF_OP_NONE):
        super(PortRange, self).__init__(port, op)

    @property
    def operands(self):
        return tuple(self.port)

    @operands.setter
    def operands(self, tup):
        self.port[0] = tup[0]
        if tup[1]:
            self.port[1] = tup[1]

    @property
    def operator(self):
        return self.op

    @operator.setter
    def operator(self, value):
        if isinstance(value, basestring):
            value = PF_OPS[value]
        if not isinstance(value, int):
            raise ValueError()
        setattr(self, "op", value)

    def _num_to_str(self, n):
        """Convert a numeric port to a service name."""
        try:
            return getservbyport(n)
        except TypeError:
            return str(n)

    def _str_to_num(self, s):
        """Convert a service name to a numeric port."""
        return getservbyname(s)


class pf_addr(Structure):
    """
    struct pf_addr {
        union {
            struct in_addr        v4;
            struct in6_addr        v6;
            u_int8_t        addr8[16];
            u_int16_t        addr16[8];
            u_int32_t        addr32[4];
        } pfa;            /* 128-bit address */
    #define v4    pfa.v4
    #define v6    pfa.v6
    #define addr8    pfa.addr8
    #define addr16    pfa.addr16
    #define addr32    pfa.addr32
    };
    """
    class _pfa(Union):
        _fields_ = [
            ("v4", c_uint32),       # struct in_addr
            ("v6", c_uint32 * 4),   # struct in6_addr
            ("addr8", c_uint8 * 16),
            ("addr16", c_uint16 * 8),
            ("addr32", c_uint32 * 4),
        ]

    _anonymous_ = ("pfa",)
    _fields_ = [("pfa", _pfa)]


class pf_addr_wrap(Structure):
    """
    struct pf_addr_wrap {
        union {
            struct {
                struct pf_addr addr;
                struct pf_addr mask;
            } a;
            char ifname[IFNAMSIZ];
            char tblname[PF_TABLE_NAME_SIZE];
            char rtlabelname[RTLABEL_LEN];
            u_int32_t rtlabel;
        } v;
        union {
    #ifdef KERNEL
            struct pfi_dynaddr *dyn __attribute__((aligned(8)));
            struct pfr_ktable *tbl  __attribute__((aligned(8)));
    #else /* !KERNEL */
            void *dyn               __attribute__((aligned(8)));
            void *tbl               __attribute__((aligned(8)));
    #endif /* !KERNEL */
            int dyncnt              __attribute__((aligned(8)));
            int tblcnt              __attribute__((aligned(8)));
        } p                         __attribute__((aligned(8)));
        u_int8_t type;      /* PF_ADDR_* */
        u_int8_t iflags;    /* PFI_AFLAG_* */
    };
    """
    class _v(Union):
        class _a(Structure):
            _fields_ = [
                ("addr", pf_addr),
                ("mask", pf_addr),
            ]

        _fields_ = [
            ("a", _a),
            ("ifname", c_char * IFNAMSIZ),
            ("tblname", c_char * PF_TABLE_NAME_SIZE),
            ("rtlabelname", c_char * RTLABEL_LEN),
            ("rtlabel", c_uint32),
        ]

    class _p(Union):
        _fields_ = [
            ("dyn", c_void_p),      # (struct pfi_dynaddr *)
            ("tbl", c_void_p),      # (struct pfr_ktable *)
            ("dyncnt", c_int),
            ("tblcnt", c_int),
        ]

    _fields_ = [
        ("v", _v),
        ("p", _p),
        ("type", c_uint8),
        ("iflags", c_uint8),
    ]


class AddressSpec(StructWrapper):
    """
    Class cluster for the heavily overloaded pf_addr_wrap struct
    representing a rule address match specification.
    """

    @property
    def _struct_type(self):
        return pf_addr_wrap

    def __new__(cls, *args, **kwargs):
        if cls is AddressSpec:
            typ = PF_ADDR_ADDRMASK
            if args[0]:
                if isinstance(args[0], pf_addr_wrap):
                    typ = args[0].type
                elif isinstance(args[0], int):
                    typ = args[0]
                elif isinstance(args[0], basestring):
                    it = AddressSpec.type_registry.itervalues()
                    while True:
                        try:
                            clazz = next(it)
                            match = clazz.match(args[0])
                            if match:
                                kwargs.update(**match)
                                return super(AddressSpec, clazz).__new__(*args, **kwargs)
                        except StopIteration:
                            raise ValueError("Could not parse address: {}".format(args[0]))
            return super(AddressSpec, AddressSpec.type_registry.get(typ)).__new__(*args, **kwargs)
        return super(AddressSpec, cls).__new__(*args, **kwargs)

    @abstractmethod
    def _to_string(self):
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def match(string):
        raise NotImplementedError()

    def __eq__(self, other):
        return self.type == other.type and self.af == other.af

    def __ne__(self, other):
        return not self.__eq__(other)


class AddressMaskAddressSpec(AddressSpec):

    def __init__(self, addr=None, af=AF_UNSPEC, **kw):
        if addr is None:
            addr = pf_addr_wrap(type=kw.get("type", PF_ADDR_ADDRMASK))
        super(AddressSpec, self).__init__(addr, af=af, **kw)

    @staticmethod
    def match(string):
        _re = ("(?P<any>any)|"
               "(?P<ipv4>[0-9.]+)(?:/(?P<mask4>\d+))?|"
               "(?P<ipv6>[0-9a-f:]+)(?:/(?P<mask6>\d+))?")
        m = re.compile(_re).match(string)
        if m:
            kwargs = {"type": PF_ADDR_ADDRMASK}
            if m.group("any"):
                kwargs["af"] = AF_UNSPEC
            elif m.group("ipv4"):
                kwargs["af"] = AF_INET
                kwargs["addr"] = m.group("ipv4")
                kwargs["mask"] = ctonm(int(m.group("mask4")) if m.group("mask4") else 32, AF_INET)
            elif m.group("ipv6"):
                kwargs["af"] = AF_INET6
                kwargs["addr"] = m.group("ipv6")
                kwargs["mask"] = ctonm(int(m.group("mask6")) if m.group("mask6") else 128, AF_INET6)
            return kwargs
        return None

    @property
    def addr(self):
        return inet_ntop(self.af, string_at(addressof(self.v.a.addr.v6), {AF_INET: 4, AF_INET6: 16}[self.af]))

    @addr.setter
    def addr(self, value):
        addr = inet_pton(self.af, value)
        memmove(self.v.a.addr.v6, c_char_p(addr), len(addr))

    @property
    def mask(self):
        return inet_ntop(self.af, string_at(addressof(self.v.a.mask.v6), {AF_INET: 4, AF_INET6: 16}[self.af]))

    @mask.setter
    def mask(self, value):
        mask = inet_pton(self.af, value)
        memmove(self.v.a.mask.v6, c_char_p(mask), len(mask))

    def _to_string(self):
        if not self.addr:
            return "any"
        s = self.addr
        if self.mask:
            bits = nmtoc(self.mask, self.af)
            if not ((self.af == AF_INET and bits == 32) or (bits == 128)):
                s += "/{}".format(bits)
        return s

    def __eq__(self, other):
        return super(AddressMaskAddressSpec, self).__eq__(other) and self.addr == other.addr and self.mask == other.mask


class NoRouteAddressSpec(AddressSpec):

    def __init__(self, obj=None, **kwargs):
        super(NoRouteAddressSpec, self).__init__(obj, type=PF_ADDR_NOROUTE, **kwargs)

    @staticmethod
    def match(string):
        return {"type": PF_ADDR_NOROUTE} if re.compile("(?Pno-route)").match(string) else None

    def _to_string(self):
        return "no-route"


class DynamicInterfaceTableAddressSpec(AddressSpec):

    def __init__(self, obj=None, **kwargs):
        super(DynamicInterfaceTableAddressSpec, self).__init__(obj, type=PF_ADDR_DYNIFTL, **kwargs)

    @property
    def ifname(self):
        return self.v.ifname

    @ifname.setter
    def ifname(self, value):
        self.v.ifname = value

    @property
    def dyncnt(self):
        return self.p.dyncnt

    @dyncnt.setter
    def dyncnt(self, value):
        self.p.dyncnt = value

    @property
    def mask(self):
        return inet_ntop(self.af, string_at(addressof(self.v.a.mask.v6), {AF_INET: 4, AF_INET6: 16}[self.af]))

    @mask.setter
    def mask(self, value):
        if self.af == AF_UNSPEC:
            mask = '\xff' * 16
        else:
            mask = inet_pton(self.af, value)
        memmove(self.v.a.mask.v6, c_char_p(mask), len(mask))

    @staticmethod
    def match(string):
        _re = "\((?P<ifname>[a-z]+[0-9]*)(?P<mod>(:network|:broadcast|:peer|:0)*)\)(?:/(?P<mask>\d+))?"
        match = re.compile(_re).match(string)
        kwargs = None
        if match:
            kwargs = {"type": PF_ADDR_DYNIFTL, "ifname": match.group("ifname"), "dyncnt": 0}
            # todo: lookup the address family of the named interface
            # if match.group("mask"):
            #     kwargs["mask"] = ctonm(int(match.group("mask")), af)
            # else:
            #     kwargs["mask"] = ctonm({AF_INET: 32, AF_INET6: 128}[af], af)
            iflags = 0
            for mod in match.group("mod").split(":")[1:]:
                iflags |= PF_IF_MODS[mod]
            kwargs["iflags"] = iflags
        return kwargs

    def _to_string(self):
        s = "({.ifname}"
        if self.iflags & PFI_AFLAG_NETWORK:
            s += ":network"
        if self.iflags & PFI_AFLAG_BROADCAST:
            s += ":broadcast"
        if self.iflags & PFI_AFLAG_PEER:
            s += ":peer"
        if self.iflags & PFI_AFLAG_NOALIAS:
            s += ":0"
        s += ")"
        if self.mask:
            bits = nmtoc(self.mask, self.af)
            if not ((self.af == AF_INET and bits == 32) or (bits == 128)):
                s += "/{}".format(bits)
        return s.format(self)

    def __eq__(self, other):
        return (super(DynamicInterfaceTableAddressSpec, self).__eq__(other) and
                self.ifname == other.ifname and
                self.mask == other.mask and
                self.iflags == other.iflags)


class AddressTableAddressSpec(AddressSpec):

    def __init__(self, obj=None, **kwargs):
        super(AddressTableAddressSpec, self).__init__(obj, type=PF_ADDR_TABLE, **kwargs)

    @property
    def tblname(self):
        return self.v.tblname

    @tblname.setter
    def tblname(self, value):
        self.v.tblname = value

    @property
    def tblcnt(self):
        return self.p.tblcnt

    @tblcnt.setter
    def tblcnt(self, value):
        self.p.tblcnt = value

    @staticmethod
    def match(string):
        match = re.compile("<(?P<tblname>\w+)>").match(string)
        if match:
            return {"type": PF_ADDR_TABLE, "tblname":  match.group("tblname"), "tblcnt": -1}
        return None

    def _to_string(self):
        return "<{.tblname}>".format(self)

    def __eq__(self, other):
        return super(AddressTableAddressSpec, self).__eq__(other) and self.tblname == other.tblname


class RouteLabelAddressSpec(AddressSpec):

    def __init__(self, obj=None, **kwargs):
        super(RouteLabelAddressSpec, self).__init__(obj, type=PF_ADDR_RTLABEL, **kwargs)

    @property
    def rtlabel(self):
        return self.v.rtlabel

    @rtlabel.setter
    def rtlabel(self, value):
        self.v.rtlabel = value

    @property
    def rtlabelname(self):
        return self.v.rtlabelname

    @rtlabelname.setter
    def rtlabelname(self, value):
        self.v.rtlabelname = value

    @staticmethod
    def match(string):
        match = re.compile("route\s+(?P<label>\w+)").match(string)
        if match:
            return {"type": PF_ADDR_RTLABEL, "rtlabelname": match.group("label"), "rtlabel": 0}
        return None

    def _to_string(self):
        return "route \"{.rtlabelname}\"".format(self)

    def __eq__(self, other):
        return super(RouteLabelAddressSpec, self).__eq__(other) and (self.rtlabelname == other.rtlabelname)


class URPFFailedAddressSpec(AddressSpec):

    def __init__(self, obj=None, **kwargs):
        super(URPFFailedAddressSpec, self).__init__(obj, type=PF_ADDR_URPFFAILED, **kwargs)

    @staticmethod
    def match(string):
        return {"type": PF_ADDR_URPFFAILED} if "urpf-failed" == string else None

    def _to_string(self):
        return "urpf-failed"


class AddressRangeAddressSpec(AddressSpec):

    def __init__(self, obj=None, **kwargs):
        super(AddressRangeAddressSpec, self).__init__(obj, type=PF_ADDR_RANGE, **kwargs)

    @property
    def addr(self):
        addr_len = {AF_INET: 4, AF_INET6: 16}[self.af]
        lower = inet_ntop(self.af, string_at(addressof(self.v.a.addr.v6), addr_len))
        upper = inet_ntop(self.af, string_at(addressof(self.v.a.mask.v6), addr_len))
        return lower, upper

    @addr.setter
    def addr(self, value):
        if not isinstance(value, tuple) or len(value) != 2:
            raise ValueError()
        lower = inet_pton(self.af, value[0])
        upper = inet_pton(self.af, value[1])
        memmove(self.v.a.addr.v6, c_char_p(lower), len(lower))
        memmove(self.v.a.mask.v6, c_char_p(upper), len(upper))

    @staticmethod
    def match(string):
        _re = ("(?P<ipv4rg>(?P<ipv4_1>[0-9.]+)\s*-\s*(?P<ipv4_2>[0-9.]+))|"
               "(?P<ipv6rg>(?P<ipv6_1>[0-9a-f:]+)\s*-\s*(?P<ipv6_2>[0-9a-f:]+))")
        m = re.compile(_re).match(string)
        if m:
            kwargs = {"type": PF_ADDR_RANGE}
            if m.group("ipv4rg"):
                kwargs["af"] = AF_INET
                kwargs["addr"] = m.group("ipv4_1", "ipv4_2")
            else:
                kwargs["af"] = AF_INET6
                kwargs["addr"] = m.group("ipv6_1", "ipv6_2")

    def _to_string(self):
        return "{0.addr[0]} - {0.addr[1]}".format(self)

    def __eq__(self, other):
        return super(AddressRangeAddressSpec, self).__eq__(other) and self.addr == other.addr


AddressSpec.type_registry = {
    PF_ADDR_ADDRMASK: AddressMaskAddressSpec,
    PF_ADDR_NOROUTE: NoRouteAddressSpec,
    PF_ADDR_DYNIFTL: DynamicInterfaceTableAddressSpec,
    PF_ADDR_TABLE: AddressTableAddressSpec,
    PF_ADDR_RTLABEL: RouteLabelAddressSpec,
    PF_ADDR_URPFFAILED: URPFFailedAddressSpec,
    PF_ADDR_RANGE: AddressRangeAddressSpec,
}


class pf_pooladdr(Structure):
    """
    struct pf_pooladdr {
        struct pf_addr_wrap         addr;
        TAILQ_ENTRY(pf_pooladdr)     entries;
        char                 ifname[IFNAMSIZ];
    #ifdef KERNEL
        struct pfi_kif            *kif    __attribute__((aligned(8)));
    #else /* !KERNEL */
        void                *kif    __attribute__((aligned(8)));
    #endif /* !KERNEL */
    };
    """
pf_pooladdr._fields_ = [
    ("addr", pf_addr_wrap),
    ("entries", c_void_p * 2),  # TAILQ_ENTRY(pf_pooladdr)
    ("ifname", c_char * IFNAMSIZ),
    ("kif", c_void_p),  # struct pfi_kif*
]


class pf_poolhashkey(Structure):
    """
    struct pf_poolhashkey {
        union {
            u_int8_t        key8[16];
            u_int16_t        key16[8];
            u_int32_t        key32[4];
        } pfk;            /* 128-bit hash key */
    #define key8    pfk.key8
    #define key16    pfk.key16
    #define key32    pfk.key32
    };
    """
    class _pfk(Union):
        _fields_ = [
            ("key8", c_uint8 * 16),
            ("key16", c_uint16 * 8),
            ("key32", c_uint32 * 4)
        ]

    _fields_ = [("pfk", _pfk)]
    _anonymous_ = ("pfk",)

pf_palist = TAILQ_HEAD("pf_palist", pf_pooladdr)


class pf_pool(Structure):
    """
    struct pf_pool {
        struct pf_palist     list;
    #ifdef KERNEL
        struct pf_pooladdr    *cur        __attribute__((aligned(8)));
    #else /* !KERNEL */
        void            *cur        __attribute__((aligned(8)));
    #endif /* !KERNEL */
        struct pf_poolhashkey     key        __attribute__((aligned(8)));
        struct pf_addr         counter;
        int             tblidx;
        u_int16_t         proxy_port[2];
        u_int8_t         port_op;
        u_int8_t         opts;
    };

    list is a list of pool addresses, with cur being a pointer to the current address in the list
    """
    _anonymous_ = ("_proxy_port_union",)
    _fields_ = [
        ("list", pf_palist),
        ("cur", POINTER(pf_pooladdr)),
        ("key", pf_poolhashkey),
        ("counter", pf_addr),
        ("tblidx", c_int),
        ("_proxy_port_union", type("_proxy_port_union", (Union,), {
            "_anonymous_": ("_po",),
            "_fields_": [
                ("proxy_port_range", pf_port_range),
                ("_po", type("_po", (Structure,), {
                    "_fields_": [
                        ("proxy_port", c_uint16 * 2),
                        ("port_op", c_uint8),
                    ]
                }))
            ]
        })),
        ("opts", c_uint8)
    ]


class AddressPool(StructWrapper):
    """Class representing an address pool."""

    @property
    def _struct_type(self):
        return pf_pool

    def __init__(self, id_, obj, **kwargs):
        self.af = AF_UNSPEC
        if isinstance(obj, AddressSpec):
            kwargs["af"] = obj.af
            kwargs["addr"] = obj
            obj = None
            if id_ == PF_POOL_NAT:
                kwargs["proxy_port"] = (PF_NAT_PROXY_PORT_LOW, PF_NAT_PROXY_PORT_HIGH)
        super(AddressPool, self).__init__(obj, **kwargs)
        self.id = id_
        # self._cur = AddressSpec(self.cur_)
        if self.id in (PF_POOL_NAT, PF_POOL_RDR):
            self._proxy_port = PortRange(self.proxy_port_range)

    @property
    def cur(self):
        return self._cur

    @cur.setter
    def cur(self, value):
        if not isinstance(value, AddressSpec):
            value = AddressSpec(value)
        self._cur = value

    @property
    def list(self):
        return list()

    @list.setter
    def list(self, value):
        pass

    @property
    def key(self):
        return "{:#010x}{:08x}{:08x}{:08x}".format(*self.key_.key32)

    @property
    def proxy_port(self):
        return self._proxy_port

    @proxy_port.setter
    def proxy_port(self, value):
        if not isinstance(value, PortRange):
            value = PortRange(value)
        self._proxy_port = value

    def _to_string(self):
        p1, p2 = self.proxy_port.operands
        s = ""

        # this should check the list for a single entry
        if self.ifname:
            if self.addr.addr is not None:
                s += "{.addr}@".format(self)
            s += self.ifname
        else:
            s += "{.addr}".format(self)

        if self.id == PF_POOL_NAT:
            if (p1, p2) != (PF_NAT_PROXY_PORT_LOW, PF_NAT_PROXY_PORT_HIGH) and (p1, p2) != (0, 0):
                if p1 == p2:
                    s += " port {}".format(p1)
                else:
                    s += " port {}:{}".format(p1, p2)
        elif self.id == PF_POOL_RDR:
            if p1:
                s += " port {}".format(p1)
                if p2 and (p2 != p1):
                    s += ":{}".format(p2)

        opt = self.opts & PF_POOL_TYPEMASK
        if opt == PF_POOL_BITMASK:
            s += " bitmask"
        elif opt == PF_POOL_RANDOM:
            s += " random"
        elif opt == PF_POOL_SRCHASH:
            s += " source-hash {0.key}".format(self)
        elif opt == PF_POOL_ROUNDROBIN:
            s += " round-robin"

        if self.opts & PF_POOL_STICKYADDR:
            s += " sticky-address"

        if (self.id == PF_POOL_NAT) and (p1 == p2 == 0):
            s += " static-port"

        return s
