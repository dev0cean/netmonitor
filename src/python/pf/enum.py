from syslog import LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR, LOG_WARNING, LOG_NOTICE, LOG_INFO, LOG_DEBUG
from pf.icmp import ICMP_UNREACH, ICMP_UNREACH_NET, ICMP_UNREACH_HOST, ICMP_UNREACH_PROTOCOL, ICMP_UNREACH_PORT, \
    ICMP_UNREACH_NEEDFRAG, ICMP_UNREACH_SRCFAIL, ICMP_UNREACH_NET_UNKNOWN, ICMP_UNREACH_HOST_UNKNOWN, \
    ICMP_UNREACH_ISOLATED, ICMP_UNREACH_NET_PROHIB, ICMP_UNREACH_HOST_PROHIB, ICMP_UNREACH_TOSNET, ICMP_UNREACH_TOSHOST, \
    ICMP_UNREACH_FILTER_PROHIB, ICMP_UNREACH_HOST_PRECEDENCE, ICMP_UNREACH_PRECEDENCE_CUTOFF, ICMP_REDIRECT_NET, \
    ICMP_REDIRECT_HOST, ICMP_REDIRECT_TOSNET, ICMP_REDIRECT_TOSHOST, ICMP_ROUTERADVERT_NORMAL, \
    ICMP_ROUTERADVERT_NOROUTE_COMMON, ICMP_TIMXCEED_INTRANS, ICMP_TIMXCEED_REASS, ICMP_PARAMPROB_ERRATPTR, \
    ICMP_PARAMPROB_OPTABSENT, ICMP_PARAMPROB_LENGTH, ICMP_PHOTURIS_UNKNOWN_INDEX, ICMP_PHOTURIS_AUTH_FAILED, \
    ICMP_PHOTURIS_DECRYPT_FAILED, ICMP_REDIRECT, ICMP_ROUTERADVERT, ICMP_TIMXCEED, ICMP_PARAMPROB, ICMP_PHOTURIS, \
    ICMP6_DST_UNREACH, ICMP6_TIME_EXCEEDED, ICMP6_PARAM_PROB, ND_REDIRECT, ND_REDIRECT_ONLINK, ND_REDIRECT_ROUTER, \
    ICMP6_PARAMPROB_NEXTHEADER, ICMP6_PARAMPROB_HEADER, ICMP6_TIME_EXCEED_REASSEMBLY, ICMP6_TIME_EXCEED_TRANSIT, \
    ICMP6_DST_UNREACH_NOPORT, ICMP6_DST_UNREACH_ADDR, ICMP6_DST_UNREACH_BEYONDSCOPE, ICMP6_DST_UNREACH_NOTNEIGHBOR, \
    ICMP6_DST_UNREACH_NOROUTE, ICMP6_DST_UNREACH_ADMIN, ICMP_ECHO, ICMP_ECHOREPLY, ICMP_SOURCEQUENCH, ICMP_ALTHOSTADDR, \
    ICMP_ROUTERSOLICIT, ICMP_TSTAMP, ICMP_TSTAMPREPLY, ICMP_IREQ, ICMP_IREQREPLY, ICMP_MASKREQ, ICMP_MASKREPLY, \
    ICMP_TRACEROUTE, ICMP_DATACONVERR, ICMP_MOBILE_REDIRECT, ICMP_IPV6_WHEREAREYOU, ICMP_IPV6_IAMHERE, \
    ICMP_MOBILE_REGREQUEST, ICMP_MOBILE_REGREPLY, ICMP_SKIP, ICMP6_PACKET_TOO_BIG, ICMP6_ECHO_REQUEST, ICMP6_ECHO_REPLY, \
    ICMP6_MEMBERSHIP_QUERY, MLD_LISTENER_QUERY, ICMP6_MEMBERSHIP_REPORT, MLD_LISTENER_REPORT, ICMP6_MEMBERSHIP_REDUCTION, \
    MLD_LISTENER_DONE, ND_ROUTER_SOLICIT, ND_ROUTER_ADVERT, ND_NEIGHBOR_SOLICIT, ND_NEIGHBOR_ADVERT, \
    ICMP6_ROUTER_RENUMBERING, ICMP6_WRUREQUEST, ICMP6_WRUREPLY, ICMP6_FQDN_QUERY, ICMP6_FQDN_REPLY, ICMP6_NI_QUERY, \
    ICMP6_NI_REPLY, MLD_MTRACE_RESP, MLD_MTRACE
from pf.pfvar import PF_LIMIT_STATES, PF_LIMIT_SRC_NODES, PF_LIMIT_FRAGS, PF_LIMIT_TABLES, PF_LIMIT_TABLE_ENTRIES, \
    PF_OP_NONE, PF_OP_IRG, PF_OP_EQ, PF_OP_NE, PF_OP_LT, PF_OP_LE, PF_OP_GT, PF_OP_GE, PF_OP_XRG, PF_OP_RRG, \
    PFTM_TCP_FIRST_PACKET, PFTM_TCP_OPENING, PFTM_TCP_ESTABLISHED, PFTM_TCP_CLOSING, PFTM_TCP_FIN_WAIT, PFTM_TCP_CLOSED, \
    PFTM_TS_DIFF, PFTM_UDP_FIRST_PACKET, PFTM_UDP_SINGLE, PFTM_UDP_MULTIPLE, PFTM_ICMP_FIRST_PACKET, \
    PFTM_ICMP_ERROR_REPLY, PFTM_OTHER_FIRST_PACKET, PFTM_OTHER_SINGLE, PFTM_OTHER_MULTIPLE, PFTM_FRAG, PFTM_INTERVAL, \
    PFTM_ADAPTIVE_START, PFTM_ADAPTIVE_END, PFTM_SRC_NODE, PFUDPS_NO_TRAFFIC, PFUDPS_SINGLE, PFUDPS_MULTIPLE
from pf.tcp_fsm import TCPS_CLOSED, TCPS_LISTEN, TCPS_SYN_SENT, TCPS_SYN_RECEIVED, TCPS_ESTABLISHED, TCPS_CLOSE_WAIT, \
    TCPS_FIN_WAIT_1, TCPS_CLOSING, TCPS_LAST_ACK, TCPS_FIN_WAIT_2, TCPS_TIME_WAIT

LOG_LEVELS = {
    "emerg": LOG_EMERG,
    "alert": LOG_ALERT,
    "crit": LOG_CRIT,
    "err": LOG_ERR,
    "warn": LOG_WARNING,
    "notice": LOG_NOTICE,
    "info": LOG_INFO,
    "debug": LOG_DEBUG
}

# Memory limits
PF_LIMITS = {
    "states": PF_LIMIT_STATES,
    "src-nodes": PF_LIMIT_SRC_NODES,
    "frags": PF_LIMIT_FRAGS,
    "tables": PF_LIMIT_TABLES,
    "table-entries": PF_LIMIT_TABLE_ENTRIES
}

# Ports, UIDs and GIDs operators
PF_OPS = {
    "": PF_OP_NONE,
    "><": PF_OP_IRG,
    "=": PF_OP_EQ,
    "!=": PF_OP_NE,
    "<": PF_OP_LT,
    "<=": PF_OP_LE,
    ">": PF_OP_GT,
    ">=": PF_OP_GE,
    "<>": PF_OP_XRG,
    ":": PF_OP_RRG,
}


# Global timeouts
PF_TIMEOUTS = {
    "tcp.first": PFTM_TCP_FIRST_PACKET,
    "tcp.opening": PFTM_TCP_OPENING,
    "tcp.established": PFTM_TCP_ESTABLISHED,
    "tcp.closing": PFTM_TCP_CLOSING,
    "tcp.finwait": PFTM_TCP_FIN_WAIT,
    "tcp.closed": PFTM_TCP_CLOSED,
    "tcp.tsdiff": PFTM_TS_DIFF,
    "udp.first": PFTM_UDP_FIRST_PACKET,
    "udp.single": PFTM_UDP_SINGLE,
    "udp.multiple": PFTM_UDP_MULTIPLE,
    "icmp.first": PFTM_ICMP_FIRST_PACKET,
    "icmp.error": PFTM_ICMP_ERROR_REPLY,
    "other.first": PFTM_OTHER_FIRST_PACKET,
    "other.single": PFTM_OTHER_SINGLE,
    "other.multiple": PFTM_OTHER_MULTIPLE,
    "frag": PFTM_FRAG,
    "interval": PFTM_INTERVAL,
    "adaptive.start": PFTM_ADAPTIVE_START,
    "adaptive.end": PFTM_ADAPTIVE_END,
    "src.track": PFTM_SRC_NODE
}

# PF Optimization Hints
PF_HINTS = {
    "normal": {
        "tcp.first": 2 * 60,
        "tcp.opening": 30,
        "tcp.established": 24 * 60 * 60,
        "tcp.closing": 15 * 60,
        "tcp.finwait": 45,
        "tcp.closed": 90,
        "tcp.tsdiff": 30
    },
    "satellite": {
        "tcp.first": 3 * 60,
        "tcp.opening": 30 + 5,
        "tcp.established": 24 * 60 * 60,
        "tcp.closing": 15 * 60 + 5,
        "tcp.finwait": 45 + 5,
        "tcp.closed": 90 + 5,
        "tcp.tsdiff": 60
    },
    "high-latency": {
        "tcp.first": 3 * 60,
        "tcp.opening": 30 + 5,
        "tcp.established": 24 * 60 * 60,
        "tcp.closing": 15 * 60 + 5,
        "tcp.finwait": 45 + 5,
        "tcp.closed": 90 + 5,
        "tcp.tsdiff": 60
    },
    "conservative": {
        "tcp.first": 60 * 60,
        "tcp.opening": 15 * 60,
        "tcp.established": 5 * 24 * 60 * 60,
        "tcp.closing": 60 * 60,
        "tcp.finwait": 10 * 60,
        "tcp.closed": 3 * 90,
        "tcp.tsdiff": 60
    },
    "aggressive": {
        "tcp.first": 30,
        "tcp.opening": 5,
        "tcp.established": 5 * 60 * 60,
        "tcp.closing": 60,
        "tcp.finwait": 30,
        "tcp.closed": 30,
        "tcp.tsdiff": 10
    }
}


# Dictionaries for mapping constants to strings
# TCP states
tcp_states = {
    TCPS_CLOSED: "CLOSED",
    TCPS_LISTEN: "LISTEN",
    TCPS_SYN_SENT: "SYN_SENT",
    TCPS_SYN_RECEIVED: "SYN_RCVD",
    TCPS_ESTABLISHED: "ESTABLISHED",
    TCPS_CLOSE_WAIT: "CLOSE_WAIT",
    TCPS_FIN_WAIT_1: "FIN_WAIT_1",
    TCPS_CLOSING: "CLOSING",
    TCPS_LAST_ACK: "LAST_ACK",
    TCPS_FIN_WAIT_2: "FIN_WAIT_2",
    TCPS_TIME_WAIT: "TIME_WAIT"
}

# UDP states
udp_states = {
    PFUDPS_NO_TRAFFIC: "NO_TRAFFIC",
    PFUDPS_SINGLE: "SINGLE",
    PFUDPS_MULTIPLE: "MULTIPLE"
}

# ICMP and ICMPv6 codes and types
icmp_codes = {
    (ICMP_UNREACH,        ICMP_UNREACH_NET):                 "net-unr",
    (ICMP_UNREACH,        ICMP_UNREACH_HOST):                "host-unr",
    (ICMP_UNREACH,        ICMP_UNREACH_PROTOCOL):            "proto-unr",
    (ICMP_UNREACH,        ICMP_UNREACH_PORT):                "port-unr",
    (ICMP_UNREACH,        ICMP_UNREACH_NEEDFRAG):            "needfrag",
    (ICMP_UNREACH,        ICMP_UNREACH_SRCFAIL):             "srcfail",
    (ICMP_UNREACH,        ICMP_UNREACH_NET_UNKNOWN):         "net-unk",
    (ICMP_UNREACH,        ICMP_UNREACH_HOST_UNKNOWN):        "host-unk",
    (ICMP_UNREACH,        ICMP_UNREACH_ISOLATED):            "isolate",
    (ICMP_UNREACH,        ICMP_UNREACH_NET_PROHIB):          "net-prohib",
    (ICMP_UNREACH,        ICMP_UNREACH_HOST_PROHIB):         "host-prohib",
    (ICMP_UNREACH,        ICMP_UNREACH_TOSNET):              "net-tos",
    (ICMP_UNREACH,        ICMP_UNREACH_TOSHOST):             "host-tos",
    (ICMP_UNREACH,        ICMP_UNREACH_FILTER_PROHIB):       "filter-prohib",
    (ICMP_UNREACH,        ICMP_UNREACH_HOST_PRECEDENCE):     "host-preced",
    (ICMP_UNREACH,        ICMP_UNREACH_PRECEDENCE_CUTOFF):   "cutoff-preced",
    (ICMP_REDIRECT,       ICMP_REDIRECT_NET):                "redir-net",
    (ICMP_REDIRECT,       ICMP_REDIRECT_HOST):               "redir-host",
    (ICMP_REDIRECT,       ICMP_REDIRECT_TOSNET):             "redir-tos-net",
    (ICMP_REDIRECT,       ICMP_REDIRECT_TOSHOST):            "redir-tos-host",
    (ICMP_ROUTERADVERT,   ICMP_ROUTERADVERT_NORMAL):         "normal-adv",
    (ICMP_ROUTERADVERT,   ICMP_ROUTERADVERT_NOROUTE_COMMON): "common-adv",
    (ICMP_TIMXCEED,       ICMP_TIMXCEED_INTRANS):            "transit",
    (ICMP_TIMXCEED,       ICMP_TIMXCEED_REASS):              "reassemb",
    (ICMP_PARAMPROB,      ICMP_PARAMPROB_ERRATPTR):          "badhead",
    (ICMP_PARAMPROB,      ICMP_PARAMPROB_OPTABSENT):         "optmiss",
    (ICMP_PARAMPROB,      ICMP_PARAMPROB_LENGTH):            "badlen",
    (ICMP_PHOTURIS,       ICMP_PHOTURIS_UNKNOWN_INDEX):      "unknown-ind",
    (ICMP_PHOTURIS,       ICMP_PHOTURIS_AUTH_FAILED):        "auth-fail",
    (ICMP_PHOTURIS,       ICMP_PHOTURIS_DECRYPT_FAILED):     "decrypt-fail",
}

icmp6_codes = {
    (ICMP6_DST_UNREACH,   ICMP6_DST_UNREACH_ADMIN):          "admin-unr",
    (ICMP6_DST_UNREACH,   ICMP6_DST_UNREACH_NOROUTE):        "noroute-unr",
    (ICMP6_DST_UNREACH,   ICMP6_DST_UNREACH_NOTNEIGHBOR):    "notnbr-unr",
    (ICMP6_DST_UNREACH,   ICMP6_DST_UNREACH_BEYONDSCOPE):    "beyond-unr",
    (ICMP6_DST_UNREACH,   ICMP6_DST_UNREACH_ADDR):           "addr-unr",
    (ICMP6_DST_UNREACH,   ICMP6_DST_UNREACH_NOPORT):         "port-unr",
    (ICMP6_TIME_EXCEEDED, ICMP6_TIME_EXCEED_TRANSIT):        "transit",
    (ICMP6_TIME_EXCEEDED, ICMP6_TIME_EXCEED_REASSEMBLY):     "reassemb",
    (ICMP6_PARAM_PROB,    ICMP6_PARAMPROB_HEADER):           "badhead",
    (ICMP6_PARAM_PROB,    ICMP6_PARAMPROB_NEXTHEADER):       "nxthdr",
    (ND_REDIRECT,         ND_REDIRECT_ONLINK):               "redironlink",
    (ND_REDIRECT,         ND_REDIRECT_ROUTER):               "redirrouter",
}

icmp_types = {
    ICMP_ECHO:                  "echoreq",
    ICMP_ECHOREPLY:             "echorep",
    ICMP_UNREACH:               "unreach",
    ICMP_SOURCEQUENCH:          "squench",
    ICMP_REDIRECT:              "redir",
    ICMP_ALTHOSTADDR:           "althost",
    ICMP_ROUTERADVERT:          "routeradv",
    ICMP_ROUTERSOLICIT:         "routersol",
    ICMP_TIMXCEED:              "timex",
    ICMP_PARAMPROB:             "paramprob",
    ICMP_TSTAMP:                "timereq",
    ICMP_TSTAMPREPLY:           "timerep",
    ICMP_IREQ:                  "inforeq",
    ICMP_IREQREPLY:             "inforep",
    ICMP_MASKREQ:               "maskreq",
    ICMP_MASKREPLY:             "maskrep",
    ICMP_TRACEROUTE:            "trace",
    ICMP_DATACONVERR:           "dataconv",
    ICMP_MOBILE_REDIRECT:       "mobredir",
    ICMP_IPV6_WHEREAREYOU:      "ipv6-where",
    ICMP_IPV6_IAMHERE:          "ipv6-here",
    ICMP_MOBILE_REGREQUEST:     "mobregreq",
    ICMP_MOBILE_REGREPLY:       "mobregrep",
    ICMP_SKIP:                  "skip",
    ICMP_PHOTURIS:              "photuris",
}

icmp6_types = {
    ICMP6_DST_UNREACH:          "unreach",
    ICMP6_PACKET_TOO_BIG:       "toobig",
    ICMP6_TIME_EXCEEDED:        "timex",
    ICMP6_PARAM_PROB:           "paramprob",
    ICMP6_ECHO_REQUEST:         "echoreq",
    ICMP6_ECHO_REPLY:           "echorep",
    ICMP6_MEMBERSHIP_QUERY:     "groupqry",
    MLD_LISTENER_QUERY:         "listqry",
    ICMP6_MEMBERSHIP_REPORT:    "grouprep",
    MLD_LISTENER_REPORT:        "listenrep",
    ICMP6_MEMBERSHIP_REDUCTION: "groupterm",
    MLD_LISTENER_DONE:          "listendone",
    ND_ROUTER_SOLICIT:          "routersol",
    ND_ROUTER_ADVERT:           "routeradv",
    ND_NEIGHBOR_SOLICIT:        "neighbrsol",
    ND_NEIGHBOR_ADVERT:         "neighbradv",
    ND_REDIRECT:                "redir",
    ICMP6_ROUTER_RENUMBERING:   "routrrenum",
    ICMP6_WRUREQUEST:           "wrureq",
    ICMP6_WRUREPLY:             "wrurep",
    ICMP6_FQDN_QUERY:           "fqdnreq",
    ICMP6_FQDN_REPLY:           "fqdnrep",
    ICMP6_NI_QUERY:             "niqry",
    ICMP6_NI_REPLY:             "nirep",
    MLD_MTRACE_RESP:            "mtraceresp",
    MLD_MTRACE:                 "mtrace",
}
