from ctypes import Structure, c_void_p, c_uint32, c_int, c_char, c_uint64, c_uint16, c_uint8

PF_OSFP_ANY = 0
PF_OSFP_UNKNOWN = -1    # (u_int32_t)-1
PF_OSFP_NOMATCH = -2    # (u_int32_t)-2

PF_OSFP_EXPANDED = 0x001    # expanded entry
PF_OSFP_GENERIC = 0x002     # generic signature
PF_OSFP_NODETAIL = 0x004    # no p0f details
PF_OSFP_LEN = 32


class pf_osfp_entry(Structure):
    """
    struct pf_osfp_entry {
        SLIST_ENTRY(pf_osfp_entry) fp_entry;
    #if !defined(__LP64__)
        u_int32_t        _pad;
    #endif /* !__LP64__ */
        pf_osfp_t        fp_os;
        int            fp_enflags;
    #define PF_OSFP_EXPANDED    0x001        /* expanded entry */
    #define PF_OSFP_GENERIC        0x002        /* generic signature */
    #define PF_OSFP_NODETAIL    0x004        /* no p0f details */
    #define PF_OSFP_LEN    32
        char            fp_class_nm[PF_OSFP_LEN];
        char            fp_version_nm[PF_OSFP_LEN];
        char            fp_subtype_nm[PF_OSFP_LEN];
    };
    """
    _fields_ = [
        ("fp_entry", c_void_p),     # SLIST_ENTRY(pf_osfp_entry)
        ("fp_os", c_uint32),        # pf_osfp_t
        ("fp_enflags", c_int),
        ("fp_class_nm", c_char * PF_OSFP_LEN),
        ("fp_version_nm", c_char * PF_OSFP_LEN),
        ("fp_subtype_nm", c_char * PF_OSFP_LEN),
    ]


class pf_osfp_ioctl(Structure):
    """
    struct pf_osfp_ioctl {
        struct pf_osfp_entry    fp_os;
        pf_tcpopts_t        fp_tcpopts;    /* packed TCP options */
        u_int16_t        fp_wsize;    /* TCP window size */
        u_int16_t        fp_psize;    /* ip->ip_len */
        u_int16_t        fp_mss;        /* TCP MSS */
        u_int16_t        fp_flags;
        u_int8_t        fp_optcnt;    /* TCP option count */
        u_int8_t        fp_wscale;    /* TCP window scaling */
        u_int8_t        fp_ttl;        /* IPv4 TTL */
        int            fp_getnum;    /* DIOCOSFPGET number */
    };
    """
    _fields_ = [
        ("fp_os", pf_osfp_entry),
        ("fp_tcpopts", c_uint64),   # pf_tcpopts_t
        ("fp_wsize", c_uint16),
        ("fp_psize", c_uint16),
        ("fp_mss", c_uint16),
        ("fp_flags", c_uint16),
        ("fp_optcnt", c_uint8),
        ("fp_wscale", c_uint8),
        ("fp_ttl", c_uint8),
        ("fp_getnum", c_int),
    ]

