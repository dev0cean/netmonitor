# ICMP types (from bsd/netinet/ip_icmp.h)
from _socket import AF_INET6


# noinspection PyPep8Naming
def ICMPPacket(version, header=(None, None), data=None):
    """
    Create an ICMPv4 or ICMPv6 packet.

    :param version: 4 or 6
    :type version: int
    :param header: two-tuple containing the type and code of the packet.
    :type header: (int, int)
    :param data: Contains data of the packet.  Can only assign a subclass of basestring
        or None.
    :type data: basestring | None
    """
    if version is 4:
        klass = ICMPv4Packet
    elif version is 6:
        klass = ICMPv6Packet
    else:
        raise ValueError("%d is not a valid IP version" % version)
    return klass(header, data)


# ICMP6 types (from bsd/netinet/icmp6.h)
ICMP6_DST_UNREACH = 1
ICMP6_PACKET_TOO_BIG = 2
ICMP6_TIME_EXCEEDED = 3
ICMP6_PARAM_PROB = 4
ICMP6_ECHO_REQUEST = 128
ICMP6_ECHO_REPLY = 129
ICMP6_MEMBERSHIP_QUERY = 130
MLD_LISTENER_QUERY = 130
ICMP6_MEMBERSHIP_REPORT = 131
MLD_LISTENER_REPORT = 131
ICMP6_MEMBERSHIP_REDUCTION = 132
MLD_LISTENER_DONE = 132
ND_ROUTER_SOLICIT = 133
ND_ROUTER_ADVERT = 134
ND_NEIGHBOR_SOLICIT = 135
ND_NEIGHBOR_ADVERT = 136
ND_REDIRECT = 137
ICMP6_ROUTER_RENUMBERING = 138
ICMP6_WRUREQUEST = 139
ICMP6_WRUREPLY = 140
ICMP6_FQDN_QUERY = 139
ICMP6_FQDN_REPLY = 140
ICMP6_NI_QUERY = 139
ICMP6_NI_REPLY = 140
MLDV2_LISTENER_REPORT = 143
MLD_MTRACE_RESP = 200
MLD_MTRACE = 201

# ICMP6 codes (from bsd/netinet/icmp6.h)
# Destination unreachable
ICMP6_DST_UNREACH_NOROUTE = 0
ICMP6_DST_UNREACH_ADMIN = 1
ICMP6_DST_UNREACH_NOTNEIGHBOR = 2
ICMP6_DST_UNREACH_BEYONDSCOPE = 2
ICMP6_DST_UNREACH_ADDR = 3
ICMP6_DST_UNREACH_NOPORT = 4

# Time exceeded
ICMP6_TIME_EXCEED_TRANSIT = 0
ICMP6_TIME_EXCEED_REASSEMBLY = 1

ICMP6_PARAMPROB_HEADER = 0
ICMP6_PARAMPROB_NEXTHEADER = 1
ICMP6_PARAMPROB_OPTION = 2

ND_REDIRECT_ONLINK = 0
ND_REDIRECT_ROUTER = 1

ICMP6_NI_SUBJ_IPV6 = 0
ICMP6_NI_SUBJ_FQDN = 1
ICMP6_NI_SUBJ_IPV4 = 2

ICMP6_NI_SUCCESS = 0
ICMP6_NI_REFUSED = 1
ICMP6_NI_UNKNOWN = 2

ICMP6_ROUTER_RENUMBERING_COMMAND = 0
ICMP6_ROUTER_RENUMBERING_RESULT = 1
ICMP6_ROUTER_RENUMBERING_SEQNUM_RESET = 255


class ICMPv6Packet(object):

    types = {
        # type: ("description", codes={ code: "description" })
        ICMP6_DST_UNREACH: ("Destination Unreachable", {
            ICMP6_DST_UNREACH_NOROUTE: "No route to destination",
            ICMP6_DST_UNREACH_ADMIN: "Communication with destination administratively prohibited",
            ICMP6_DST_UNREACH_BEYONDSCOPE: "Beyond scope of source address",
            ICMP6_DST_UNREACH_ADDR: "Address unreachable",
            ICMP6_DST_UNREACH_NOPORT: "Port unreachable",
            5: "Source address failed ingress/egress policy",
            6: "Reject route to destination",
            7: "Error in Source Routing header",
        }),
        ICMP6_PACKET_TOO_BIG: ("Packet Too Big", {0: ""}),
        ICMP6_TIME_EXCEEDED: (6, "Time Exceeded", {
            ICMP6_TIME_EXCEED_TRANSIT: "Hop limit exceeded in transit",
            ICMP6_TIME_EXCEED_REASSEMBLY: "Fragment reassembly time exceeded",
        }),
        ICMP6_PARAM_PROB: ("Parameter Problem", {
            ICMP6_PARAMPROB_HEADER: "Erroneous header field encountered",
            ICMP6_PARAMPROB_NEXTHEADER: "Unrecognized Next Header type encountered",
            ICMP6_PARAMPROB_OPTION: "Unrecognized IPv6 option encountered",
        }),
        ICMP6_ECHO_REQUEST: ("Echo Request", {0: ""}),
        ICMP6_ECHO_REPLY: ("Echo Reply", {0: ""}),
        ICMP6_MEMBERSHIP_QUERY: ("Multicast Listener Query", {0: ""}),
        ICMP6_MEMBERSHIP_REPORT: ("Multicast Listener Report", {0: ""}),
        ICMP6_MEMBERSHIP_REDUCTION: ("Multicast Listener Done", {0: ""}),
        ND_ROUTER_SOLICIT: ("Router Solicitation", {0: ""}),
        ND_ROUTER_ADVERT: ("Router Advertisement", {0: ""}),
        ND_NEIGHBOR_SOLICIT: ("Neighbor Solicitation", {0: ""}),
        ND_NEIGHBOR_ADVERT: ("Neighbor Advertisement", {0: ""}),
        ND_REDIRECT: ("Redirect Message", {0: ""}),
        ICMP6_ROUTER_RENUMBERING: ("Router Renumbering", {
            ICMP6_ROUTER_RENUMBERING_COMMAND: "Router Renumbering Command",
            ICMP6_ROUTER_RENUMBERING_RESULT: "Router Renumbering Result",
            ICMP6_ROUTER_RENUMBERING_SEQNUM_RESET: "Sequence Number Reset",
        }),
        ICMP6_NI_QUERY: ("ICMP Node Information Query", {
            ICMP6_NI_SUBJ_IPV6: "The Data field contains an IPv6 address which is the Subject of this Query",
            ICMP6_NI_SUBJ_FQDN: "The Data field contains a name which is the Subject of this Query, or is empty, "
                                "in the case of a NOOP",
            ICMP6_NI_SUBJ_IPV4: "The Data field contains an IPv4 address which is the Subject of this Query",
        }),
        ICMP6_NI_REPLY: ("ICMP Node Information Response", {
            ICMP6_NI_SUCCESS: "Successful reply",
            ICMP6_NI_REFUSED: "The Responder refuses to supply the answer",
            ICMP6_NI_UNKNOWN: "The Qtype of the Query is unknown to the Responder",
        }),
        141: ("Inverse Neighbor Discovery Solicitation Message", {0: ""}),
        142: ("Inverse Neighbor Discovery Advertisement Message", {0: ""}),
        MLDV2_LISTENER_REPORT: ("Multicast Listener Discovery Report", {0: ""}),
        144: ("Home Agent Address Discovery Request Message", {0: ""}),
        145: ("Home Agent Address Discovery Reply Message", {0: ""}),
        146: ("Mobile Prefix Solicitation", {0: ""}),
        147: ("Mobile Prefix Advertisement", {0: ""}),
        148: ("Certification Path Solicitation", {0: ""}),
        149: ("Certification Path Advertisement", {0: ""}),
        151: ("Multicast Router Advertisement", {0: ""}),
        152: ("Multicast Router Solicitation", {0: ""}),
        153: ("Multicast Router Termination", {0: ""}),
        155: ("RPL Control Message", {0: ""}),
    }

    def __init__(self, header=(None, None), data=None):
        """
        Create an ICMPv6 packet.

        :param header: two-tuple containing the type and code of the packet.
        :type header: (int, int)
        :param data: Contains data of the packet.  Can only assign a subclass of basestring
            or None.
        :type data: basestring | None
        """
        self._type = self._code = 0
        self.header = header
        self.data = data


# ICMP types (from bsd/netinet/ip_icmp.h)

ICMP_ECHOREPLY = 0
ICMP_UNREACH = 3
ICMP_SOURCEQUENCH = 4
ICMP_REDIRECT = 5
ICMP_ALTHOSTADDR = 6
ICMP_ECHO = 8
ICMP_ROUTERADVERT = 9
ICMP_ROUTERSOLICIT = 10
ICMP_TIMXCEED = 11
ICMP_PARAMPROB = 12
ICMP_TSTAMP = 13
ICMP_TSTAMPREPLY = 14
ICMP_IREQ = 15
ICMP_IREQREPLY = 16
ICMP_MASKREQ = 17
ICMP_MASKREPLY = 18
ICMP_TRACEROUTE = 30
ICMP_DATACONVERR = 31
ICMP_MOBILE_REDIRECT = 32
ICMP_IPV6_WHEREAREYOU = 33
ICMP_IPV6_IAMHERE = 34
ICMP_MOBILE_REGREQUEST = 35
ICMP_MOBILE_REGREPLY = 36
ICMP_SKIP = 39
ICMP_PHOTURIS = 40

# ICMP codes (from bsd/netinet/ip_icmp.h)

# Destination unreachable
ICMP_UNREACH_NET = 0
ICMP_UNREACH_HOST = 1
ICMP_UNREACH_PROTOCOL = 2
ICMP_UNREACH_PORT = 3
ICMP_UNREACH_NEEDFRAG = 4
ICMP_UNREACH_SRCFAIL = 5
ICMP_UNREACH_NET_UNKNOWN = 6
ICMP_UNREACH_HOST_UNKNOWN = 7
ICMP_UNREACH_ISOLATED = 8
ICMP_UNREACH_NET_PROHIB = 9
ICMP_UNREACH_HOST_PROHIB = 10
ICMP_UNREACH_TOSNET = 11
ICMP_UNREACH_TOSHOST = 12
ICMP_UNREACH_FILTER_PROHIB = 13
ICMP_UNREACH_HOST_PRECEDENCE = 14
ICMP_UNREACH_PRECEDENCE_CUTOFF = 15

# Shorter route
ICMP_REDIRECT_NET = 0
ICMP_REDIRECT_HOST = 1
ICMP_REDIRECT_TOSNET = 2
ICMP_REDIRECT_TOSHOST = 3

# Router advertisement
ICMP_ROUTERADVERT_NORMAL = 0
ICMP_ROUTERADVERT_NOROUTE_COMMON = 16

# Time exceeded
ICMP_TIMXCEED_INTRANS = 0
ICMP_TIMXCEED_REASS = 1

# IP header bad
ICMP_PARAMPROB_ERRATPTR = 0
ICMP_PARAMPROB_OPTABSENT = 1
ICMP_PARAMPROB_LENGTH = 2

# Photuris
ICMP_PHOTURIS_UNKNOWN_INDEX = 1
ICMP_PHOTURIS_AUTH_FAILED = 2
ICMP_PHOTURIS_DECRYPT_FAILED = 3


class ICMPv4Packet(object):

    types = {
        # type: ("description", codes={ code: "description" })
        ICMP_ECHOREPLY: ("Echo Reply", {0: ""}),
        ICMP_UNREACH: ("Destination Unreachable", {
            ICMP_UNREACH_NET: "Network Unreachable",
            ICMP_UNREACH_HOST: "Host Unreachable",
            ICMP_UNREACH_PROTOCOL: "Protocol Unreachable",
            ICMP_UNREACH_PORT: "Port Unreachable",
            ICMP_UNREACH_NEEDFRAG: "Fragmentation Needed and Don't Fragment was Set",
            ICMP_UNREACH_SRCFAIL: "Source Route Failed",
            ICMP_UNREACH_NET_UNKNOWN: "Destination Network Unknown",
            ICMP_UNREACH_HOST_UNKNOWN: "Destination Host Unknown",
            ICMP_UNREACH_ISOLATED: "Source Host Isolated",
            ICMP_UNREACH_NET_PROHIB: "Communication with Destination Network is Administratively Prohibited",
            ICMP_UNREACH_HOST_PROHIB: "Communication with Destination Host is Administratively Prohibited",
            ICMP_UNREACH_TOSNET: "Destination Network Unreachable for Type of Service",
            ICMP_UNREACH_TOSHOST: "Destination Host Unreachable for Type of Service",
            ICMP_UNREACH_FILTER_PROHIB: "Communication Administratively Prohibited",
            ICMP_UNREACH_HOST_PRECEDENCE: "Host Precedence Violation",
            ICMP_UNREACH_PRECEDENCE_CUTOFF: "Precedence cutoff in effect",
        }),
        ICMP_REDIRECT: ("Redirect", {
            ICMP_REDIRECT_NET: "Redirect for Network",
            ICMP_REDIRECT_HOST: "Redirect for Host",
            ICMP_REDIRECT_TOSNET: "Redirect for Type of Service and Network",
            ICMP_REDIRECT_TOSHOST: "Redirect for Type of Service and Host",
        }),
        ICMP_ALTHOSTADDR: ("Alternate Host Address", {0: ""}),
        ICMP_ECHO: ("Echo Request", {0: ""}),
        ICMP_ROUTERADVERT: ("Router Advertisement", {
            ICMP_ROUTERADVERT_NORMAL: "Normal Advertisement",
            ICMP_ROUTERADVERT_NOROUTE_COMMON: "Selective Routing",
        }),
        ICMP_ROUTERSOLICIT: ("Router Selection", {0: ""}),
        ICMP_TIMXCEED: ("Time Exceeded", {
            ICMP_TIMXCEED_INTRANS: "TTL Exceeded In Transit",
            ICMP_TIMXCEED_REASS: "Fragment Reassembly Time Exceeded",
        }),
        ICMP_PARAMPROB: ("Parameter Problem", {
            ICMP_PARAMPROB_ERRATPTR: "Pointer indicates the error",
            ICMP_PARAMPROB_OPTABSENT: "Missing a Required Option",
            ICMP_PARAMPROB_LENGTH: "Bad Length",
        }),
        ICMP_TSTAMP: ("Timestamp", {0: ""}),
        ICMP_TSTAMPREPLY: ("Timestamp Reply", {0: ""}),
        ICMP_IREQ: ("Information Request", {0: ""}),
        ICMP_IREQREPLY: ("Information Reply", {0: ""}),
        ICMP_MASKREQ: ("Address Mask Request", {0: ""}),
        ICMP_MASKREPLY: ("Address Mask Reply", {0: ""}),
        ICMP_TRACEROUTE: ("Traceroute", {0: ""}),
        ICMP_DATACONVERR: ("Data Conversion Error", {0: ""}),
        ICMP_MOBILE_REDIRECT: ("Mobile Host Redirect", {0: ""}),
        ICMP_IPV6_WHEREAREYOU: ("IPv6 Where Are You", {0: ""}),
        ICMP_IPV6_IAMHERE: ("IPv6 I Am Here", {0: ""}),
        ICMP_MOBILE_REGREQUEST: ("Mobile Registration Request", {0: ""}),
        ICMP_MOBILE_REGREPLY: ("Mobile Registration Reply", {0: ""}),
        ICMP_SKIP: ("SKIP", {0: ""}),
        ICMP_PHOTURIS: ("Photuris", {
            ICMP_PHOTURIS_UNKNOWN_INDEX: "Unknown Sec Index",
            ICMP_PHOTURIS_AUTH_FAILED: "Authentication Failed",
            ICMP_PHOTURIS_DECRYPT_FAILED: "Decrypt Failed",
        }),
    }

    def __init__(self, header=(None, None), data=None):
        """
        Create an ICMPv4 packet.

        :param header: two-tuple containing the type and code of the packet.
        :type header: (int, int)
        :param data: Contains data of the packet.  Can only assign a subclass of basestring
            or None.
        :type data: basestring | None
        """
        self.ttl = 64
        self._type = self._code = 0
        self.header = header
        self.data = data


def get_icmp_code_by_number(type_, code, af):
    """Return the ICMP code as a string."""
    cls = ICMPv6Packet if (af == AF_INET6) else ICMPv4Packet
    try:
        return cls.types[type_][1][code]
    except KeyError:
        return None


def get_icmp_type_by_number(type_, af):
    """Return the ICMP type as a string."""
    cls = ICMPv6Packet if (af == AF_INET6) else ICMPv4Packet
    try:
        return cls.types[type_][0]
    except KeyError:
        return None
