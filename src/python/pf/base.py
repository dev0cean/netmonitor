"""Base class for all PF-related objects"""
from abc import ABCMeta, abstractmethod, abstractproperty
import re
from pf.enum import PF_OPS
from pf.pfvar import PF_OP_NONE, PF_OP_EQ, PF_OP_NE, PF_OP_IRG, PF_OP_XRG, PF_OP_RRG


__all__ = ['StructWrapper']


class StructWrapper(object):
    """Base class for struct wrappers."""
    __metaclass__ = ABCMeta

    def __init__(self, obj=None, **kwargs):
        """Check the type of obj and initialize instance attributes."""
        if isinstance(obj, self._struct_type):
            self._from_struct(obj)
        else:
            self._struct = self._struct_type()
            if isinstance(obj, basestring):
                self._from_string(obj)
        self._from_kwargs(**kwargs)

    @abstractproperty
    def _struct_type(self):
        raise NotImplementedError()

    @property
    def _prefix(self):
        return ''

    def _from_struct(self, struct):
        """Initialize class attributes from an instance of its mapped struct type."""
        self._struct = struct
        # for k, v in struct.__dict__:
        #     if str.startswith(k, self._prefix):
        #         self.__setattr__(str.replace(k, self._prefix, ''), v)

    def _from_string(self, line):
        raise NotImplementedError()

    def _from_kwargs(self, **kwargs):
        for k, v in kwargs.iteritems():
            if hasattr(self, k):
                setattr(self, k, v)
            else:
                raise AttributeError("Unexpected argument: {}".format(k))

    def to_struct(self):
        return self._struct

    @abstractmethod
    def _to_string(self):
        raise NotImplementedError()

    def __eq__(self, other):
        return self._struct == other._struct

    def __str__(self):
        return self._to_string()

    def __repr__(self):
        return self._to_string()

    def __getattr__(self, key):
        if not key.startswith('_'):
            _key = key.rstrip('_')
            if hasattr(self._struct, self._prefix + _key):
                return getattr(self._struct, self._prefix + _key)
        return object.__getattribute__(self, key)

    def __setattr__(self, key, value):
        if not key.startswith('_'):
            _key = key.rstrip('_')
            if hasattr(self._struct, self._prefix + _key):
                setattr(self._struct, self._prefix + _key, value)
                return
        object.__setattr__(self, key, value)


class Constraint(StructWrapper):
    @abstractproperty
    def _struct_type(self):
        raise NotImplementedError()

    def __init__(self, operands=None, operator=PF_OP_NONE):
        """Check arguments and initialize instance attributes."""
        self.operator = operator

        if isinstance(operands, basestring) or isinstance(operands, self._struct_type):
            super(Constraint, self).__init__(operands)
        elif operands is None:
            self.operands = (None, None)
        elif isinstance(operands, int):
            self.operands = (operands, None)
        elif isinstance(operands, tuple):
            self.operands = operands

    def _from_string(self, predicate):
        """Initalize a new instance from a string."""
        op_re = ("(?:(?P<left>[0-9]+)?\s*"
                 "(?P<op>" + '|'.join(PF_OPS.keys[1:]) + "))?\s*"
                                                         "(?P<right>[0-9a-z-]+)?")

        match = re.compile(op_re).match(predicate)
        if not match:
            raise ValueError("Could not parse string: {}".format(predicate))

        self.operator = PF_OPS[match.group("op")] if match.group("op") else PF_OP_EQ
        try:
            right = int(match.group("right"))
        except ValueError:
            if self.operator in (PF_OP_EQ, PF_OP_NE):
                right = self._str_to_num(match.group("right"))
            else:
                raise
        if self.operator in (PF_OP_IRG, PF_OP_XRG, PF_OP_RRG):
            left = int(match.group("left"))
            self.operands = (left, right)
        else:
            self.operands = (right, None)

    def _to_string(self):
        """Return the string representation of the operation."""
        left, right = self.operands

        if self.operator == PF_OP_NONE:
            return str(left) if left else ""
        if self.operator in (PF_OP_EQ, PF_OP_NE):
            left = self._num_to_str(left)

        if right is None:
            s = "{0} {1}"
        else:
            s = "{1}{0}{2}" if self.operator is PF_OP_RRG else "{1} {0} {2}"
        return s.format(PF_OPS[self.operator], left, right)

    @abstractmethod
    def _num_to_str(self, n):
        """Convert a numeric operand to a string."""
        raise NotImplementedError()

    @abstractmethod
    def _str_to_num(self, s):
        """Convert a string to a numeric operand."""
        raise NotImplementedError()

    def __eq__(self, predicate):
        return self.operands == predicate.operands and self.operator == predicate.operator

    def __ne__(self, predicate):
        return not self.__eq__(predicate)

