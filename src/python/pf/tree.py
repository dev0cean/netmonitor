# from libkern/tree.h
from ctypes import Structure, POINTER

RB_classes = {}
RB_ENTRY_classes = {}
RB_BLACK = 0
RB_RED = 1
RB_PLACEHOLDER = None


def RB_HEAD(name, type_):
    return RB_classes.setdefault(
        name,
        type(name, (Structure,), {
            "_fields_": [
                ("rbh_root", POINTER(type_)),   # root of the tree
            ]
        }))


def RB_INIT(root):
    root.rbh_root.content = None


def RB_ENTRY(type_):
    return RB_classes.setdefault(
        type_,
        type("RB_ENTRY_{.__name__}".format(type_), (Structure,), {
            "_fields_": [
                ("rbe_parent", POINTER(type_)),
                ("rbe_left", POINTER(type_)),
                ("rbe_right", POINTER(type_)),
            ]
        }))
