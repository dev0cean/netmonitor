""" from bsd/net/pfvar.h """
from _socket import AF_INET, AF_INET6, htonl, ntohl
from ctypes import Structure, addressof, sizeof, c_char, c_uint32, c_uint16, c_void_p, c_int
from pf.tcp_fsm import TCP_NSTATES


class BufferStructure(Structure):
    """A subclass of ctypes.Structure to simplify ioctl() system calls."""

    def __init__(self, *args, **kwargs):
        """Call the parent constructor"""
        super(BufferStructure, self).__init__(*args, **kwargs)

    # noinspection PyPep8Naming
    def asBuffer(self):
        """Return a buffer pointing to the Structure.

        This allows ioctl() to write directly to the structure, even if bigger
        than 1024 bytes.
        """
        return (c_char * sizeof(self)).from_address(addressof(self))


PF_MD5_DIGEST_LENGTH = 16


# Address manipulation functions

def PF_AEQ(a, b, af):
    return (
        (af == AF_INET and a.addr32[0] == b.addr32[0]) or
        (a.addr32[3] == b.addr32[3] and
         a.addr32[2] == b.addr32[2] and
         a.addr32[1] == b.addr32[1] and
         a.addr32[0] == b.addr32[0]))


def PF_ANEQ(a, b, af):
    return (
        (af == AF_INET and a.addr32[0] != b.addr32[0]) or
        (a.addr32[3] != b.addr32[3] or
         a.addr32[2] != b.addr32[2] or
         a.addr32[1] != b.addr32[1] or
         a.addr32[0] != b.addr32[0]))


def PF_ALEQ(a, b, af):
    return (
        (af == AF_INET and a.addr32[0] <= b.addr32[0]) or
        (a.addr32[3] <= b.addr32[3] and
         a.addr32[2] <= b.addr32[2] and
         a.addr32[1] <= b.addr32[1] and
         a.addr32[0] <= b.addr32[0]))


def PF_AZERO(a, af):
    return (
        (af == AF_INET and not a.addr32[0]) or
        (not a.addr32[0] and
         not a.addr32[1] and
         not a.addr32[2] and
         not a.addr32[3]))


def pf_match_addr(n, a, m, b, af):
    """
    Return True if the addresses a and b match (with mask m), otherwise return False.
    If n is False, they match if they are equal. If n is True, they match if they are different.
    """
    match = False
    if af == AF_INET:
        match = (a.addr32[0] & m.addr32[0]) == (b.addr32[0] & m.addr32[0])
    elif af == AF_INET6:
        match = (((a.addr32[0] & m.addr32[0]) == (b.addr32[0] & m.addr32[0])) and
                 ((a.addr32[1] & m.addr32[1]) == (b.addr32[1] & m.addr32[1])) and
                 ((a.addr32[2] & m.addr32[2]) == (b.addr32[2] & m.addr32[2])) and
                 ((a.addr32[3] & m.addr32[3]) == (b.addr32[3] & m.addr32[3])))
    return match ^ n

PF_MATCHA = pf_match_addr


def pf_addrcpy(dst, src, af):
    if af == AF_INET:
        dst.addr32[0] = src.addr32[0]
    elif af == AF_INET6:
        dst.addr32[0] = src.addr32[0]
        dst.addr32[1] = src.addr32[1]
        dst.addr32[2] = src.addr32[2]
        dst.addr32[3] = src.addr32[3]

PF_ACPY = pf_addrcpy


def pf_addr_inc(addr, af):
    if af == AF_INET:
        addr.addr32[0] = htonl(ntohl(addr.addr32[0]) + 1)
    elif af == AF_INET6:
        if addr.addr32[3] == 0xffffffff:
            addr.addr32[3] = 0
            if addr.addr32[2] == 0xffffffff:
                addr.addr32[2] = 0
                if addr.addr32[1] == 0xffffffff:
                    addr.addr32[1] = 0
                    addr.addr32[0] = htonl(ntohl(addr.addr32[0]) + 1)
                else:
                    addr.addr32[1] = htonl(ntohl(addr.addr32[1]) + 1)
            else:
                addr.addr32[2] = htonl(ntohl(addr.addr32[2]) + 1)
        else:
            addr.addr32[3] = htonl(ntohl(addr.addr32[3]) + 1)

PF_AINC = pf_addr_inc


def pf_poolmask(naddr, raddr, rmask, saddr, af):
    if af == AF_INET:
        naddr.addr32[0] = (raddr.addr32[0] & rmask.addr32[0]) | ((rmask.addr32[0] ^ 0xffffffff) & saddr.addr32[0])
    elif af == AF_INET6:
        naddr.addr32[0] = (raddr.addr32[0] & rmask.addr32[0]) | ((rmask.addr32[0] ^ 0xffffffff) & saddr.addr32[0])
        naddr.addr32[1] = (raddr.addr32[1] & rmask.addr32[1]) | ((rmask.addr32[1] ^ 0xffffffff) & saddr.addr32[1])
        naddr.addr32[2] = (raddr.addr32[2] & rmask.addr32[2]) | ((rmask.addr32[2] ^ 0xffffffff) & saddr.addr32[2])
        naddr.addr32[3] = (raddr.addr32[3] & rmask.addr32[3]) | ((rmask.addr32[3] ^ 0xffffffff) & saddr.addr32[3])

PF_POOLMASK = pf_poolmask


PF_TCPS_PROXY_SRC = TCP_NSTATES + 0
PF_TCPS_PROXY_DST = TCP_NSTATES + 1

# direction enum
PF_INOUT = 0
PF_IN = 1
PF_OUT = 2

# action enum
PF_PASS = 0
PF_DROP = 1
PF_SCRUB = 2
PF_NOSCRUB = 3
PF_NAT = 4
PF_NONAT = 5
PF_BINAT = 6
PF_NOBINAT = 7
PF_RDR = 8
PF_NORDR = 9
PF_SYNPROXY_DROP = 10
PF_DUMMYNET = 11
PF_NODUMMYNET = 12

# ruleset enum
PF_RULESET_SCRUB = 0
PF_RULESET_FILTER = 1
PF_RULESET_NAT = 2
PF_RULESET_BINAT = 3
PF_RULESET_RDR = 4
PF_RULESET_DUMMYNET = 5
PF_RULESET_MAX = 6

# operator enum
PF_OP_NONE = 0
PF_OP_IRG = 1
PF_OP_EQ = 2
PF_OP_NE = 3
PF_OP_LT = 4
PF_OP_LE = 5
PF_OP_GT = 6
PF_OP_GE = 7
PF_OP_XRG = 8
PF_OP_RRG = 9

# debug level enum
PF_DEBUG_NONE = 0
PF_DEBUG_URGENT = 1
PF_DEBUG_MISC = 2
PF_DEBUG_NOISY = 3

# change enum
PF_CHANGE_NONE = 0
PF_CHANGE_ADD_HEAD = 1
PF_CHANGE_ADD_TAIL = 2
PF_CHANGE_ADD_BEFORE = 3
PF_CHANGE_ADD_AFTER = 4
PF_CHANGE_REMOVE = 5
PF_CHANGE_GET_TICKET = 6

PF_GET_NONE = 0
PF_GET_CLR_CNTR = 1

# timeouts enum
PFTM_TCP_FIRST_PACKET = 0
PFTM_TCP_OPENING = 1
PFTM_TCP_ESTABLISHED = 2
PFTM_TCP_CLOSING = 3
PFTM_TCP_FIN_WAIT = 4
PFTM_TCP_CLOSED = 5
PFTM_UDP_FIRST_PACKET = 6
PFTM_UDP_SINGLE = 7
PFTM_UDP_MULTIPLE = 8
PFTM_ICMP_FIRST_PACKET = 9
PFTM_ICMP_ERROR_REPLY = 10
PFTM_GREv1_FIRST_PACKET = 11
PFTM_GREv1_INITIATING = 12
PFTM_GREv1_ESTABLISHED = 13
PFTM_ESP_FIRST_PACKET = 14
PFTM_ESP_INITIATING = 15
PFTM_ESP_ESTABLISHED = 16
PFTM_OTHER_FIRST_PACKET = 17
PFTM_OTHER_SINGLE = 18
PFTM_OTHER_MULTIPLE = 19
PFTM_FRAG = 20
PFTM_INTERVAL = 21
PFTM_ADAPTIVE_START = 22
PFTM_ADAPTIVE_END = 23
PFTM_SRC_NODE = 24
PFTM_TS_DIFF = 25
PFTM_MAX = 26
PFTM_PURGE = 27
PFTM_UNLINKED = 28
PFTM_UNTIL_PACKET = 29

# PFTM default values
PFTM_TCP_FIRST_PACKET_VAL = 120    # First TCP packet
PFTM_TCP_OPENING_VAL = 30    # No response yet
PFTM_TCP_ESTABLISHED_VAL = (24 * 60 * 60)    # Established
PFTM_TCP_CLOSING_VAL = (15 * 60)    # Half closed
PFTM_TCP_FIN_WAIT_VAL = 45    # Got both FINs
PFTM_TCP_CLOSED_VAL = 90    # Got a RST
PFTM_UDP_FIRST_PACKET_VAL = 60    # First UDP packet
PFTM_UDP_SINGLE_VAL = 30    # Unidirectional
PFTM_UDP_MULTIPLE_VAL = 60    # Bidirectional
PFTM_ICMP_FIRST_PACKET_VAL = 20    # First ICMP packet
PFTM_ICMP_ERROR_REPLY_VAL = 10    # Got error response
PFTM_GREv1_FIRST_PACKET_VAL = 120
PFTM_GREv1_INITIATING_VAL = 30
PFTM_GREv1_ESTABLISHED_VAL = 1800
PFTM_ESP_FIRST_PACKET_VAL = 120
PFTM_ESP_INITIATING_VAL = 30
PFTM_ESP_ESTABLISHED_VAL = 900
PFTM_OTHER_FIRST_PACKET_VAL = 60    # First packet
PFTM_OTHER_SINGLE_VAL = 30    # Unidirectional
PFTM_OTHER_MULTIPLE_VAL = 60    # Bidirectional
PFTM_FRAG_VAL = 30    # Fragment expire
PFTM_INTERVAL_VAL = 10    # Expire interval
PFTM_SRC_NODE_VAL = 0    # Source tracking
PFTM_TS_DIFF_VAL = 30    # Allowed TS diff

# routes enum
PF_NOPFROUTE = 0
PF_FASTROUTE = 1
PF_ROUTETO = 2
PF_DUPTO = 3
PF_REPLYTO = 4

# limits enum
PF_LIMIT_STATES = 0
PF_LIMIT_APP_STATES = 1
PF_LIMIT_SRC_NODES = 2
PF_LIMIT_FRAGS = 3
PF_LIMIT_TABLES = 4
PF_LIMIT_TABLE_ENTRIES = 5
PF_LIMIT_MAX = 6

PF_POOL_IDMASK = 0x0f


PF_WSCALE_FLAG = 0x80
PF_WSCALE_MASK = 0x0f

PF_LOG = 0x01
PF_LOG_ALL = 0x02
PF_LOG_SOCKET_LOOKUP = 0x04


PF_TABLE_NAME_SIZE = 32

RTLABEL_LEN = 32


PF_SKIP_IFP = 0
PF_SKIP_DIR = 1
PF_SKIP_AF = 2
PF_SKIP_PROTO = 3
PF_SKIP_SRC_ADDR = 4
PF_SKIP_SRC_PORT = 5
PF_SKIP_DST_ADDR = 6
PF_SKIP_DST_PORT = 7
PF_SKIP_COUNT = 8

PF_RULE_LABEL_SIZE = 64
PF_QNAME_SIZE = 64
PF_TAG_NAME_SIZE = 64
PF_OWNER_NAME_SIZE = 64

PF_STATE_NORMAL = 0x1
PF_STATE_MODULATE = 0x2
PF_STATE_SYNPROXY = 0x3

# service class categories
SCIDX_MASK = 0x0f
SC_BE = 0x10
SC_BK_SYS = 0x11
SC_BK = 0x12
SC_RD = 0x13
SC_OAM = 0x14
SC_AV = 0x15
SC_RV = 0x16
SC_VI = 0x17
SC_VO = 0x18
SC_CTL = 0x19

# diffserve code points
DSCP_MASK = 0xfc
DSCP_CUMASK = 0x03
DSCP_EF = 0xb8
DSCP_AF11 = 0x28
DSCP_AF12 = 0x30
DSCP_AF13 = 0x38
DSCP_AF21 = 0x48
DSCP_AF22 = 0x50
DSCP_AF23 = 0x58
DSCP_AF31 = 0x68
DSCP_AF32 = 0x70
DSCP_AF33 = 0x78
DSCP_AF41 = 0x88
DSCP_AF42 = 0x90
DSCP_AF43 = 0x98
AF_CLASSMASK = 0xe0
AF_DROPPRECMASK = 0x18

PF_FLUSH = 0x01
PF_FLUSH_GLOBAL = 0x02

PFSTATE_NOSYNC = 0x01
PFSTATE_FROMSYNC = 0x02
PFSTATE_STALE = 0x04

# Unified state structures for pulling states out of the kernel
# used by pfsync(4) and the pf(4) ioctl.

PFSYNC_SCRUB_FLAG_VALID = 0x01


MAXPATHLEN = 1024   # from bsd/sys/param.h


PFR_FB_NONE = 0
PFR_FB_MATCH = 1
PFR_FB_ADDED = 2
PFR_FB_DELETED = 3
PFR_FB_CHANGED = 4
PFR_FB_CLEARED = 5
PFR_FB_DUPLICATE = 6
PFR_FB_NOTMATCH = 7
PFR_FB_CONFLICT = 8
PFR_FB_MAX = 9


# flow direction
PFR_DIR_IN = 0
PFR_DIR_OUT = 1
PFR_DIR_MAX = 2

# rule operation
PFR_OP_BLOCK = 0
PFR_OP_PASS = 1
PFR_OP_ADDR_MAX = 2
PFR_OP_TABLE_MAX = 3
PFR_OP_XPASS = PFR_OP_ADDR_MAX


PFI_IFLAG_SKIP = 0x0100     # skip filtering on interface

# flags for RDR options
PF_DPORT_RANGE = 0x01        # Dest port uses range
PF_RPORT_RANGE = 0x02        # RDR'ed port uses range

# Reasons code for passing/dropping a packet
PFRES_MATCH = 0        # Explicit match of a rule
PFRES_BADOFF = 1        # Bad offset for pull_hdr
PFRES_FRAG = 2        # Dropping following fragment
PFRES_SHORT = 3        # Dropping short packet
PFRES_NORM = 4        # Dropping by normalizer
PFRES_MEMORY = 5        # Dropped due to lacking mem
PFRES_TS = 6        # Bad TCP Timestamp (RFC1323)
PFRES_CONGEST = 7        # Congestion (of ipintrq)
PFRES_IPOPTIONS = 8        # IP option
PFRES_PROTCKSUM = 9        # Protocol checksum invalid
PFRES_BADSTATE = 10        # State mismatch
PFRES_STATEINS = 11        # State insertion failure
PFRES_MAXSTATES = 12        # State limit
PFRES_SRCLIMIT = 13        # Source node/conn limit
PFRES_SYNPROXY = 14        # SYN proxy
PFRES_DUMMYNET = 15        # Dummynet
PFRES_MAX = 16        # total+1

PFRES_NAMES = [
    "match",
    "bad-offset",
    "fragment",
    "short",
    "normalize",
    "memory",
    "bad-timestamp",
    "congestion",
    "ip-option",
    "proto-cksum",
    "state-mismatch",
    "state-insert",
    "state-limit",
    "src-limit",
    "synproxy",
    "dummynet",
]

# Counters for other things we want to keep track of
LCNT_STATES = 0            # states
LCNT_SRCSTATES = 1        # max-src-states
LCNT_SRCNODES = 2        # max-src-nodes
LCNT_SRCCONN = 3        # max-src-conn
LCNT_SRCCONNRATE = 4    # max-src-conn-rate
LCNT_OVERLOAD_TABLE = 5    # entry added to overload table
LCNT_OVERLOAD_FLUSH = 6    # state entries flushed
LCNT_MAX = 7            # total+1

LCNT_NAMES = [
    "max states per rule",
    "max-src-states",
    "max-src-nodes",
    "max-src-conn",
    "max-src-conn-rate",
    "overload table insertion",
    "overload flush states",
]

# UDP state enumeration
PFUDPS_NO_TRAFFIC = 0
PFUDPS_SINGLE = 1
PFUDPS_MULTIPLE = 2
PFUDPS_NSTATES = 3    # number of state levels

PFUDPS_NAMES = [
    "NO_TRAFFIC",
    "SINGLE",
    "MULTIPLE",
]

# GREv1 protocol state enumeration
PFGRE1S_NO_TRAFFIC = 0
PFGRE1S_INITIATING = 1
PFGRE1S_ESTABLISHED = 2
PFGRE1S_NSTATES = 3    # number of state levels

PFGRE1S_NAMES = [
    "NO_TRAFFIC",
    "INITIATING",
    "ESTABLISHED",
]

PFESPS_NO_TRAFFIC = 0
PFESPS_INITIATING = 1
PFESPS_ESTABLISHED = 2
PFESPS_NSTATES = 3    # number of state levels

PFESPS_NAMES = [
    "NO_TRAFFIC",
    "INITIATING",
    "ESTABLISHED",
]

# Other protocol state enumeration
PFOTHERS_NO_TRAFFIC = 0
PFOTHERS_SINGLE = 1
PFOTHERS_MULTIPLE = 2
PFOTHERS_NSTATES = 3    # number of state levels

PFOTHERS_NAMES = [
    "NO_TRAFFIC",
    "SINGLE",
    "MULTIPLE",
]

FCNT_STATE_SEARCH = 0
FCNT_STATE_INSERT = 1
FCNT_STATE_REMOVALS = 2
FCNT_MAX = 3

FCNT_NAMES = [
    "searches",
    "inserts",
    "removals",
]

SCNT_SRC_NODE_SEARCH = 0
SCNT_SRC_NODE_INSERT = 1
SCNT_SRC_NODE_REMOVALS = 2
SCNT_MAX = 3

SCNT_NAMES = [
    "searches",
    "inserts",
    "removals",
]


# bandwidth types
PF_ALTQ_BW_ABSOLUTE = 1    # bw in absolute value (bps)
PF_ALTQ_BW_PERCENT = 2  # bandwidth in percentage

# ALTQ rule flags
PF_ALTQF_TBR = 0x1  # enable Token Bucket Regulator

# queue rule flags
PF_ALTQ_QRF_WEIGHT = 0x1    # weight instead of priority


class pf_tagname(Structure):
    """
    struct pf_tagname {
        TAILQ_ENTRY(pf_tagname)    entries;
        char            name[PF_TAG_NAME_SIZE];
        u_int16_t        tag;
        int            ref;
    };
    """
pf_tagname._fields_ = [
    ("entries", c_void_p * 2),  # TAILQ_ENTRY(pf_tagname)
    ("name", c_char * PF_TAG_NAME_SIZE),
    ("tag", c_uint16),
    ("ref", c_int),
]

PFFRAG_FRENT_HIWAT = 5000        # Number of fragment entries
PFFRAG_FRAG_HIWAT = 1000        # Number of fragmented packets
PFFRAG_FRCENT_HIWAT = 50000        # Number of fragment cache entries
PFFRAG_FRCACHE_HIWAT = 10000    # Number of fragment descriptors

PFR_KTABLE_HIWAT = 1000            # Number of tables
PFR_KENTRY_HIWAT = 200000        # Number of table entries
PFR_KENTRY_HIWAT_SMALL = 100000    # Number of table entries (tiny hosts)


PF_THRESHOLD_MULT = 1000
PF_THRESHOLD_MAX = 0xffffffff / PF_THRESHOLD_MULT


class pf_threshold(Structure):
    """
    struct pf_threshold {
        u_int32_t	limit;
        #define	PF_THRESHOLD_MULT	1000
        #define PF_THRESHOLD_MAX	0xffffffff / PF_THRESHOLD_MULT
        u_int32_t	seconds;
        u_int32_t	count;
        u_int32_t	last;
    };
    """
    _fields_ = [
        ("limit", c_uint32),
        ("seconds", c_uint32),
        ("count", c_uint32),
        ("last", c_uint32),
    ]
