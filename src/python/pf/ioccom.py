""" from bsd/sys/ioccom.h """

from ctypes import *

# Ioctl's have the command encoded in the lower word, and the size of
# any in or out parameters in the upper word.  The high 3 bits of the
# upper word are used to encode the in/out status of the parameter.

IOCPARM_MASK = 0x1fff		# parameter length, at most 13 bits
IOCPARM_MAX = (IOCPARM_MASK + 1)    # max size of ioctl args


def IOCPARM_LEN(x):
    return (x >> 16) & IOCPARM_MASK


def IOCBASECMD(x):
    return x & ~(IOCPARM_MASK << 16)


def IOCGROUP(x):
    return (x >> 8) & 0xff


# no parameters
IOC_VOID = 0x20000000L
# copy parameters out
IOC_OUT = 0x40000000L
# copy parameters in
IOC_IN = 0x80000000L
# copy paramters in and out
IOC_INOUT = IOC_IN | IOC_OUT
# mask for IN/OUT/VOID
IOC_DIRMASK = 0xe0000000L


# noinspection PyPep8Naming
def _IOC(inout, group, num, _len):
    return inout | ((_len & IOCPARM_MASK) << 16) | (group << 8) | num


# noinspection PyPep8Naming
def _IO(group, num):
    return _IOC(IOC_VOID, ord(group), num, 0)


# noinspection PyPep8Naming
def _IOR(group, num, _type):
    return _IOC(IOC_OUT, ord(group), num, sizeof(_type))


# noinspection PyPep8Naming
def _IOW(group, num, _type):
    return _IOC(IOC_IN, ord(group), num, sizeof(_type))


# this should be _IORW, but stdio got there first
# noinspection PyPep8Naming
def _IOWR(group, num, _type):
    return _IOC(IOC_INOUT, ord(group), num, sizeof(_type))
