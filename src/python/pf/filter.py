import os
from socket import htonl, AF_UNSPEC
from ctypes import sizeof, addressof, c_uint32
from errno import EEXIST, ENOENT, EBUSY, EINVAL
from fcntl import ioctl
from pf.enum import LOG_LEVELS, PF_LIMITS, PF_TIMEOUTS, PF_HINTS
from pf.exception import PFError
from pf.iface import Interface, pfi_kif
from pf.ioctl import pfioc_trans_e, pfioc_trans, DIOCXBEGIN, DIOCXCOMMIT, DIOCXROLLBACK, DIOCSTART, DIOCSTOP, \
    DIOCSETDEBUG, DIOCSETHOSTID, pfioc_limit, DIOCGETLIMIT, DIOCSETLIMIT, pfioc_tm, DIOCGETTIMEOUT, DIOCSETTIMEOUT, \
    pfioc_iface, DIOCIGETIFACES, DIOCSETIFFLAG, DIOCCLRIFFLAG, DIOCSETSTATUSIF, DIOCGETSTATUS, DIOCCLRSTATUS, \
    pfioc_states, DIOCGETSTATES, pfioc_state_kill, DIOCCLRSTATES, DIOCKILLSTATES, pfioc_rule, DIOCGETRULES, \
    DIOCGETRULE, pfioc_table, PFR_FLAG_ADDRSTOO, DIOCRINADEFINE, DIOCADDRULE, DIOCRADDTABLES, DIOCRCLRTABLES, \
    DIOCRDELTABLES, DIOCRGETTABLES, DIOCRADDADDRS, DIOCRCLRADDRS, DIOCRDELADDRS, DIOCRSETADDRS, DIOCRGETADDRS, \
    DIOCRGETTSTATS, DIOCRCLRTSTATS
from pf.pfvar import PF_GET_CLR_CNTR, PF_PASS, PFI_IFLAG_SKIP
from pf.rule import Ruleset, Rule
from pf.state import pfsync_state, State
from pf.status import Status, pf_status
from pf.table import Table, Address, TableStats, pfr_addr, pfr_table, pfr_tstats


class _Transaction(object):
    """Class for managing transactions with the Packet Filter subsystem."""

    def __init__(self, dev, path="", *trans_type):
        """Initialize the required structures."""
        self.dev = dev
        self.size = len(trans_type)
        self.array = (pfioc_trans_e * self.size)()

        for a, t in zip(self.array, trans_type):
            a.type = t
            a.anchor = path

        self._pt = pfioc_trans(size=self.size, esize=sizeof(pfioc_trans_e), array=addressof(self.array))

    def __enter__(self):
        """Start the transaction."""
        ioctl(self.dev, DIOCXBEGIN, self._pt.asBuffer())
        return self

    def __exit__(self, type, value, traceback):
        """Commit changes if no exceptions occurred; otherwise, rollback."""
        if type is None:
            ioctl(self.dev, DIOCXCOMMIT, self._pt.asBuffer())
        else:
            ioctl(self.dev, DIOCXROLLBACK, self._pt.asBuffer())


class PacketFilter(object):
    """Class representing the kernel's packet filtering subsystem.

    It provides a set of methods that allow you to send commands to the kernel
    through the ioctl(2) interface provided by the pf(4) pseudo-device.
    Basically, all methods in this class are just wrappers to ioctl(2) calls,
    and may consequently raise IOError if the ioctl() request fails.
    """

    def __init__(self, dev="/dev/pf"):
        """Set the pf device."""
        self.dev = dev

    def enable(self):
        """Enable Packet Filtering."""
        with open(self.dev, 'w') as d:
            try:
                ioctl(d, DIOCSTART)
            except IOError, (e, s):
                if e != EEXIST:       # EEXIST means PF is already enabled
                    raise

    def disable(self):
        """Disable Packet Filtering."""
        with open(self.dev, 'w') as d:
            try:
                ioctl(d, DIOCSTOP)
            except IOError, (e, s):
                if e != ENOENT:       # ENOENT means PF is already disabled
                    raise

    def set_debug(self, level):
        """Set the debug level.

        The debug level can be either one of the LOG_* constants or a string.
        """
        if level in LOG_LEVELS:
            level = LOG_LEVELS[level]

        with open(self.dev, 'w') as d:
            with _Transaction(d):
                ioctl(d, DIOCSETDEBUG, c_uint32(level))

    def set_hostid(self, hostid):
        """Set the host ID.

        The host ID is used by pfsync to identify the host that created a state
        table entry. 'hostid' must be a 32-bit unsigned integer.
        """
        with open(self.dev, 'w') as d:
            with _Transaction(d):
                ioctl(d, DIOCSETHOSTID, c_uint32(htonl(hostid)))

    # UNSUPPORTED ON MAC OS X
    # def set_reassembly(self, reassembly):
    #     """Enable reassembly of network traffic.
    #
    #     The 'reassembly' argument specifies the flags for the reassembly
    #     operation; available flags are PF_REASS_ENABLED and PF_REASS_NODF.
    #     """
    #     with open(self.dev, 'w') as d:
    #         with _PFTrans(d):
    #             ioctl(d, DIOCSETREASS, c_uint32(reassembly))

    def get_limit(self, limit=None):
        """Return the hard limits on the memory pools used by Packet Filter.

        'limit' can be either one of the PF_LIMIT_* constants or a string;
        return the value of the requested limit (UINT_MAX means unlimited) or,
        if called with no arguments, a dictionary containing all the available
        limits.
        """
        if limit is None:
            return dict([(l, self.get_limit(l)) for l in PF_LIMITS])
        elif limit in PF_LIMITS:
            limit = PF_LIMITS[limit]

        pl = pfioc_limit(index=limit)

        with open(self.dev, 'r') as d:
            ioctl(d, DIOCGETLIMIT, pl.asBuffer())

        return pl.limit

    def set_limit(self, limit, value):
        """Set hard limits on the memory pools used by Packet Filter.

        'limit' can be either one of the PF_LIMIT_* constants or a string; a
        'value' of UINT_MAX means unlimited. Raise PFError if the current pool
        size exceeds the requested hard limit.
        """
        if limit in PF_LIMITS:
            limit = PF_LIMITS[limit]

        pl = pfioc_limit(index=limit, limit=value)
        with open(self.dev, 'w') as d:
            with _Transaction(d):
                try:
                    ioctl(d, DIOCSETLIMIT, pl.asBuffer())
                except IOError, (e, s):
                    if e == EBUSY:
                        raise PFError("Current pool size > {0:d}".format(value))
                    raise

    def get_timeout(self, timeout=None):
        """Return the configured timeout values for PF states.

        'timeout' can be either one of the PFTM_* constants or a string; return
        the value of the requested timeout or, if called with no arguments, a
        dictionary containing all the available timeouts.
        """
        if timeout is None:
            return dict([(t, self.get_timeout(t)) for t in PF_TIMEOUTS])
        elif timeout in PF_TIMEOUTS:
            timeout = PF_TIMEOUTS[timeout]

        tm = pfioc_tm(timeout=timeout)
        with open(self.dev, 'r') as d:
            ioctl(d, DIOCGETTIMEOUT, tm.asBuffer())

        return tm.seconds

    def set_timeout(self, timeout, value):
        """Set the timeout 'value' for a specific PF state.

        'timeout' can be either one of the PFTM_* constants or a string; return
        the old value of the specified timeout.
        """
        if timeout in PF_TIMEOUTS:
            timeout = PF_TIMEOUTS[timeout]

        tm = pfioc_tm(timeout=timeout, seconds=value)
        with open(self.dev, 'w') as d:
            with _Transaction(d):
                ioctl(d, DIOCSETTIMEOUT, tm.asBuffer())

        return tm.seconds

    def set_optimization(self, opt="normal"):
        """Set the optimization profile for state handling like pfctl."""
        for name, val in PF_HINTS[opt].iteritems():
            self.set_timeout(name, val)

    def get_optimization(self):
        """ """
        tm = self.get_timeout()
        for name, val in PF_HINTS.iteritems():
            if val["tcp.first"] == tm["tcp.first"]:
                return name

    def get_ifaces(self, ifname=""):
        """Get the list of interfaces and interface drivers known to pf.

        Return a tuple of PFIface objects or a single PFIface object if a
        specific 'ifname' is specified.
        """
        pi = pfioc_iface(pfiio_name=ifname, pfiio_esize=sizeof(pfi_kif))

        with open(self.dev, 'w') as d:
            ioctl(d, DIOCIGETIFACES, pi.asBuffer())
            buf = (pfi_kif * pi.pfiio_size)()
            pi.pfiio_buffer = addressof(buf)
            ioctl(d, DIOCIGETIFACES, pi.asBuffer())

        if ifname and len(buf) == 1:
            return Interface(buf[0])
        else:
            return tuple(map(Interface, buf))

    def set_ifflags(self, ifname, flags):
        """Set the user setable 'flags' on the interface 'ifname'."""
        pi = pfioc_iface(pfiio_name=ifname, pfiio_flags=flags)
        with open(self.dev, 'w') as d:
            with _Transaction(d):
                ioctl(d, DIOCSETIFFLAG, pi.asBuffer())

    def clear_ifflags(self, ifname, flags=None):
        """Clear the specified user setable 'flags' on the interface 'ifname'.

        If no flags are specified, clear all flags.
        """
        if flags is None:
            flags = PFI_IFLAG_SKIP

        pi = pfioc_iface(pfiio_name=ifname, pfiio_flags=flags)
        with open(self.dev, 'w') as d:
            with _Transaction(d):
                ioctl(d, DIOCCLRIFFLAG, pi.asBuffer())

    def set_status_if(self, ifname=""):
        """Specify the interface for which statistics are accumulated.

        If no 'ifname' is provided, turn off the collection of per-interface
        statistics. Raise PFError if 'ifname' is not a valid interface name.
        """
        pi = pfioc_iface(pfiio_name=ifname)
        with open(self.dev, 'w') as d:
            with _Transaction(d):
                try:
                    ioctl(d, DIOCSETSTATUSIF, pi.asBuffer())
                except IOError, (e, s):
                    if e == EINVAL:
                        raise PFError("Invalid ifname: '{0}'".format(ifname))
                    raise

    def get_status(self):
        """Return a PFStatus object containing the internal PF statistics."""
        s = pf_status()
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCGETSTATUS, s.asBuffer())

        return Status(s)

    def clear_status(self, ifname=""):
        """Clear the internal packet filter statistics.

        An optional 'ifname' can be specified in order to clear statistics only
        for a specific interface.
        """
        pi = pfioc_iface(pfiio_name=ifname)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCCLRSTATUS, pi.asBuffer())

    def get_states(self):
        """Retrieve Packet Filter's state table entries.

        Return a tuple of PFState objects representing the states currently
        tracked by PF.
        """
        ps = pfioc_states()

        l = 0
        with open(self.dev, 'w') as d:
            while True:
                if l:
                    ps_states = (pfsync_state * (l / sizeof(pfsync_state)))()
                    ps.ps_buf = addressof(ps_states)
                    ps.ps_len = l
                ioctl(d, DIOCGETSTATES, ps.asBuffer())
                if ps.ps_len == 0:
                    return ()
                if ps.ps_len <= l:
                    break
                l = (ps.ps_len * 2)

        ps_num = (ps.ps_len / sizeof(pfsync_state))
        return tuple([State(s) for s in ps_states[:ps_num]])

    def clear_states(self, ifname=""):
        """Clear all states.

        If an interface name is provided, only states for that interface will
        be cleared. Return the number of cleared states.
        """
        psk = pfioc_state_kill(psk_ifname=ifname)

        with open(self.dev, 'w') as d:
            ioctl(d, DIOCCLRSTATES, psk.asBuffer())

        return psk.psk_killed

    def kill_states(self, af=AF_UNSPEC, proto=0, src=None, dst=None, ifname="",
                    label="", rdomain=0):
        """Clear states matching the specified arguments.

        States can be specified by address family, layer-4 protocol, source and
        destination addresses, interface name, label and routing domain. Return
        the number of killed states.
        """
        psk = pfioc_state_kill(psk_af=af, psk_proto=proto, psk_ifname=ifname,
                               psk_label=label, psk_rdomain=rdomain)
        if src:
            psk.psk_src = src.to_struct()
        if dst:
            psk.psk_dst = dst.to_struct()

        with open(self.dev, 'w') as d:
            ioctl(d, DIOCKILLSTATES, psk.asBuffer())

        return psk.psk_killed

    def clear_rules(self, path=""):
        """Clear all rules contained in the anchor 'path'."""
        self.load_ruleset(Ruleset(), path)

    # UNSUPPORTED ON MAC OS X
    # def load_queues(self, *queues):
    #     """Load a set of queues on an interface.
    #
    #     'queues' must be PFQueue objects.
    #     """
    #     with open(self.dev, 'w') as d:
    #         with _PFTrans(d, "", PF_TRANS_RULESET) as t:
    #             for queue in queues:
    #                 q = pfioc_queue(ticket=t.array[0].ticket,
    #                                 queue=queue._to_struct())
    #                 ioctl(d, DIOCADDQUEUE, q)

    # UNSUPPORTED ON MAC OS X
    # def get_queues(self):
    #     """Retrieve the currently loaded queues.
    #
    #     Return a tuple of PFQueue objects.
    #     """
    #     queues = []
    #     pq = pfioc_queue()
    #     with open(self.dev, 'r') as d:
    #         ioctl(d, DIOCGETQUEUES, pq)
    #
    #         qstats = queue_stats()
    #         for nr in range(pq.nr):
    #             pqs = pfioc_qstats(nr=nr, ticket=pq.ticket,
    #                                buf=addressof(qstats.data),
    #                                nbytes=sizeof(hfsc_class_stats))
    #             ioctl(d, DIOCGETQSTATS, pqs)
    #             queue = PFQueue(pqs.queue)
    #             queue.stats = PFQueueStats(qstats.data)
    #             queues.append(queue)
    #
    #     return queues

    def _get_rules(self, path, dev, clear):
        """Recursively retrieve rules from the specified ruleset."""
        if path.endswith("/*"):
            path = path[:-2]

        pr = pfioc_rule(anchor=path)
        if clear:
            pr.action = PF_GET_CLR_CNTR

        pr.rule.action = PF_PASS
        ioctl(dev, DIOCGETRULES, pr.asBuffer())

        tables = list(self.get_tables(Table(anchor=path)))
        rules = []
        for nr in range(pr.nr):
            pr.nr = nr
            ioctl(dev, DIOCGETRULE, pr.asBuffer())
            if pr.anchor_call:
                path = os.path.join(pr.anchor, pr.anchor_call)
                rs = Ruleset(pr.anchor_call, pr.rule)
                rs.append(*self._get_rules(path, dev, clear))
                rules.append(rs)
            else:
                rules.append(Rule(pr.rule))

        return tables + rules

    def get_ruleset(self, path="", clear=False, **kw):
        """Return a PFRuleset object containing the active ruleset.
        
        'path' is the path of the anchor to retrieve rules from. If 'clear' is
        True, per-rule statistics will be cleared. Keyword arguments can be
        passed for returning only matching rules.
        """
        rs = Ruleset(os.path.basename(path))

        with open(self.dev, 'r') as d:
            for rule in self._get_rules(path, d, clear):
                if isinstance(rule, Rule):
                    if not all((getattr(rule, attr) == value)
                               for (attr, value) in kw.iteritems()):
                        continue
                rs.append(rule)
        return rs

    @staticmethod
    def _inadefine(table, dev, path, ticket):
        """Define a table in the inactive ruleset."""
        table.anchor = path
        io = pfioc_table(pfrio_table=table.to_struct(), pfrio_ticket=ticket, pfrio_esize=sizeof(pfr_addr))

        if table.addrs:
            io.pfrio_flags |= PFR_FLAG_ADDRSTOO
            addrs = table.addrs
            buf = (pfr_addr * len(addrs))(*[a.to_struct() for a in addrs])
            io.pfrio_buffer = addressof(buf)
            io.pfrio_size = len(addrs)

        ioctl(dev, DIOCRINADEFINE, io.asBuffer())

    def load_ruleset(self, ruleset, path=""):
        """Load the given ruleset.

        'ruleset' must be a Ruleset object; 'path' is the name of the anchor into which to load rules
        """
        with open(self.dev, 'w') as fd:
            with _Transaction(fd, path) as t:
                for a in t.array:
                    for r in ruleset.rules:
                        pr = pfioc_rule(ticket=a.ticket, anchor=path, rule=r.to_struct())

                        if isinstance(r, Ruleset):
                            pr.anchor_call = r.name

                        ioctl(fd, DIOCADDRULE, pr.asBuffer())

                        if isinstance(r, Ruleset):
                            self.load_ruleset(r, os.path.join(path, r.name))

    def add_tables(self, *tables):
        """Create one or more tables.

        'tables' must be Table objects; return the number of tables created.
        """
        io = pfioc_table(pfrio_esize=sizeof(pfr_table), pfrio_size=len(tables))
        buf = (pfr_table * len(tables))(*[t.to_struct() for t in tables])
        io.pfrio_buffer = addressof(buf)

        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRADDTABLES, io.asBuffer())

        for table in filter(lambda t: t.addrs, tables):
            self.add_addrs(table, *table.addrs)

        return io.pfrio_nadd

    def clear_tables(self, filter_=None):
        """Clear all tables.

        'filter' is a Table object that allows you to specify the attributes
        of the tables to delete. Return the number of tables deleted.
        """
        io = pfioc_table()
        if filter_ is not None:
            io.pfrio_table = pfr_table(pfrt_name=filter_.name, pfrt_anchor=filter_.anchor)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRCLRTABLES, io.asBuffer())
        return io.pfrio_ndel

    def del_tables(self, *tables):
        """Delete one or more tables.

        'tables' must be Table objects. Return the number of tables deleted.
        """
        io = pfioc_table(pfrio_esize=sizeof(pfr_table), pfrio_size=len(tables))

        buf = (pfr_table * len(tables))()
        for (t, b) in zip(tables, buf):
            b.pfrt_name = t.name
            b.pfrt_anchor = t.anchor
        io.pfrio_buffer = addressof(buf)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRDELTABLES, io.asBuffer())
        return io.pfrio_ndel

    def get_tables(self, filter_=None, buf_size=10):
        """Get the list of all tables.

        'filter' is a Table object that allows you to specify the attributes
        of the tables to retrieve. Return a tuple of Table objects containing
        the currently-loaded tables.
        """
        io = pfioc_table(pfrio_esize=sizeof(pfr_table))

        if filter_ is not None:
            io.pfrio_table = pfr_table(pfrt_name=filter_.name, pfrt_anchor=filter_.anchor)

        with open(self.dev, 'w') as d:
            while True:
                buf = (pfr_table * buf_size)()
                io.pfrio_buffer = addressof(buf)
                io.pfrio_size = buf_size
                ioctl(d, DIOCRGETTABLES, io.asBuffer())

                if io.pfrio_size <= buf_size:
                    break
                buf_size = io.pfrio_size

        tables = []
        for t in buf[:io.pfrio_size]:
            try:
                addrs = self.get_addrs(Table(t))
            except IOError, (e, s):
                pass       # Ignore tables of which you can't get the addresses
            else:
                tables.append(Table(t, *addrs))

        return tuple(tables)

    def add_addrs(self, table, *addrs):
        """Add one or more addresses to a table.

        'table' can be either a Table instance or a string containing the
        table name; 'addrs' can be either Address instances or strings.
        Return the number of addresses effectively added.
        """
        if isinstance(table, basestring):
            table = pfr_table(pfrt_name=table)
        else:
            table = pfr_table(pfrt_name=table.name, pfrt_anchor=table.anchor)

        _addrs = []
        for addr in addrs:
            if isinstance(addr, Address):
                _addrs.append(addr)
            else:
                _addrs.append(Address(addr))

        io = pfioc_table(pfrio_table=table, pfrio_esize=sizeof(pfr_addr), pfrio_size=len(addrs))
        buf = (pfr_addr * len(addrs))(*[a.to_struct() for a in _addrs])
        io.pfrio_buffer = addressof(buf)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRADDADDRS, io.asBuffer())
        return io.pfrio_nadd

    def clear_addrs(self, table):
        """Clear all addresses in the specified table.

        Return the number of addresses removed.
        """
        if isinstance(table, basestring):
            table = pfr_table(pfrt_name=table)
        else:
            table = pfr_table(pfrt_name=table.name, pfrt_anchor=table.anchor)
        io = pfioc_table(pfrio_table=table)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRCLRADDRS, io.asBuffer())
        return io.pfrio_ndel

    def del_addrs(self, table, *addrs):
        """Delete one or more addresses from the specified table.

        'table' can be either a Table instance or a string containing the
        table name; 'addrs' can be either Address instances or strings.
        Return the number of addresses deleted.
        """
        if isinstance(table, basestring):
            table = pfr_table(pfrt_name=table)
        else:
            table = pfr_table(pfrt_name=table.name, pfrt_anchor=table.anchor)

        _addrs = []
        for addr in addrs:
            if isinstance(addr, Address):
                _addrs.append(addr)
            else:
                _addrs.append(Address(addr))
        io = pfioc_table(pfrio_table=table, pfrio_esize=sizeof(pfr_addr), pfrio_size=len(addrs))
        buf = (pfr_addr * len(addrs))(*[a.to_struct() for a in _addrs])
        io.pfrio_buffer = addressof(buf)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRDELADDRS, io.asBuffer())
        return io.pfrio_ndel

    def set_addrs(self, table, *addrs):
        """Replace the content of a table.

        'table' can be either a Table instance or a string containing the
        table name; 'addrs' can be either Address instances or strings.
        Return a tuple containing the number of addresses deleted, added and
        changed.
        """
        if isinstance(table, basestring):
            table = pfr_table(pfrt_name=table)
        else:
            table = pfr_table(pfrt_name=table.name, pfrt_anchor=table.anchor)

        _addrs = []
        for addr in addrs:
            if isinstance(addr, Address):
                _addrs.append(addr)
            else:
                _addrs.append(Address(addr))

        io = pfioc_table(pfrio_table=table, pfrio_esize=sizeof(pfr_addr), pfrio_size=len(addrs))
        buf = (pfr_addr * len(addrs))(*[a.to_struct() for a in _addrs])
        io.pfrio_buffer = addressof(buf)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRSETADDRS, io.asBuffer())
        return io.pfrio_ndel, io.pfrio_nadd, io.pfrio_nchange

    def get_addrs(self, table, buf_size=10):
        """Get the addresses in the specified table.

        'table' can be either a Table instance or a string containing the
        table name. Return a list of Address objects.
        """
        if isinstance(table, basestring):
            table = pfr_table(pfrt_name=table)
        else:
            table = pfr_table(pfrt_name=table.name, pfrt_anchor=table.anchor)
        io = pfioc_table(pfrio_table=table, pfrio_esize=sizeof(pfr_addr))
        with open(self.dev, 'w') as d:
            while True:
                buf = (pfr_addr * buf_size)()
                io.pfrio_buffer = addressof(buf)
                io.pfrio_size = buf_size
                ioctl(d, DIOCRGETADDRS, io.asBuffer())
                if io.pfrio_size <= buf_size:
                    break
                buf_size = io.pfrio_size
        return tuple([Address(a) for a in buf[:io.pfrio_size]])

    def get_tstats(self, filter_=None, buf_size=10):
        """Get statistics information for one or more tables.

        'filter' is a Table object that allows you to specify the attributes
        of the tables to retrieve statistics for. Return a tuple of TableStats
        objects.
        """
        io = pfioc_table(pfrio_esize=sizeof(pfr_tstats))
        if filter_ is not None:
            io.pfrio_table = pfr_table(pfrt_name=filter_.name, pfrt_anchor=filter_.anchor)

        with open(self.dev, 'w') as d:
            while True:
                buf = (pfr_tstats * buf_size)()
                io.pfrio_buffer = addressof(buf)
                io.pfrio_size = buf_size
                ioctl(d, DIOCRGETTSTATS, io.asBuffer())
                if io.pfrio_size <= buf_size:
                    break
                buf_size = io.pfrio_size

        stats = []
        for t in buf[:io.pfrio_size]:
            if t.pfrts_tzero:
                stats.append(TableStats(t))

        return tuple(stats)

    def clear_tstats(self, *tables):
        """Clear the statistics of one or more tables.

        'tables' must be Table objects. Return the number of tables cleared.
        """
        io = pfioc_table(pfrio_esize=sizeof(pfr_table), pfrio_size=len(tables))
        _buffer = (pfr_table * len(tables))()
        for (t, b) in zip(tables, _buffer):
            b.pfrt_name = t.name
            b.pfrt_anchor = t.anchor

        io.pfrio_buffer = addressof(_buffer)
        with open(self.dev, 'w') as d:
            ioctl(d, DIOCRCLRTSTATS, io.asBuffer())
        return io.pfrio_nadd
