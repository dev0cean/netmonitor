#!/usr/bin/env python3
import sys
from launchctl import launchctl
# import argparse
# import argcomplete
# import subprocess
# import six

try:
    action = sys.argv[1]
    launchctl({"enter": "load", "exit": "unload"}.get(action), "/Library/LaunchDaemons/org.macports.cntlm.plist")
except IndexError:
    print("One of 'enter', 'exit' is required")


# CNTLM_PLIST = "/Library/LaunchDaemons/org.macports.cntlm.plist"
# ROUTES_CONF = "FlightCentre/routes.conf"
#
#
# def launchctl(subcommand, *args):
#     """
#     A minimal wrapper to call the launchctl binary and capture the output
#     :param subcommand: string
#     """
#     if not isinstance(subcommand, six.string_types):
#         raise ValueError("Argument is invalid: %r" % repr(subcommand))
#     if isinstance(subcommand, six.text_type):
#         subcommand = subcommand.encode('utf-8')
#
#     cmd = ["launchctl", subcommand]
#     for arg in args:
#         if isinstance(arg, six.string_types):
#             if isinstance(arg, six.text_type):
#                 cmd.append(arg.encode('utf-8'))
#             else:
#                 cmd.append(arg)
#         else:
#             raise ValueError("Argument is invalid: %r" % repr(arg))
#     return subprocess.check_output(cmd, stdin=None, stderr=subprocess.STDOUT, shell=False)
#
#
# def route(action, kind, address, gateway, netmask):
#     cmd = ["route", action, "-%s" % kind, address, gateway, netmask]
#     return subprocess.check_output(cmd, stdin=None, stderr=subprocess.STDOUT, shell=False)
#
#
# def alter_routes(interface, config, action):
#     with open(config) as infile:
#         for line in infile.readlines():
#             if line.startswith(interface):
#                 _, kind, address, netmask, gateway = line.split()
#                 route(action, kind, address, gateway, netmask)
#
#
# def _enter():
#     print("enter")
#     exit(0)
#     launchctl("load", CNTLM_PLIST)
#
#
# def _exit():
#     print("exit")
#     exit(0)
#     launchctl("unload", CNTLM_PLIST)
#
#
# def _up():
#     print("up %s" % args.interface)
#     exit(0)
#     alter_routes(args.interface, ROUTES_CONF, "add")
#
#
# def _down():
#     print("down %s" % args.interface)
#     exit(0)
#     alter_routes(args.interface, ROUTES_CONF, "del")
#
#
# if __name__ == '__main__':
#     parser = argparse.ArgumentParser()
#     subparsers = parser.add_subparsers()
#     enter_parser = subparsers.add_parser("enter")
#     enter_parser.set_defaults(func=_enter)
#     exit_parser = subparsers.add_parser("exit")
#     exit_parser.set_defaults(func=_exit)
#     up_parser = subparsers.add_parser("up")
#     up_parser.add_argument("interface")
#     up_parser.set_defaults(func=_up)
#     down_parser = subparsers.add_parser("down")
#     down_parser.add_argument("interface")
#     down_parser.set_defaults(func=_down)
#
#     argcomplete.autocomplete(parser)
#     args = parser.parse_args()
#     args.func()
#     pass
