#!/usr/bin/env python
import sys
from launchctl import launchctl

try:
    action = sys.argv[1]
    launchctl({"enter": "load", "exit": "unload"}.get(action), "/Library/LaunchDaemons/org.macports.cntlm.plist")
except IndexError:
    print("One of 'enter', 'exit' is required")
